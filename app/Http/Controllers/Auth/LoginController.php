<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\activitylog;
use Auth;
Use \Carbon\Carbon;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function redirectTo()
    {
        $type = "";

        if(auth()->user()->isAdmin()) {


            if(Auth::user()->type == 1){

                $type = "admin";

            }elseif(Auth::user()->type == 2){
                $type = "maintenance";

            }else{

                $type = "user";
            }

            $insertactivity = activitylog::insert(array(
                'name' => Auth::user()->email,
                'message_problem'=> "login",
                'type' =>  $type

            ));





            return '/admin/dashboard';

        }
        elseif (auth()->user()->isMaintence()) {


            if(Auth::user()->type == 1){

                $type = "admin";

            }elseif(Auth::user()->type == 2){
                $type = "maintenance";

            }else{

                $type = "user";
            }

            $insertactivity = activitylog::insert(array(
            'name' => Auth::user()->email,
            'message_problem'=> "login",
            'type' =>  $type

                ));


            return '/maintence/dashboard';
        }
        else {


            if(Auth::user()->type == 1){

                    $type = "admin";

            }elseif(Auth::user()->type == 2){
                     $type = "maintenance";

            }else{

                    $type = "user";
            }

            $insertactivity = activitylog::insert(array(
            'name' => Auth::user()->email,
            'message_problem'=> "login",
            'type' =>  $type

            ));

            return '/home';

        }
    }

}
