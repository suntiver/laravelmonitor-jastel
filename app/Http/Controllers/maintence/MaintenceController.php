<?php

namespace App\Http\Controllers\maintence;

use DB;
use Cache;
use Auth;
use App\User;
use App\device;
use App\devicepcb;
use App\activitylog;
use App\table_rack;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
class MaintenceController extends Controller
{


        #จัดการตู้ Rack
        public function rack(){

                $queryrack = DB::table('table_rack')->where('key_rack_setting','!=','#P3-100000')->get();
                $querydeviceonline = DB::table('device')->where('status','=',1)->where('key_rack','!=','#P3-100000')->get();
                $querydeviceonlineaddtorack = DB::table('device')->where('status','=',1)->where('key_rack','=','#P3-100000')->get();
                $querydeviceonlinenew = DB::table('device')->where('status','=',0)->get();

                #คิวรี key_rack ไปเเสดงใน dropdown
                $anotherRack = DB::table('table_rack')->select('key_rack_setting')->where('key_rack_setting','!=','#P3-100000')->where('status','=',1)->get();

                return view('maintence/rack',compact('queryrack','querydeviceonline','querydeviceonlineaddtorack','querydeviceonlinenew','anotherRack'));

        }


         #อัพการเปิดสถานะอุปกรณ์ที่เชื่อมต่อเข้ามาใหม่
         public function updatedeviceconnectnew($id,Request $request)
        {

            $status_update =  device::where('id_device','=',$id)->update(['status' => $request->input('status') ]);

            if($status_update != true){
                $notification = array(
                'message' => 'จัดการข้อมูลที่เลือกไม่สำเร็จ!!',
                'alert-type' => 'warning',
                );

            }else{

                $notification = array(
                'message' => 'จัดการข้อมูลที่เลือกสำเร็จ!!',
                'alert-type' => 'success',
                );
            }


            //บันทึกกิจกรรม ลง log
            $type = ""; //บทบาท

            if(Auth::user()->type == 1){

                $type = "admin";

            }elseif(Auth::user()->type == 2){
                $type = "maintenance";

            }else{

                $type = "user";
            }
            $insertactivity = activitylog::insert(array(
                'name' => Auth::user()->email,
                'message_problem'=> ("updatedeviceconnec id_device:".$id.",status :".$request->input('status')),
                'type' =>  $type
            ));

            return back()->with($notification);
        }


         #อัพเดท อุปกรณ์ที่เปิดเเต่ยังไม่ถูกเพิ่มไปในตู้เเร็ค
         public function updeviceopenfortoreack($id,Request $request)
        {

            $update_device =  device::where('id_device','=',$id)->update([

            'status' => $request->input('status'),
            'key_rack' =>$request->input('selectrack')

            ]);

            if($update_device  != true){
                $notification = array(
                'message' => 'จัดการข้อมูลที่เลือกไม่สำเร็จ!!',
                'alert-type' => 'warning',
                );

            }else{

                $notification = array(
                'message' => 'จัดการข้อมูลที่เลือกสำเร็จ!!',
                'alert-type' => 'success',
                );
            }

            //บันทึกกิจกรรม ลง log
            $type = ""; //บทบาท

            if(Auth::user()->type == 1){

                $type = "admin";

            }elseif(Auth::user()->type == 2){
                $type = "maintenance";

            }else{

                $type = "user";
            }
            $insertactivity = activitylog::insert(array(
                'name' => Auth::user()->email,
                'message_problem'=> ("updeviceopenfortoreack id_device :".$id.",status :".$request->input('status').",key_rack :".$request->input('selectrack')),
                'type' =>  $type
            ));





            return back()->with($notification);
        }


        #ดึงตัวเเคมป์ใน rack ว่ามีตู้ rackที่เลือกมีอะไรบ้าง
        public function getcampinrack($id)
        {
            $key_rack_setting = DB::table('table_rack')->select('key_rack_setting')->where('id','=',$id)->pluck('key_rack_setting');

            $key_rack_setting_value = $key_rack_setting[0];


            $queryrack = DB::table('device')->where('key_rack','=',$key_rack_setting)->where('status','!=',0)->get();

            $showrackallformove =  DB::table('table_rack')->where('key_rack_setting','!=',$key_rack_setting)->where('key_rack_setting','!=','#P3-100000')->where('status','!=',0)->get();

            $querynamerack =  DB::table('table_rack')->where('key_rack_setting','=',$key_rack_setting_value)->first();


            return  view('maintence/campinrack',compact('key_rack_setting_value','queryrack','showrackallformove','querynamerack'));
        }



        #เอาไว้ลบอุปกรณ์วัด
        public function deletedevice($id){

            $deledevice=  device::where('id_device','=',$id)->delete();

                $notification = array(
                'message' => 'ลบอุปกรณ์ที่เลือกสำเร็จ!!',
                'alert-type' => 'success'
                );


            //บันทึกกิจกรรม ลง log
            $type = ""; //บทบาท

            if(Auth::user()->type == 1){

                $type = "admin";

            }elseif(Auth::user()->type == 2){
                $type = "maintenance";

            }else{

                $type = "user";
            }
            $insertactivity = activitylog::insert(array(
                'name' => Auth::user()->email,
                'message_problem'=> ("deletedevice  id_device :".$id),
                'type' =>  $type
            ));


            return  back()->with($notification);

        }


       #เอาไว้ดูว่าเราสามารถย้ายอุปกรณ์ไปตู้เเร็คไหนได้บ้าง
       public function movecamp($id,Request $request)
       {

           $selectNotification = $request->input('selectNotification');
           $Low = $request->input('Low');
           $High = $request->input('High');
           $keyrackselect = $request->input('keyrackselect');

           //บันทึกกิจกรรม ลง log
           $type = ""; //บทบาท

           if(Auth::user()->type == 1){

               $type = "admin";

           }elseif(Auth::user()->type == 2){
               $type = "maintenance";

           }else{

               $type = "user";
           }


           if($keyrackselect  == "0"){

                   if($Low >= 1  and ($High >  $Low)){
                       $update_device =  device::where('id_device','=',$id)->update([
                           'settingLow'=> $Low,
                           'settingHight'=> $High,
                           'status_irm' =>  $selectNotification
                       ]);

                       $notification = array(
                               'message' => 'จัดการอุปกรณ์สำเร็จ!!',
                               'alert-type' => 'success'
                       );

                       $insertactivity = activitylog::insert(array(
                       'name' => Auth::user()->email,
                       'message_problem'=> ("movecamp  id_device :".$id.",key_rack :".$keyrackselect." setting Low: ".$Low." setting High: ".$High),
                       'type' =>  $type
                       ));


                       return  back()->with($notification);

                   }else{
                       $notification = array(
                               'message' => 'ค่าเกณฑ์ที่ตั้งไม่ตรงตามเงือนไขโปรดตรวจสอบ!!',
                               'alert-type' => 'warning'
                       );

                       $insertactivity = activitylog::insert(array(
                           'name' => Auth::user()->email,
                           'message_problem'=> ("movecamp  id_device :".$id.",key_rack :".$keyrackselect." setting Low: ".$Low." setting High: ".$High),
                           'type' =>  $type
                       ));
                       return  back()->with($notification);
                   }


           }else{

               if($Low >= 1  and ($High >  $Low)){
                   $update_device =  device::where('id_device','=',$id)->update([
                           'key_rack' =>  $keyrackselect,
                           'settingLow'=> $Low,
                           'settingHight'=> $High,
                           'status_irm' =>  $selectNotification
                   ]);

                   $notification = array(
                           'message' => 'จัดการอุปกรณ์สำเร็จ!!'.$Low,
                           'alert-type' => 'success'
                   );

                   $insertactivity = activitylog::insert(array(
                       'name' => Auth::user()->email,
                       'message_problem'=> ("movecamp  id_device :".$id.",key_rack :".$keyrackselect." setting Low: ".$Low." setting High: ".$High),
                       'type' =>  $type
                   ));
                   return  back()->with($notification);
               }else{
                       $notification = array(
                               'message' => 'ค่าเกณฑ์ที่ตั้งไม่ตรงตามเงือนไขโปรดตรวจสอบ!!'.$Low,
                               'alert-type' => 'warning'
                       );

                       $insertactivity = activitylog::insert(array(
                           'name' => Auth::user()->email,
                           'message_problem'=> ("movecamp  id_device :".$id.",key_rack :".$keyrackselect." setting Low: ".$Low." setting High: ".$High),
                           'type' =>  $type
                       ));
                       return  back()->with($notification);
               }

           }


       }




        #จัดการสถานะอุปกรณ์วัดว่าเป็นอย่างไร
        public function devicepcb()
        {
            $displaydevicepcb= devicepcb::all();


            return view('maintence/devicepcb',compact('displaydevicepcb'));
        }

        #เอาไว้เพิ่มรูปภาพอุปกรณ์วัด pcb
        public function  adddevicepcb(Request $request)
        {
            #เอาไว้เช็คว่า ip ที่ผู้ใช้ป้อนเข้ามาซ้ำกับในระบบหรือไม่ ถ้าซ้ำเเจ้งเตือนกลับไปว่าไม่สามารถใช้ได้
            $check = devicepcb::where('ipdevice','=',$request->input('ip'))->count();
            if($check == 1){
                $notification = array(
                    'message' => 'ไม่สามารถเพิ่มอุปกรณ์ได้เนื่องจาก ไอพีนี้มีอยู่ในระบบเเล้ว',
                    'alert-type' => 'warning'
                 );

                //บันทึกกิจกรรม ลง log
                $type = ""; //บทบาท
                if(Auth::user()->type == 1){

                    $type = "admin";

                }elseif(Auth::user()->type == 2){
                    $type = "maintenance";

                }else{

                    $type = "user";
                }
                $insertactivity = activitylog::insert(array(
                    'name' => Auth::user()->email,
                    'message_problem'=> ("adddevicepcb falid  ipdevice :".$request->input('ip')),
                    'type' =>  $type
                ));


            }else{

                    $devicepcb  = new devicepcb();
                    $devicepcb->nameDevice =  $request->input('device');
                    $devicepcb->ipdevice  = $request->input('ip');

                    if($request->hasfile('uploadimg')){
                            $file = $request->file('uploadimg');
                            $extension = $file->getClientOriginalExtension();  //getting image extension
                            $filename = time().'.'.$extension;
                            $file->move('uploads/devicepcb/',$filename);
                            $devicepcb->image = $filename;
                    }
                    $devicepcb->save();

                    $notification = array(
                    'message' => 'เพิ่มอุปกรณ์พีซีบีสำหรับวัดไฟสำเร็จ',
                    'alert-type' => 'success'
                    );

                    //บันทึกกิจกรรม ลง log
                    $type = ""; //บทบาท
                    if(Auth::user()->type == 1){

                        $type = "admin";

                    }elseif(Auth::user()->type == 2){
                        $type = "maintenance";

                    }else{

                        $type = "user";
                    }
                    $insertactivity = activitylog::insert(array(
                        'name' => Auth::user()->email,
                        'message_problem'=> ("adddevicepcb success ipdevice :".$request->input('ip')),
                        'type' =>  $type
                    ));


            }


           return  back()->with($notification);
        }


         #เอาไว้ลบอุปกรณ์พีซีบี
         public function  deletedevicepcb($id){


                    $filenameimage = devicepcb::findOrFail($id);

                    $image_path = public_path().'/uploads/devicepcb/'.$filenameimage->image ;


                    if(File::exists($image_path))
                    {
                        File::delete($image_path);
                    }

                    $filenameimage->delete();

                    $notification = array(
                    'message' => 'ลบอุปกรณ์ที่ผู้ใช้เลือกสำเร็จ',
                    'alert-type' => 'success'
                    );


                    //บันทึกกิจกรรม ลง log
                    $type = ""; //บทบาท
                    if(Auth::user()->type == 1){

                        $type = "admin";

                    }elseif(Auth::user()->type == 2){
                        $type = "maintenance";

                    }else{

                        $type = "user";
                    }
                    $insertactivity = activitylog::insert(array(
                        'name' => Auth::user()->email,
                        'message_problem'=> ("deletedevicepcb id :".$id),
                        'type' =>  $type
                    ));

                    return  back()->with($notification);

        }



        #เอาไว้ตั้งค่าการเเจ้งเตือนระดับร้ายเเรง ไฟดับ
        public function critical()
        {

            $querydatacolor = DB::table('setting_notification')->where('rang','=',3)->get()->first();

            return view('maintence/critical',compact('querydatacolor'));
        }

          #เอาไว้อัพเดทสีระรับร้ายเเรงไฟดับ
         public function updatecritical(Request $request)
        {

            $postupdate = DB::table('setting_notification')->where('rang','=',3)->update([
                'color'=> $request->input('selectcolor'),
                'message' => $request->input('content')

                ]);

            $notification = array(
                    'message' => 'อัพเดทสีสำหรับการเเจ้งเตือนเมื่อเกิดเหตุการณ์ไฟดับ!!!',
                    'alert-type' => 'success'
             );


            //บันทึกกิจกรรม ลง log
            $type = ""; //บทบาท
            if(Auth::user()->type == 1){

                $type = "admin";

            }elseif(Auth::user()->type == 2){
                $type = "maintenance";

            }else{

                $type = "user";
            }
            $insertactivity = activitylog::insert(array(
                'name' => Auth::user()->email,
                'message_problem'=> ("updatecritical  color :".$request->input('selectcolor').",message :".$request->input('content')),
                'type' =>  $type
            ));


            return back()->with($notification);
        }



        #เอาไว้ตั้งค่าการเเจ้งเตือนกระเเสไฟสูงกว่าเกณฑ์
            public function high()
        {
            $querydatacolor = DB::table('setting_notification')->where('rang','=',2)->get()->first();

            return view('maintence/high',compact('querydatacolor'));
        }



          #เอาไว้อัพเดทสีระรับร้ายเเรงไฟฟ้าสูงกว่าเกณฑ์
          public function updatehigh(Request $request)
        {

            $postupdate = DB::table('setting_notification')->where('rang','=',2)->update([
                'color'=> $request->input('selectcolor'),
                'message' => $request->input('content')

                ]);

            $notification = array(
                    'message' => 'อัพเดทสีสำหรับการเเจ้งเตือนเมื่อเกิดเหตุการณ์ไฟฟ้าสูงกว่าเกณฑ์!!!',
                    'alert-type' => 'success'
             );



            //บันทึกกิจกรรม ลง log
            $type = ""; //บทบาท
            if(Auth::user()->type == 1){

                $type = "admin";

            }elseif(Auth::user()->type == 2){
                $type = "maintenance";

            }else{

                $type = "user";
            }
            $insertactivity = activitylog::insert(array(
                'name' => Auth::user()->email,
                'message_problem'=> ("updatehigh  color :".$request->input('selectcolor').",message :".$request->input('content')),
                'type' =>  $type
            ));


            return back()->with($notification);
        }


        #เอาไว้ตั้งค่าการเเจ้งเตือนกระเเสไฟต่ำกว่าเกณฑ์
            public function low()
        {
            $querydatacolor = DB::table('setting_notification')->where('rang','=',1)->get()->first();

            return view('maintence/low',compact('querydatacolor'));
        }


          #เอาไว้อัพเดทสีระรับร้ายเเรงไฟฟ้าต่ำกว่าเกณฑ์
          public function updatelow(Request $request)
        {

            $postupdate = DB::table('setting_notification')->where('rang','=',1)->update([
                'color'=> $request->input('selectcolor'),
                'message' => $request->input('content')

                ]);

            $notification = array(
                    'message' => 'อัพเดทสีสำหรับการเเจ้งเตือนเมื่อเกิดเหตุการณ์ไฟฟ้าต่ำกว่าเกณฑ์!!!',
                    'alert-type' => 'success'
             );

            //บันทึกกิจกรรม ลง log
            $type = ""; //บทบาท
            if(Auth::user()->type == 1){

                $type = "admin";

            }elseif(Auth::user()->type == 2){
                $type = "maintenance";

            }else{

                $type = "user";
            }
            $insertactivity = activitylog::insert(array(
                'name' => Auth::user()->email,
                'message_problem'=> ("updatelow  color :".$request->input('selectcolor').",message :".$request->input('content')),
                'type' =>  $type
            ));


            return back()->with($notification);
        }


        #เอาไว้ตั้งค่าการเเจ้งเตือนกระเเสไฟปกติ
          public function normal()
        {

            $querydatacolor = DB::table('setting_notification')->where('rang','=',0)->get()->first();

            return view('maintence/normal',compact('querydatacolor'));
        }

        #เอาไว้อัพเดทสีระรับร้ายเเรงไฟฟ้าปกติ
         public function updatenormal(Request $request)
        {

            $postupdate = DB::table('setting_notification')->where('rang','=',0)->update([
                'color'=> $request->input('selectcolor'),
                'message' => $request->input('content')

                ]);

            $notification = array(
                    'message' => 'อัพเดทสีสำหรับการเเจ้งเตือนเมื่อเกิดเหตุการณ์ไฟฟ้าปกติ!!!',
                    'alert-type' => 'success'
             );


            //บันทึกกิจกรรม ลง log
            $type = ""; //บทบาท
            if(Auth::user()->type == 1){

                $type = "admin";

            }elseif(Auth::user()->type == 2){
                $type = "maintenance";

            }else{

                $type = "user";
            }
            $insertactivity = activitylog::insert(array(
                'name' => Auth::user()->email,
                'message_problem'=> ("updatenormal  color :".$request->input('selectcolor').",message :".$request->input('content')),
                'type' =>  $type
            ));


            return back()->with($notification);
        }


        #เอาไว้ให้เเนะนำ admin ตรงนี้คืออะไร
        public function help()
        {


            return view('maintence/help');
        }

        #เอาไว้อัพเดทข้อมูลตู้เเร็ค
        public function updateRack($id,Request $request)
        {

            $rack = table_rack::find($id);

            $rack->key_rack_setting = $request->get('key_rack');
            $rack->Ownwer = $request->input('Ownwer');
            $rack->status = $request->input('status');

            $rack->save();

            $notification = array(
                    'message' => 'อัพเดทข้อมูลตู้เเร็คสำเร็จ',
                    'alert-type' => 'success'
             );


              //บันทึกกิจกรรม ลง log
            $type = ""; //บทบาท
            if(Auth::user()->type == 1){

                $type = "admin";

            }elseif(Auth::user()->type == 2){
                $type = "maintenance";

            }else{

                $type = "user";
            }
            $insertactivity = activitylog::insert(array(
                'name' => Auth::user()->email,
                'message_problem'=> ("updatenormal  status_rack :".$request->input('status').",namerack :".$request->input('Ownwer')),
                'type' =>  $type
            ));


            return back()->with($notification);
        }


        public  function   showdatapcb($id,Request $request){

             $querydatacolor = devicepcb::where('id','=',$id)->get()->first();

            return   $querydatacolor;

        }


        public  function editpcbname(Request $request){


            $postupdate = devicepcb::where('id','=',$request->input('id'))->update([
                'nameDevice'=> $request->input('nameDevice')
             ]);    


             $notification = array(
                'message' => 'อัพเดทชื่ออุปกรณ์วัด PCB สำเร็จ!!!',
                'alert-type' => 'success'
            );


            //บันทึกกิจกรรม ลง log
            $type = ""; //บทบาท
            if(Auth::user()->type == 1){

                $type = "admin";

            }elseif(Auth::user()->type == 2){
                $type = "maintenance";

            }else{

                $type = "user";
            }
            
            $insertactivity = activitylog::insert(array(
                'name' => Auth::user()->email,
                'message_problem'=> ("update name deivcePCB :".$request->input('id')),
                'type' =>  $type
            ));



            return back()->with($notification);



        }




}
