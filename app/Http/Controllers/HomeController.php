<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use DB;
use Cache;
use Auth;
use App\User;
use App\device;
use App\devicepcb;
use App\activitylog;

use Illuminate\Http\Request;

class HomeController extends Controller
{




    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    #ส่วนการเเเสดงผลหน้าเว็บ-----------------------------------

    public function index()
    {
        //คิวรีการเเจ้งเตือนการอัพเดทต้องมีทุกหน้า
        $querylastnotification =  DB::table('newnotificationupdate')->orderBy('id', 'DESC')->get()->first();

        //คิวรีตู้เเร็คที่มีสถานะ ไม่เท่ากับคู้เเร็คเริ่มต้น เเละ สถานะตู้เเร็คต้องเปิดการใช้งาน
        $queryrack = DB::table('table_rack')->where('key_rack_setting','!=','#P3-100000')->where('status','=',1)->get();

        //เอาไว้เช็คความผิดปกติร้ายเเรง  rang_serverity 3 คือ เป็นระดับร้ายเเรงที่สุด
        $notificationProm =  DB::table('table_rack')->where('rang_severity','=',3)->where('status','=',1)
                          ->where('key_rack_setting','!=','#P3-100000')->get();


        // $current_view = DB::table('current_viwe')->select('id_current_view')->where('auto_submit','=',0)->where('key_rack','!=','#P3-100000')->count();
        $current_view = device::select('degree')->where('degree','!=',0)->where('key_rack','!=','#P3-100000')->where('status','=',1)->count();
        // $current_view_clear = DB::table('current_viwe')->select('id_current_view')->where('auto_submit','=',1)->where('key_rack','!=','#P3-100000')->count();
        $current_view_clear = device::select('degree')->where('degree','=',0)->where('key_rack','!=','#P3-100000')->where('status','=',1)->count();
        $deviceonline = DB::table('devicepcb')->select('id')->where('signal_device','=',1)->count();
        $deviceofline = DB::table('devicepcb')->select('id')->where('signal_device','=',0)->count();

        $countproble = [
                                'current_view' =>  $current_view,
                                'current_view_clear' => $current_view_clear,
                                'deviceonline'=>  $deviceonline,
                                'deviceofline' => $deviceofline
                        ];

        //สีของการเเจ้งเตือน
        $color_normal = DB::table('setting_notification')->where('rang','=',0)->pluck('color');
        $color_low = DB::table('setting_notification')->where('rang','=',1)->pluck('color');
        $color_high = DB::table('setting_notification')->where('rang','=',2)->pluck('color');
        $color_critical = DB::table('setting_notification')->where('rang','=',3)->pluck('color');

        $color =[
                'color_normal' =>  $color_normal[0],
                'color_low'  =>  $color_low[0],
                'color_high'  =>   $color_high[0],
                'color_critical' => $color_critical[0]
        ];



        return view('user/home',compact('queryrack','notificationProm','countproble','color','querylastnotification'));


    }

     #หน้าการค้นหากระเเสไฟย้อนหลัง
     public function searchhistory()
    {

        //คิวรีการเเจ้งเตือนการอัพเดทต้องมีทุกหน้า
        $querylastnotification =  DB::table('newnotificationupdate')->orderBy('id', 'DESC')->get()->first();

        //สีของการเเจ้งเตือน
        $color_normal = DB::table('setting_notification')->where('rang','=',0)->pluck('color');
        $color_low = DB::table('setting_notification')->where('rang','=',1)->pluck('color');
        $color_high = DB::table('setting_notification')->where('rang','=',2)->pluck('color');
        $color_critical = DB::table('setting_notification')->where('rang','=',3)->pluck('color');
        $color =[
                'color_normal' =>  $color_normal[0],
                'color_low'  =>  $color_low[0],
                'color_high'  =>   $color_high[0],
                'color_critical' => $color_critical[0]
        ];

        //option รหัสตู้เเร็ค
        $optionkeyrack = DB::table('table_rack')->select('key_rack_setting')->where('status','=',1)->where('key_rack_setting','!=','#P3-100000')->get();

        //option รหัสอุปกรณ์วัด
        $optionkeydevice = DB::table('device')->select('key_device')->where('key_rack','!=','#P3-100000')->where('status','=',1)->get();

        return view('user/searchhistory',compact('color','optionkeyrack','optionkeydevice','querylastnotification'));
    }


    #เอาไว้รับ request จาก การค้นหา
    public function queryhistory(Request $request)
    {
        //คิวรีการเเจ้งเตือนการอัพเดทต้องมีทุกหน้า
        $querylastnotification =  DB::table('newnotificationupdate')->orderBy('id', 'DESC')->get()->first();

        //สีของการเเจ้งเตือน
        $color_normal = DB::table('setting_notification')->where('rang','=',0)->pluck('color');
        $color_low = DB::table('setting_notification')->where('rang','=',1)->pluck('color');
        $color_high = DB::table('setting_notification')->where('rang','=',2)->pluck('color');
        $color_critical = DB::table('setting_notification')->where('rang','=',3)->pluck('color');
        $color =[
                'color_normal' =>  $color_normal[0],
                'color_low'  =>  $color_low[0],
                'color_high'  =>   $color_high[0],
                'color_critical' => $color_critical[0]
        ];

        $startdatetime =   $request->get('startdate')." 00:00:00";
        $enddatetime =  $request->get('enddate')." 23:59:00";
        // $keyrack =  $request->get('keyrack');
        $keydevice =  $request->get('keydevice');
        $typesearch =  $request->get('typesearch');
        $startdate =  $request->get('startdate');
        $enddate =  $request->get('enddate');




        $data = [
                        'startdate' =>    $startdatetime,
                        'enddate'   =>  $enddatetime,
                        // 'keyrack'    =>   $keyrack,
                        'keydevice'  =>   $keydevice
                ];



        //ถ้าผู้ใช้เลือก 0 อยากดูเเบบ กราฟ
        if($typesearch == 0){


            return view('user/historychart',compact('color','data','querylastnotification'));

        }
         //ถ้าผู้ใช้เลือก 1 อยากดูเเบบ ตาราง
        else{



            $query  =  DB::table('data_1minutes')->whereBetween('timedate',[$startdatetime,$enddatetime])->where('key_rack','!=','#P3-100000')->where('key_device_1minutes','=',$keydevice)->orderby('id_1minute','desc')->get();


            return view('user/historytable',compact('color','data','query','querylastnotification'));

        }


    }



    #หน้าการค้นหาข้อมูลเชิกลึกย้อนหลัง
    public function searchtecnical()
    {

        //คิวรีการเเจ้งเตือนการอัพเดทต้องมีทุกหน้า
        $querylastnotification =  DB::table('newnotificationupdate')->orderBy('id', 'DESC')->get()->first();

        //สีของการเเจ้งเตือน
        $color_normal = DB::table('setting_notification')->where('rang','=',0)->pluck('color');
        $color_low = DB::table('setting_notification')->where('rang','=',1)->pluck('color');
        $color_high = DB::table('setting_notification')->where('rang','=',2)->pluck('color');
        $color_critical = DB::table('setting_notification')->where('rang','=',3)->pluck('color');
        $color =[
                'color_normal' =>  $color_normal[0],
                'color_low'  =>  $color_low[0],
                'color_high'  =>   $color_high[0],
                'color_critical' => $color_critical[0]
        ];

        //option รหัสตู้เเร็ค
        $optionkeyrack = DB::table('table_rack')->select('key_rack_setting')->where('status','=',1)->where('key_rack_setting','!=','#P3-100000')->get();

        //option รหัสอุปกรณ์วัด
        $optionkeydevice = DB::table('device')->select('key_device')->where('status','=',1)->get();


        return view('user/searchtecnical',compact('color','optionkeyrack','optionkeydevice','querylastnotification'));
    }

    #เอาไว้รับ request จาก การค้นหาข้อมูลเชิกลึกย้อนหลัง
    public function querytecnical(Request $request)
    {

        //คิวรีการเเจ้งเตือนการอัพเดทต้องมีทุกหน้า
        $querylastnotification =  DB::table('newnotificationupdate')->orderBy('id', 'DESC')->get()->first();

        //สีของการเเจ้งเตือน
        $color_normal = DB::table('setting_notification')->where('rang','=',0)->pluck('color');
        $color_low = DB::table('setting_notification')->where('rang','=',1)->pluck('color');
        $color_high = DB::table('setting_notification')->where('rang','=',2)->pluck('color');
        $color_critical = DB::table('setting_notification')->where('rang','=',3)->pluck('color');
        $color =[
                'color_normal' =>  $color_normal[0],
                'color_low'  =>  $color_low[0],
                'color_high'  =>   $color_high[0],
                'color_critical' => $color_critical[0]
        ];

        $startdatetime =   $request->get('startdate')." 00:00:00";
        $enddatetime =  $request->get('enddate')." 23:59:00";
        $keyrack =  $request->get('keyrack');
        $typesearch =  $request->get('typesearch');
        $startdate =  $request->get('startdate');
        $enddate =  $request->get('enddate');




        $data = [
                        'startdate' =>  $startdate,
                        'enddate'   =>  $enddate,
                        'keyrack'    =>  $keyrack
             ];



        //ถ้าผู้ใช้เลือก 0 อยากดูเเบบ 15 นาที
        if($typesearch == 0){
            $query  =  DB::table('data_15minutes')->whereBetween('datetime',[ $startdate,$enddate])->select('key_rack','key_device_15minutes','datetime')->where('key_rack','=',$keyrack)->get();
            #ถ้ามีข้อมูล
            if( count($query) > 0){
                for ($i = 0 ;$i < count($query); $i++){
                    $timestame = strtotime($query[$i]->datetime)-900; //แปลงเวลาเป็น timestamse เเล้ว ลบ 840 คือลบออกไป 14 นาที
                    $converttime15 = date('Y-m-d H:i:s',$timestame); //แปลงเวลาจาก timestamp เป็น  เวลาปกติ
                    $getsettinghigh = DB::table('device')->where('key_device','=',$query[$i]->key_device_15minutes)->pluck('settingHight');
                    $getsettinglow = DB::table('device')->where('key_device','=',$query[$i]->key_device_15minutes)->pluck('settingLow');
                    $countmoresettinghigh = DB::table('data_1minutes')->whereBetween('timedate',[$converttime15,$query[$i]->datetime])->where('key_device_1minutes','=',$query[$i]->key_device_15minutes)->where('irm_value_1minutes','>',$getsettinghigh[0])->where('irm_value_1minutes','!=',0.00)->count();
                    $countmoresettinglow  = DB::table('data_1minutes')->whereBetween('timedate',[$converttime15,$query[$i]->datetime])->where('key_device_1minutes','=',$query[$i]->key_device_15minutes)->where('irm_value_1minutes','<',$getsettinglow[0])->where('irm_value_1minutes','!=',0)->count();
                    $mindata     =  DB::table('data_1minutes')->whereBetween('timedate',[$converttime15,$query[$i]->datetime])->where('key_device_1minutes','=',$query[$i]->key_device_15minutes)->where('irm_value_1minutes','!=',0)->min('irm_value_1minutes');
                    $maxdata     =  DB::table('data_1minutes')->whereBetween('timedate',[$converttime15,$query[$i]->datetime])->where('key_device_1minutes','=',$query[$i]->key_device_15minutes)->where('irm_value_1minutes','!=',0)->max('irm_value_1minutes');
                    $sumcountmoresettinghigh  =  DB::table('data_1minutes')->whereBetween('timedate',[$converttime15,$query[$i]->datetime])->where('key_device_1minutes','=',$query[$i]->key_device_15minutes)->where('irm_value_1minutes','>',$getsettinghigh[0])->sum('irm_value_1minutes');

                    $manage['RACK'][] = Array(
                        'keyrack'   =>  $query[$i]->key_rack,
                        'keydevice' =>  $query[$i]->key_device_15minutes,
                        'datetimeend'  =>  $query[$i]->datetime,
                        'countmoresettinghigh' => $countmoresettinghigh,
                        'countmoresettinglow' => $countmoresettinglow,
                        'min' => $mindata,
                        'max' =>   $maxdata,
                        'sumcountmoresettinghigh' =>  $sumcountmoresettinghigh - (16 *$countmoresettinghigh),
                    );
                }
            }else{
                #ถ้าไม่มีข้อมูลให้ส่งค่า manage ว่างเปล่าออกไปเเล้วไปเขียน ดัก อีกที ใน หน้า view
                $manage['RACK'][] = Array();

            }


            return view('user/tecnical15m',compact('color','data','query','manage','querylastnotification'));

        }
         //ถ้าผู้ใช้เลือก 1 อยากดูเเบบ 24 ชั่วโมง
        else{


            $query  =  DB::table('data_24h')->whereBetween('datetime',[$startdatetime,$enddatetime])->select('key_rack','key_device_24h','datetime')->where('key_rack','=',$keyrack)->get();
            #ถ้ามีข้อมูล
            if( count($query) > 0){
                for ($i = 0 ;$i < count($query); $i++){

                    $timestame = strtotime($query[$i]->datetime)-86400; //แปลงเวลาเป็น timestamse เเล้ว ลบ 86400 คือลบออกไป 1 วัน
                    $converttime24 = date('Y-m-d H:i:s',$timestame); //แปลงเวลาจาก timestamp เป็น  เวลาปกติ
                    $getsettinghigh = DB::table('device')->where('key_device','=',$query[$i]->key_device_24h)->pluck('settingHight');
                    $getsettinglow = DB::table('device')->where('key_device','=',$query[$i]->key_device_24h)->pluck('settingLow');
                    $countmoresettinghigh = DB::table('data_1minutes')->whereBetween('timedate',[$converttime24,$query[$i]->datetime])->where('key_device_1minutes','=',$query[$i]->key_device_24h)->where('irm_value_1minutes','>',$getsettinghigh[0])->count();
                    $countmoresettinglow  = DB::table('data_1minutes')->whereBetween('timedate',[$converttime24,$query[$i]->datetime])->where('key_device_1minutes','=',$query[$i]->key_device_24h)->where('irm_value_1minutes','<',$getsettinglow[0])->count();
                    $mindata     =  DB::table('data_1minutes')->whereBetween('timedate',[$converttime24,$query[$i]->datetime])->where('key_device_1minutes','=',$query[$i]->key_device_24h)->min('irm_value_1minutes');
                    $maxdata     =  DB::table('data_1minutes')->whereBetween('timedate',[$converttime24,$query[$i]->datetime])->where('key_device_1minutes','=',$query[$i]->key_device_24h)->max('irm_value_1minutes');
                    $sumcountmoresettinghigh  =  DB::table('data_1minutes')->whereBetween('timedate',[$converttime24,$query[$i]->datetime])->where('key_device_1minutes','=',$query[$i]->key_device_24h)->where('irm_value_1minutes','>',$getsettinghigh[0])->sum('irm_value_1minutes');

                    $manage['RACK'][] = Array(
                        'keyrack'   =>  $query[$i]->key_rack,
                        'keydevice' =>  $query[$i]->key_device_24h,
                        'datetimeend'  =>  $query[$i]->datetime,
                        'countmoresettinghigh' => $countmoresettinghigh,
                        'countmoresettinglow' => $countmoresettinglow,
                        'min' => $mindata,
                        'max' =>   $maxdata,
                        'sumcountmoresettinghigh' =>  $sumcountmoresettinghigh - (16 *$countmoresettinghigh),
                    );
                }
            }else{
                #ถ้าไม่มีข้อมูลให้ส่งค่า manage ว่างเปล่าออกไปเเล้วไปเขียน ดัก อีกที ใน หน้า view
                $manage['RACK'][] = Array();

            }





            return view('user/tecnical24h',compact('color','data','query','manage','querylastnotification'));

        }



    }

    #หน้าการสรุปค่ากระเเสไฟ
    public function searchreport()
    {

        //คิวรีการเเจ้งเตือนการอัพเดทต้องมีทุกหน้า
        $querylastnotification =  DB::table('newnotificationupdate')->orderBy('id', 'DESC')->get()->first();

         //สีของการเเจ้งเตือน
        $color_normal = DB::table('setting_notification')->where('rang','=',0)->pluck('color');
        $color_low = DB::table('setting_notification')->where('rang','=',1)->pluck('color');
        $color_high = DB::table('setting_notification')->where('rang','=',2)->pluck('color');
        $color_critical = DB::table('setting_notification')->where('rang','=',3)->pluck('color');
        $color =[
                'color_normal' =>  $color_normal[0],
                'color_low'  =>  $color_low[0],
                'color_high'  =>   $color_high[0],
                'color_critical' => $color_critical[0]
        ];

         //option รหัสตู้เเร็ค
         $optionkeyrack = DB::table('table_rack')->select('key_rack_setting')->where('status','=',1)->where('key_rack_setting','!=','#P3-100000')->get();

        return view('user/searchreport',compact('color','optionkeyrack','querylastnotification'));
    }


     #เอาไว้รับ request จาก การสรุปค่ากระเเสไฟ
     public function report(Request $request){

        //คิวรีการเเจ้งเตือนการอัพเดทต้องมีทุกหน้า
        $querylastnotification =  DB::table('newnotificationupdate')->orderBy('id', 'DESC')->get()->first();

        //สีของการเเจ้งเตือน
        $color_normal = DB::table('setting_notification')->where('rang','=',0)->pluck('color');
        $color_low = DB::table('setting_notification')->where('rang','=',1)->pluck('color');
        $color_high = DB::table('setting_notification')->where('rang','=',2)->pluck('color');
        $color_critical = DB::table('setting_notification')->where('rang','=',3)->pluck('color');
        $color =[
                'color_normal' =>  $color_normal[0],
                'color_low'  =>  $color_low[0],
                'color_high'  =>   $color_high[0],
                'color_critical' => $color_critical[0]
        ];



        $startdatetime =   $request->get('startdate')." 00:00:00";
        $enddatetime =  $request->get('enddate')." 23:59:00";
        $keyrack =  $request->get('keyrack');

        $startdate =  $request->get('startdate');
        $enddate =  $request->get('enddate');




        $data = [
                        'startdate' =>  $startdate,
                        'enddate'   =>  $enddate,
                        'keyrack'    =>   $keyrack,
                ];


        #ถ้าผู้ใช้เลือกทั้งหมด จะทำการคิวรีการเเจ้งเตือนย้อนหลังทั้งหมด
        if($keyrack == "all"){
            set_time_limit(0);    
            $querykeyrackall = DB::table('table_rack')->where('key_rack_setting','!=','#P3-100000')->where('status','=',1)->pluck('key_rack_setting');
            set_time_limit(0);
            $namekey_rack    = DB::table('table_rack')->where('key_rack_setting','!=','#P3-100000')->where('status','=',1)->pluck('Ownwer');

            $array = array();
            for ($x = 0 ;$x < count($querykeyrackall); $x++){
                    set_time_limit(0);
                    $A = DB::table('data_1minutes')->whereBetween('timedate',[$startdatetime,$enddatetime])->where('key_rack','=',$querykeyrackall[$x])->where('irm_value_1minutes','!=',0.00)->get()->avg('irm_value_1minutes');

                    $p  =  $A* 220;
                    $Amp = $A;

                    $KWh_per_mont =  ($p * 30 * 24)/1000;

                    $manage['RACK'][] = Array(
                        'key_rack' => $querykeyrackall[$x],
                        'name_keyrack' => $namekey_rack[$x],
                        'Amp' => $Amp,
                        'KWh_per_mont' => $KWh_per_mont,

                    );

             }


            return view('user/report',compact('color','data','manage','querylastnotification'));

        }else if($keyrack != "falied"){
            set_time_limit(0);
            $namekey_rack    = DB::table('table_rack')->where('key_rack_setting','!=','#P3-100000')->where('status','=',1)->pluck('Ownwer');
            set_time_limit(0);
            $A = DB::table('data_1minutes')->whereBetween('timedate',[$startdatetime,$enddatetime])->where('key_rack','=',$keyrack)->where('irm_value_1minutes','!=',0.00)->get()->avg('irm_value_1minutes');

            $p  =  $A* 220;
            $Amp = $A;
            $KWh_per_mont =  ($p * 30 * 24)/1000;



            $manage['RACK'][] = Array(
                'key_rack' =>  $keyrack,
                'name_keyrack' => $namekey_rack[0] ,
                'Amp' => $Amp,
                'KWh_per_mont' => $KWh_per_mont,

            );


            return view('user/report',compact('color','data','manage','querylastnotification'));
        }else{


          // ถ้าไม่มีรหัสตู่้เเร็ค ก็ให้ ไป หน้า report 2 เเทน

            return view('user/report2',compact('color','data','querylastnotification'));

        }







     }

    #กระเเสไฟสูง
    public function currentAlamhigh()
    {

        //คิวรีการเเจ้งเตือนการอัพเดทต้องมีทุกหน้า
        $querylastnotification =  DB::table('newnotificationupdate')->orderBy('id', 'DESC')->get()->first();

         //สีของการเเจ้งเตือน
        $color_normal = DB::table('setting_notification')->where('rang','=',0)->pluck('color');
        $color_low = DB::table('setting_notification')->where('rang','=',1)->pluck('color');
        $color_high = DB::table('setting_notification')->where('rang','=',2)->pluck('color');
        $color_critical = DB::table('setting_notification')->where('rang','=',3)->pluck('color');
        $color =[
                'color_normal' =>  $color_normal[0],
                'color_low'  =>  $color_low[0],
                'color_high'  =>   $color_high[0],
                'color_critical' => $color_critical[0]
        ];


        $queryhigh = device::where('degree','=',1)->where('key_rack','!=','#P3-100000')->where('status','=',1)->get();




        return view('user/currentAlamhigh',compact('color','queryhigh','querylastnotification'));
    }


    #กระเเสไฟต่ำ
    public function currentAlamlow()
    {

        //คิวรีการเเจ้งเตือนการอัพเดทต้องมีทุกหน้า
        $querylastnotification =  DB::table('newnotificationupdate')->orderBy('id', 'DESC')->get()->first();

         //สีของการเเจ้งเตือน
        $color_normal = DB::table('setting_notification')->where('rang','=',0)->pluck('color');
        $color_low = DB::table('setting_notification')->where('rang','=',1)->pluck('color');
        $color_high = DB::table('setting_notification')->where('rang','=',2)->pluck('color');
        $color_critical = DB::table('setting_notification')->where('rang','=',3)->pluck('color');
        $color =[
                'color_normal' =>  $color_normal[0],
                'color_low'  =>  $color_low[0],
                'color_high'  =>   $color_high[0],
                'color_critical' => $color_critical[0]
        ];

        $querylow = device::where('degree','=',2)->where('key_rack','!=','#P3-100000')->where('status','=',1)->get();


        return view('user/currentAlamlow',compact('color','querylow','querylastnotification'));
    }

    #กระเเสไฟมีปัญหา
    public function currentAlamprom()
    {

        //คิวรีการเเจ้งเตือนการอัพเดทต้องมีทุกหน้า
        $querylastnotification =  DB::table('newnotificationupdate')->orderBy('id', 'DESC')->get()->first();

         //สีของการเเจ้งเตือน
        $color_normal = DB::table('setting_notification')->where('rang','=',0)->pluck('color');
        $color_low = DB::table('setting_notification')->where('rang','=',1)->pluck('color');
        $color_high = DB::table('setting_notification')->where('rang','=',2)->pluck('color');
        $color_critical = DB::table('setting_notification')->where('rang','=',3)->pluck('color');
        $color =[
                'color_normal' =>  $color_normal[0],
                'color_low'  =>  $color_low[0],
                'color_high'  =>   $color_high[0],
                'color_critical' => $color_critical[0]
        ];

        $querycritical  = device::where('degree','=',3)->where('key_rack','!=','#P3-100000')->where('status','=',1)->get();

        return view('user/currentAlamprom',compact('color','querycritical','querylastnotification'));
    }

    #ประวัติกระเเสไฟย้อนหลัง
    public function searchhistorycurrent()
    {

        //คิวรีการเเจ้งเตือนการอัพเดทต้องมีทุกหน้า
        $querylastnotification =  DB::table('newnotificationupdate')->orderBy('id', 'DESC')->get()->first();

         //สีของการเเจ้งเตือน
        $color_normal = DB::table('setting_notification')->where('rang','=',0)->pluck('color');
        $color_low = DB::table('setting_notification')->where('rang','=',1)->pluck('color');
        $color_high = DB::table('setting_notification')->where('rang','=',2)->pluck('color');
        $color_critical = DB::table('setting_notification')->where('rang','=',3)->pluck('color');
        $color =[
                'color_normal' =>  $color_normal[0],
                'color_low'  =>  $color_low[0],
                'color_high'  =>   $color_high[0],
                'color_critical' => $color_critical[0]
        ];


        //option รหัสตู้เเร็ค
        $optionkeyrack = DB::table('table_rack')->select('key_rack_setting')->where('status','=',1)->where('key_rack_setting','!=','#P3-100000')->get();

        return view('user/searchhistorycurrent',compact('color','optionkeyrack','querylastnotification'));
    }



    #เอาไว้รับ request จาก การค้นหา ประวัติกระเเสไฟย้อนหลัง
    public function historycurrent(Request $request)
    {

        //คิวรีการเเจ้งเตือนการอัพเดทต้องมีทุกหน้า
        $querylastnotification =  DB::table('newnotificationupdate')->orderBy('id', 'DESC')->get()->first();


        //สีของการเเจ้งเตือน
        $color_normal = DB::table('setting_notification')->where('rang','=',0)->pluck('color');
        $color_low = DB::table('setting_notification')->where('rang','=',1)->pluck('color');
        $color_high = DB::table('setting_notification')->where('rang','=',2)->pluck('color');
        $color_critical = DB::table('setting_notification')->where('rang','=',3)->pluck('color');
        $color =[
                'color_normal' =>  $color_normal[0],
                'color_low'  =>  $color_low[0],
                'color_high'  =>   $color_high[0],
                'color_critical' => $color_critical[0]
        ];


        $startdatetime =   $request->get('startdate')." 00:00:00";
        $enddatetime =  $request->get('enddate')." 23:59:00";
        $keyrack =  $request->get('keyrack');

        $startdate =  $request->get('startdate');
        $enddate =  $request->get('enddate');




        $data = [
                        'startdate' =>  $startdate,
                        'enddate'   =>  $enddate,
                        'keyrack'    =>   $keyrack,
                ];



        #ถ้าผู้ใช้เลือกทั้งหมด จะทำการคิวรีการเเจ้งเตือนย้อนหลังทั้งหมด
        if($keyrack == "all"){

            $query  =  DB::table('history_notification')->whereBetween('event_time',[$startdatetime,$enddatetime])->orderby('id_history_notification','desc')->get();

            return view('user/historycurrent',compact('color','data','query','querylastnotification'));

        }else{
            $query  =  DB::table('history_notification')->whereBetween('event_time',[$startdatetime,$enddatetime])->where('key_rack','=',$keyrack)->orderby('id_history_notification','desc')->get();

            return view('user/historycurrent',compact('color','data','query','querylastnotification'));
        }



    }

    #ช่วยเหลือ
    public function help()
    {

        //คิวรีการเเจ้งเตือนการอัพเดทต้องมีทุกหน้า
        $querylastnotification =  DB::table('newnotificationupdate')->orderBy('id', 'DESC')->get()->first();

        //สีของการเเจ้งเตือน
        $color_normal = DB::table('setting_notification')->where('rang','=',0)->pluck('color');
        $color_low = DB::table('setting_notification')->where('rang','=',1)->pluck('color');
        $color_high = DB::table('setting_notification')->where('rang','=',2)->pluck('color');
        $color_critical = DB::table('setting_notification')->where('rang','=',3)->pluck('color');
        $color =[
                'color_normal' =>  $color_normal[0],
                'color_low'  =>  $color_low[0],
                'color_high'  =>   $color_high[0],
                'color_critical' => $color_critical[0]
        ];

        return view('user/help',compact('color','querylastnotification'));
    }




    #API--------------------------

    #api เอาไว้คิวรีว่าในตู้เเร็คที่เลือกมีตัวเเคมป์ไฟกี่ตัว
    public function getcampinrack($id)
    {

        $querynamerack =  DB::table('table_rack')->where('id','=',$id)->pluck('key_rack_setting');

        $queryrack = DB::table('device')->select('id_device','key_device','Name','settingHight','settingLow','status','irm_value')->where('key_rack','=',$querynamerack[0])->get();


        return json_encode(array('data' =>  $queryrack ));
    }

    #api ดึง rack ออกมาว่ามีจำนวน
    public function getrack()
    {
        $queryrack = DB::table('table_rack')->where('key_rack_setting','!=','#P3-100000')->where('status','=',1)->get();


            return json_encode(array('data' =>  $queryrack ));
    }

    #api ดึง ว่าตู้ rack ไหนมี alaram critical บ้าง
     public function  getcriticalinrack()
    {
        $queryrack =  DB::table('table_rack')->where('rang_severity','=',3)->where('status','=',1)->where('key_rack_setting','!=','#P3-100000')->get();


            return json_encode(array('data' =>  $queryrack ));
    }


    #api นับจำนวน สิ่งที่มีปัญหา
    public function   getcountpromble()
    {
            // $current_view = DB::table('current_viwe')->select('id_current_view')->where('auto_submit','=',0)->where('key_rack','!=','#P3-100000')->count();
            // $current_view_clear = DB::table('current_viwe')->select('id_current_view')->where('auto_submit','=',1)->where('key_rack','!=','#P3-100000')->count();
            $current_view = device::select('degree')->where('degree','!=',0)->where('key_rack','!=','#P3-100000')->where('status','=',1)->count();

            $current_view_clear = device::select('degree')->where('degree','=',0)->where('key_rack','!=','#P3-100000')->where('status','=',1)->count();

            $deviceonline = DB::table('devicepcb')->select('id')->where('signal_device','=',1)->count();
            $deviceofline = DB::table('devicepcb')->select('id')->where('signal_device','=',0)->count();
            $countproble = [
                                 'current_view' =>  $current_view,
                                 'current_view_clear' => $current_view_clear,
                                 'deviceonline'=>  $deviceonline,
                                 'deviceofline' => $deviceofline
                            ];

            return json_encode(array('data' =>   [$countproble] ));
    }


    #api คิวรี ความผิดปกติ currentview
    public function   getcurrentviewunclear()
    {


            $current_view = device::where('degree','!=',0)->where('key_rack','!=','#P3-100000')->where('status','=',1)->get();


            return json_encode(array('data' =>  $current_view ));
    }


    #api คิวรี ความผิดปกติ currentview เคลียร์เเล้ว
    public function   getcurrentviewclear()
    {

            $current_view_clear = device::where('degree','=',0)->where('key_rack','!=','#P3-100000')->where('status','=',1)->get();

            return json_encode(array('data' =>  $current_view_clear ));
    }


    #api คิวรี อุปกรณ์วัดกระเเสไฟ pcb ที่ออนไลน์
    public function   getdevicepcbonline()
    {

            $devicepcbonline = DB::table('devicepcb')->where('signal_device','=',1)->get();

            return json_encode(array('data' =>  $devicepcbonline ));
    }

    #api คิวรี อุปกรณ์วัดกระเเสไฟ pcb ที่ออฟไลน์
    public function   getdevicepcbofline()
    {

         $devicepcbofline = DB::table('devicepcb')->where('signal_device','=',0)->get();

         return json_encode(array('data' =>  $devicepcbofline ));
    }


     #api คิวรี ข้อมูล 1 นาที
    public function   getData1Minutes($id){

        $datetimenow = date('Y-m-d H:i:s');
        $timestamp =   Carbon::now()->timestamp;
        $timeinvert = $timestamp - 82800; # 23 ชั่วโมง  #14400  4 ชั่วโมง
        $datetimeconvert  = date("Y-m-d H:i:s", $timeinvert );


        //เอาไว้ไปหา รหัสอุปกรณ์จาก id ที่ได้รับมา
        $getkey_device  =  DB::table('device')->where('id_device','=',$id)->pluck('key_device');


        $stack = array();   // สร้างไว้เก็บตัวแปร array

        $current =  DB::table('data_1minutes')->whereBetween('timedate',[$datetimeconvert,$datetimenow])->where('key_device_1minutes','=',$getkey_device[0])->pluck('irm_value_1minutes');
        $date = DB::table('data_1minutes')->whereBetween('timedate',[$datetimeconvert,$datetimenow])->where('key_device_1minutes','=',$getkey_device[0])->pluck('timedate');

        $temp  = count($current);  // นับจำนวน
        for($i = 0; $i<$temp;$i++){
            $timestamp = strtotime($date[$i]); //แปลง string time to timestamp
            array_push($stack,[intval($timestamp."000"),floatval($current[$i])]);
        }

        return   $stack;

    }


    //  #api จำนวน อลาม สูง  ต่ำ  ร้ายเเรง
    public function   getalamall(){

        $low = device::select('degree')->where('degree','=',2)->where('key_rack','!=','#P3-100000')->where('status','=',1)->count();
        $high = device::select('degree')->where('degree','=',1)->where('key_rack','!=','#P3-100000')->where('status','=',1)->count();
        $critical = device::select('degree')->where('degree','=',3)->where('key_rack','!=','#P3-100000')->where('status','=',1)->count();

    //   $low = DB::select(DB::raw("SELECT * FROM  device   WHERE irm_value >= 0.7  and irm_value <  settingLow  and status = 1 and key_rack != '#P3-100000'"));
    //   $high = DB::select(DB::raw("SELECT * FROM  device   WHERE irm_value > settingHight   and status = 1 and key_rack != '#P3-100000'"));
    //   $critical  = DB::select(DB::raw("SELECT * FROM  device   WHERE irm_value < 0.7  and status = 1 and key_rack != '#P3-100000' and status_irm != 1 "));


      $countalarm = [
                                 'low' => $low,
                                 'high'=>  $high,
                                 'critical' => $critical
                            ];

      return json_encode(array('data' =>  [$countalarm]));

    }



    #api คิวรี 1 นาที ตามที่มีการ ค้นหา
    public function   queryData1Minutefromsearch($startdate,$enddate,$keydevice)
    {

        $startdatetime =  $startdate." 00:00:00";
        $enddatetime =  $enddate." 23:59:00";


        $stack = array();   // สร้างไว้เก็บตัวแปร array

        $current =  DB::table('data_1minutes')->whereBetween('timedate',[$startdatetime,$enddatetime])->where('key_device_1minutes','=',$keydevice)->pluck('irm_value_1minutes');
        $date =  DB::table('data_1minutes')->whereBetween('timedate',[$startdatetime,$enddatetime])->where('key_device_1minutes','=',$keydevice)->pluck('timedate');

        $temp  = count($current);  // นับจำนวน
        for($i = 0; $i<$temp;$i++){
            $timestamp = strtotime($date[$i]); //แปลง string time to timestamp
            array_push($stack,[intval($timestamp."000"),floatval($current[$i])]);
        }

        return  $stack;
    }


    #api เอาไว้ ดูข้อมูลย่อยว่า 15 นาที ผ่านมี 1 นาทีอะไรบ้างที่มีปัญหา
    public function   getdetal15minutes($datetime,$keydevice)
    {


        $timestame = strtotime($datetime)-900; //แปลงเวลาเป็น timestamse เเล้ว ลบ 840 คือลบออกไป 14 นาที

        $converttime15 = date('Y-m-d H:i:s', $timestame); //แปลงเวลาจาก timestamp เป็น  เวลาปกติ


        $seetinghigh  =  DB::table('device')->where('key_device','=',$keydevice)->pluck('settingHight');
        $seetinglow  =  DB::table('device')->where('key_device','=',$keydevice)->pluck('settingLow');


        $data_1minutes =   DB::table('data_1minutes')->whereBetween('timedate',[$converttime15,$datetime])->where('key_device_1minutes','=',$keydevice)
                            ->where(function($query) use ($seetinghigh,$seetinglow ){
                                 $query->where('irm_value_1minutes','>=' ,$seetinghigh)
                                 ->orWhere('irm_value_1minutes', '<=',$seetinglow );
                            })->get();



        return json_encode(array('data' =>   $data_1minutes ));
    }



    #api เอาไว้ ดูข้อมูลย่อยว่า 24 ชั่วโมง ผ่านมี 1 นาทีอะไรบ้างที่มีปัญหา
    public function  getdetal24h($datetime,$keydevice)
    {



        $timestame = strtotime($datetime)-61140; //แปลงเวลาเป็น timestamse เเล้ว ลบ 61140 คือลบออกไป 24 ชั่วโมง

        $converttime15 = date('Y-m-d H:i:s', $timestame); //แปลงเวลาจาก timestamp เป็น  เวลาปกติ

        $seetinghigh  =  DB::table('device')->where('key_device','=',$keydevice)->pluck('settingHight');
        $seetinglow  =  DB::table('device')->where('key_device','=',$keydevice)->pluck('settingLow');

        $data_1minutes =   DB::table('data_1minutes')->whereBetween('timedate',[$converttime15,$datetime])->where('key_device_1minutes','=',$keydevice)
                            ->where(function($query) use ($seetinghigh,$seetinglow ){
                                 $query->where('irm_value_1minutes','>=' ,$seetinghigh)
                                 ->orWhere('irm_value_1minutes', '<=',$seetinglow );
                            })->get();


        return json_encode(array('data' =>   $data_1minutes ));
    }



    #api เอาไว้อัพเดทผู้ใช้ จำพวกเสียง  สี เเละรูปแบบการเเสดงผล
    public function  updateuserlayout($email,Request $request){

            $sound   =  $request->get('statussound');

            #อัพเดทเสียง
            $update =  User::where('email','=',$email)->update(['sound' => $sound ]);

            $notification = array(
                        'message' => 'อัพเดทหัวข้อที่เลือกสำเร็จ',
                        'alert-type' => 'success'
             );



            //บันทึกกิจกรรม ลง log
            $type = ""; //บทบาท
            if(Auth::user()->type == 1){

                $type = "admin";

            }elseif(Auth::user()->type == 2){
                $type = "maintenance";

            }else{

                $type = "user";
            }
            $insertactivity = activitylog::insert(array(
                'name' => Auth::user()->email,
                'message_problem'=> ("updatenormal  sound :".$sound),
                'type' =>  $type
            ));



            return back()->with($notification);
        }


        #api เอาไว้ query ว่าใครกดยอมรับการเเจ้งเตือนเเล้วมั่ง
        public  function  getwhonotificationupdate ($email){

             $checkusersubmitnotification = User::where('email','=',$email)->where('submitnotfication','=',0)->count();
             if($checkusersubmitnotification > 0 ){
                 $status = "NO";
                return json_encode(array('data' =>  $status ));
             }else{

                $status = "YES";
                return json_encode(array('data' =>  $status));
             }

        }

        #api นี้เอาไว้ให้ผู้ใช้กดรับทราบการอัพเดท
        public  function  postupdatenotification($email){

            #อัพเดทการเเจ้งเตือน
            $update =  User::where('email','=',$email)->update(['submitnotfication' => 1 ]);

            $notification = array(
                        'message' => 'ท่านได้กดรับทราบเเล้ว',
                        'alert-type' => 'success'
            );


            //บันทึกกิจกรรม ลง log
            $type = ""; //บทบาท
            if(Auth::user()->type == 1){

                $type = "admin";

            }elseif(Auth::user()->type == 2){
                $type = "maintenance";

            }else{

                $type = "user";
            }
            $insertactivity = activitylog::insert(array(
                'name' => Auth::user()->email,
                'message_problem'=> ("updatenormal  user submit update notification :".$email),
                'type' =>  $type
            ));


              return back()->with($notification);

        }


        #api เอาไว้เช็คว่าผู้ัใช้กดเปิดเสียงหรือไม่ถ้ากดให้ทำการเเจ้งเตือนเสียงนั้น
        public  function  getstatesound($id){

            $checkstatesound = User::where('id','=',$id)->pluck('sound');


             if($checkstatesound[0]  > 0 ){
                 $status = "open";
                return json_encode(array('data' =>  $status ));

             }else{

                $status = "close";
                return json_encode(array('data' =>  $status));

             }

        }



          #api นี้เอาไว้คิวรีข้อมูลเพิ่มเติมของอุปกรณ์วัดในตู้เเร็ค พวกค่าสูง  ค่าต่ำ
          public  function  getmoredeviceonrack($startdate,$endate,$keyrack){

            $startdatetime =   $startdate." 00:00:00";
            $enddatetime =  $endate." 23:59:00";



            $getkeyDevice = DB::table('device')->where('key_rack','=',$keyrack)->orderby('id_device','ASC')->pluck('key_device');




            //ปั้นข้อมูลเพื่อใช้เเสดงรายละเอียด
            $array = array();
            for ($i = 0 ;$i < count($getkeyDevice); $i++){

                   $getsettinghigh = DB::table('device')->where('key_device','=',$getkeyDevice[$i])->pluck('settingHight');
                   $getsettinglow = DB::table('device')->where('key_device','=',$getkeyDevice[$i])->pluck('settingLow');
                   $mindata     =  DB::table('data_1minutes')->whereBetween('timedate',[$startdatetime,$enddatetime])->where('key_device_1minutes','=',$getkeyDevice[$i])->where('irm_value_1minutes','!=',0.00)->min('irm_value_1minutes');
                   $maxdata     =  DB::table('data_1minutes')->whereBetween('timedate',[$startdatetime,$enddatetime])->where('key_device_1minutes','=',$getkeyDevice[$i])->where('irm_value_1minutes','!=',0.00)->max('irm_value_1minutes');
                   $countmoresettinghigh = DB::table('data_1minutes')->whereBetween('timedate',[$startdatetime,$enddatetime])->where('key_device_1minutes','=',$getkeyDevice[$i])->where('irm_value_1minutes','!=',0.00)->where('irm_value_1minutes','>',$getsettinghigh[0])->count();
                   $countmoresettinglow  = DB::table('data_1minutes')->whereBetween('timedate',[$startdatetime,$enddatetime])->where('key_device_1minutes','=',$getkeyDevice[$i])->where('irm_value_1minutes','!=',0.00)->where('irm_value_1minutes','<',$getsettinglow[0])->count();

                   $manage['RACK'][] = Array(
                        'keydevice' =>  $getkeyDevice[$i],
                        'settinghigh' => $getsettinghigh[0],
                        'settinglow' => $getsettinglow[0],
                        'min' =>    $mindata,
                        'max' => $maxdata,
                        'moresettinghigh' => $countmoresettinghigh,
                        'moresettinglow' => $countmoresettinglow
                    );

             }


            return json_encode(array('data' =>  $manage));


          }







    }
