<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class devicepcb extends Model
{
    protected $table = 'devicepcb';
    protected $fillable = ['image','ipdevice','nameDevice'];
}
