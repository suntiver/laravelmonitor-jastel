<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class activitylog extends Model
{
    protected $table = 'activitylog';
    protected $fillable = ['name','message_problem','type'];
}
