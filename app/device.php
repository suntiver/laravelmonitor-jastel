<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class device extends Model
{

    protected $table = 'device';
    protected $fillable = ['id_device','key_device','irm_value','datetime',
                            'status','key_rack','Name','settingHight',
                            'settingLow','degree','status_irm','ipdevice'];

}
