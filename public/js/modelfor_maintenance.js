$(document).on('show.bs.modal','#exampleModal-edit', function (e) {

    var id = $(e.relatedTarget).data('id');
    var name = $(e.relatedTarget).data('name');
    var email = $(e.relatedTarget).data('email');
    var type = $(e.relatedTarget).data('type');
    var status = $(e.relatedTarget).data('status');
    var sound = $(e.relatedTarget).data('sound');

    var model = $(this);

    model.find('#name').val(name);
    model.find('#email').val(email);

    model.find('#status').val(status);
    model.find('#sound').val(sound);




    if(type == 0){
        $("#type").html("<option value='0'> User </option>"+
                        "<option value='2'> Maintance </option>");
    }else{
        $("#type").html("<option value='2'>  Maintance  </option>"+
                        "<option value='0'> User </option>");
    }

    if(status == 1){
            $("#status").html("<option value='1'> เปิดใช้งาน </option>"+
                        "<option value='0'> ปิดใช้งาน </option>");

    }else{
            $("#status").html("<option value='0'>  ปิดใช้งาน  </option>"+
                            "<option value='1'> เปิดใช้งาน  </option>");
    }

    if(sound == 1){
            $("#sound").html("<option value='1'> เปิด </option>"+
                        "<option value='0'>  ปิด </option>");
    }else{
            $("#sound").html("<option value='0'>  ปิด </option>"+
                            "<option value='1'> เปิด  </option>");
    }

     $('#editformuser').attr('action','updateuser/'+id);


 });



 $(document).on('show.bs.modal','#deviceconnectnew', function (e) {

    var id = $(e.relatedTarget).data('id_device');
    var key_device = $(e.relatedTarget).data('key_device');
    var status = $(e.relatedTarget).data('status');


    var model = $(this);

    model.find('#keydevice').val(key_device);

    if(status == 1){
        $("#status").html("<option value='1'> เปิดใช้งาน </option>"+
                    "<option value='0'> ปิดใช้งาน </option>");

    }else{
            $("#status").html("<option value='0'>  ปิดใช้งาน  </option>"+
                            "<option value='1'> เปิดใช้งาน  </option>");
    }

     $('#updatedeviceconnectnew').attr('action','updatedeviceconnectnew/'+id);

 });



 $(document).on('show.bs.modal','#deviceopenfortoreack', function (e) {

    var id = $(e.relatedTarget).data('id');

    var status = $(e.relatedTarget).data('status');



    if(status == 1){
        $("#deviceopenfortoreackkeydevice").html("<option value='1'> เปิดใช้งาน </option>"+
                    "<option value='0'> ปิดใช้งาน </option>");

    }else{
            $("#deviceopenfortoreackkeydevice").html("<option value='0'>  ปิดใช้งาน  </option>"+
                            "<option value='1'> เปิดใช้งาน </option>");
    }

     $('#updeviceopenfortoreack').attr('action','updeviceopenfortoreack/'+id);


 });


 $(document).on('show.bs.modal','#modeldeletedevice', function (e) {

    var id = $(e.relatedTarget).data('id_device');


     $('#formdeletedevice').attr('action','/maintence/deletedevice/'+id);


 });



 $(document).on('show.bs.modal','#modelmoverack', function (e) {

    var id = $(e.relatedTarget).data('id');
    var Low = $(e.relatedTarget).attr('data-Low')
    var High = $(e.relatedTarget).attr('data-High')

    var notification = $(e.relatedTarget).attr('data-notification')

    var model = $(this);

    model.find('#Low').val(Low);
    model.find('#High').val(High);

    
    if(notification == 1){
        $("#notificationSelect").html("<option value='1'> ปิดการเเจ้งเตือน </option>"+
                    "<option value='0'> เปิดการเเจ้งเตือน </option>");

    }else{
            $("#notificationSelect").html("<option value='0'>  เปิดการเเจ้งเตือน  </option>"+
                            "<option value='1'> ปิดการเเจ้งเตือน </option>");
    }


     $('#movedevice').attr('action','../movecamp/'+id);


 });



 $(document).on('show.bs.modal','#modeldeleteuser', function (e) {

    var id = $(e.relatedTarget).data('id');



     $('#deleteuser').attr('action','../admin/deleteuser/'+id);


 });


 $(document).on('show.bs.modal','#Modaledelete', function (e) {
    var id = $(e.relatedTarget).data('id');

     $('#deletedevicepcb').attr('action','../maintence/deletedevicepcb/'+id);

 });



 $(document).on('show.bs.modal','#Modalpopuprack', function (e) {
    var key_rack_setting = $(e.relatedTarget).data('key_rack_setting');
    var Ownwer = $(e.relatedTarget).data('ownwer');
    var id = $(e.relatedTarget).data('id');

    $("#title").empty();
    $("#title").html(" รหัสตู้ Rack : <span style='color:#000' class='font-weight-bold'> "+key_rack_setting+" </span>  ชื่อตู้ :  <span style='color:#000;' class='font-weight-bold'> "+Ownwer+" </span> ->เเสดงรายการวัดกระเเสไฟ");

    fetch('getcampinrack/'+id,{mode: 'cors'})
    .then((response) => {
        return response.json();
    })
    .then((myJson) => {

        var output = "";
        $("#table_tr_campinrack").empty();
        if(myJson.data.length > 0){
            for(var i=0;i < myJson.data.length ;i++){
                output +=   "<tr>"+
                            "<td>"+myJson.data[i]['key_device']+"</td>"+
                            "<td>"+myJson.data[i]['Name']+"</td>"+
                            "<td>"+myJson.data[i]['settingHight']+" แอมป์</td>"+
                            "<td>"+myJson.data[i]['settingLow']+" แอมป์</td>"+
                            "<td> <button  class='btn btn-success' data-toggle='modal' data-target='#Modalchart' data-dismiss='modal'  data-id_device='"+myJson.data[i]['id_device']+"'     data-key_device='"+myJson.data[i]['key_device']+"'> ดูกราฟ</button></td>"+
                            "</tr>";
           }

        }else{
            output +=   "<tr>"+
            "<td colspan='5' style='color:red'> ตู้เเร็คนี้ไม่มีตัววัดกระเเสไฟ </td>"+
            "</tr>";
        }

       $("#table_tr_campinrack").html(output);

    });


 });




 $(document).on('show.bs.modal','#Modalcritical', function (e) {

    var keyrack = $(e.relatedTarget).data('keyrack');
    var datetime = $(e.relatedTarget).data('datetime');

    $("#key_rack").empty();
    $("#key_rack").html(keyrack);
    $("#datetime").empty();
    $("#datetime").html(datetime);


 });




 $(document).on('show.bs.modal','#Modalcurrentunclear', function (e) {

    fetch('getcurrentviewunclear',{mode: 'cors'})
    .then((response) => {
        return response.json();
    })
    .then((myJson) => {
        var output = "";
        $("#table_tr_currenctunclear").empty();

        if(myJson.data.length > 0){

            for(var i=0;i < myJson.data.length ;i++){
                    output +=   "<tr>"+
                                "<td>"+myJson.data[i]['event_time']+"</td>"+
                                "<td>"+myJson.data[i]['key_device_current_view']+"</td>"+
                                "<td>"+myJson.data[i]['key_rack']+"</td>"+
                                "<td>"+myJson.data[i]['irm_value']+" แอมป์</td>"+
                                "<td>"+myJson.data[i]['message']+"</td>"+
                                "</tr>";

            }
        }
        else{
            output +=   "<tr>"+
            "<td colspan='5' style='color:red'>ไม่พบความผิดปกติ </td>"+
            "</tr>";
        }


       $("#table_tr_currenctunclear").html(output);
    });
 });



 $(document).on('show.bs.modal','#Modalcurrentclear', function (e) {

    fetch('getcurrentviewclear',{mode: 'cors'})
    .then((response) => {
        return response.json();
    })
    .then((myJson) => {
        var output = "";
        $("#table_tr_currenctclear").empty();
        if(myJson.data.length > 0){
            for(var i=0;i < myJson.data.length ;i++){
                    output +=   "<tr>"+
                                "<td>"+myJson.data[i]['event_time']+"</td>"+
                                "<td>"+myJson.data[i]['timeclear']+"</td>"+
                                "<td>"+myJson.data[i]['key_device_current_view']+"</td>"+
                                "<td>"+myJson.data[i]['key_rack']+"</td>"+
                                "<td>"+myJson.data[i]['irm_value']+" แอมป์</td>"+
                                "<td>"+myJson.data[i]['message']+"</td>"+
                                "</tr>";
            }
        }else{
                output +=   "<tr>"+
                "<td colspan='6' style='color:red'> ไม่พบความผิดปกติที่เคลียร์เเล้ว </td>"+
                "</tr>";

        }

       $("#table_tr_currenctclear").html(output);
    });
 });



 $(document).on('show.bs.modal','#Modaldeviceonline', function (e) {

    fetch('getdevicepcbonline',{mode: 'cors'})
    .then((response) => {
        return response.json();
    })
    .then((myJson) => {
        var output = "";
        $("#table_tr_device_onlnie").empty();
        if(myJson.data.length > 0){
            for(var i=0;i < myJson.data.length ;i++){
                    output +=   "<tr>"+
                                "<td class='w-25'>"+
                                "<img src='../uploads/devicepcb/"+myJson.data[i]['image']+"' class='img-fluid img-thumbnail' width='190px;' height='190px;'>"+
                                "</td>"+
                                "<td>"+myJson.data[i]['ipdevice']+"</td>"+
                                "<td>"+myJson.data[i]['nameDevice']+"</td>"+
                                "<td>"+myJson.data[i]['datetime']+"</td>"+
                                "<td>ออนไลน์ <i class='fas fa-wifi ml-3' style='color:greenyellow;'></i></td>"+
                                "</tr>";
            }
        }else{

                    output +=   "<tr>"+
                                "<td colspan='5' style='color:red'>ไม่พบอุปกรณ์ที่ออนไลน์ขณะนี้ </td>"+
                                 "</tr>";
        }

       $("#table_tr_device_onlnie").html(output);
    });
 });


 $(document).on('show.bs.modal','#Modaldeviceofline', function (e) {

    fetch('getdevicepcbofline',{mode: 'cors'})
    .then((response) => {
        return response.json();
    })
    .then((myJson) => {
        var output = "";
        $("#table_tr_device_oflnie").empty();
        if(myJson.data.length > 0){
                for(var i=0;i < myJson.data.length ;i++){
                    output +=   "<tr>"+
                                "<td class='w-25'>"+
                                "<img src='../uploads/devicepcb/"+myJson.data[i]['image']+"' class='img-fluid img-thumbnail' width='190px;' height='190px;'>"+
                                "</td>"+
                                "<td>"+myJson.data[i]['ipdevice']+"</td>"+
                                "<td>"+myJson.data[i]['nameDevice']+"</td>"+
                                "<td>"+myJson.data[i]['datetime']+"</td>"+
                                "<td>ออฟไลน์ <i class='fas fa-wifi ml-3' style='color:red;'></i></td>"+
                                "</tr>";
            }
        }else{
            output +=   "<tr>"+
            "<td colspan='5' style='color:red'>ไม่พบอุปกรณ์ที่ออฟไลน์ขณะนี้ </td>"+
             "</tr>";

        }
       $("#table_tr_device_oflnie").html(output);
    });
 });


 $(document).on('show.bs.modal','#Modalchart', function (e) {

    var key_device = $(e.relatedTarget).data('key_device');
    var id_device = $(e.relatedTarget).data('id_device');

    //ส่วนนี้คือส่วน เเสดงผลหัวข้อ บน model
    $("#modelid_keydevice").html(key_device);


    //ส่วนนี้คือส่วน เเสดงผลของกราฟ
    Highcharts.getJSON('getData1Minutes/'+id_device, function (data) {

        if(data.length > 0 ){

            //เอาไว้เก็บกระเเสไฟ
            var temp_irm = [];

            // หาขนาดของ data
            var n = data.length;
            //เอาค่ากระเเสตัวสุดท้ายมาเเสดง
            var b = data[n-1][1];

            //บันทึกกระเเสไฟลง ตัวแปร array
            for(var i =0;i<n;i++){
                temp_irm.push(data[i][1]);
            }

            //หาค่า สูงสุด
            var max = temp_irm.reduce(function(a, b) {
                return Math.max(a, b);
            })

            //หาค่า ต่ำสุด
            var min = temp_irm.reduce(function(a, b) {
                return Math.min(a, b);
            })



            //ส่วนเเสดงผลหน้าเว็บ
            document.getElementById("valuenow").innerHTML = b+" แอมป์";
            document.getElementById("valuemin").innerHTML = min+" แอมป์";
            document.getElementById("valuemax").innerHTML = max+" แอมป์";


            //ส่วนนี้คือส่วน เเสดงผลของกราฟ
            Highcharts.setOptions({
                time: {
                    timezoneOffset: -7 * 60
               }
            });


            Highcharts.stockChart('container', {
                exporting: {
                    enabled: false
                },

                rangeSelector: {

                    inputEnabled: false,
                    selected: 1,
                    buttons: [
                         {
                        type: 'minute',
                        count: 5,
                        text: '5m'
                    }, {
                        type: 'minute',
                        count: 15,
                        text: '15m'
                    },
                    {
                        type: 'minute',
                        count: 30,
                        text: '30m'
                    },
                    {
                        type: 'minute',
                        count: 60,
                        text: '1h'
                    },
                    {
                        type: 'minute',
                        count: 120,
                        text: '2h'
                    },
                    {
                        type: 'minute',
                        count: 180,
                        text: '3h'
                    },
                    {
                        type: 'all',
                        text: 'All'
                    }]
                },

                title: {
                    text: 'กระเเสไฟฟ้า หน่วยเป็นแอมป์'
                },

                // yAxis: {
                //     title: {
                //         text: null
                //     },
                //     plotLines: [{
                //         value:  2,
                //         color: '#7CFC00',
                //         dashStyle: 'shortdash',
                //         width: 2

                //     },{
                //         value: 3,
                //         color: '#FFFF00',
                //         dashStyle: 'shortdash',
                //         width: 2

                //     }, {
                //         value: 3,
                //         color: '#FFA500',
                //         dashStyle: 'shortdash',
                //         width: 2

                //     }]
                // },

                series: [{
                    name: 'กระเเสไฟ',
                    data: data,
                    tooltip: {
                        valueDecimals: 1
                    }
                }]
            });

        }else{
                var output =  "<div class='mt-2'>"+
                              "<center>------ไม่พบข้อมูล------</center>"+
                              "</div>";

             $("#container").html(output);
        }



    });



 });



 $(document).on('show.bs.modal','#Modalshowdetal', function (e) {


    var datetime = $(e.relatedTarget).data('datetime');
    var keydevice = $(e.relatedTarget).data('keydevice');


    fetch('getdetal15minutes/'+encodeURIComponent(datetime)+'/'+encodeURIComponent(keydevice),{mode: 'cors'})
    .then((response) => {
        return response.json();
    })
    .then((myJson) => {

        var output = "";
        $("#table_tr_showdetal").empty();
        if(myJson.data.length > 0){
                for(var i=0;i < myJson.data.length ;i++){
                    output +=   "<tr>"+
                                "<td>"+myJson.data[i]['timedate']+"</td>"+
                                "<td>"+myJson.data[i]['key_rack']+"</td>"+
                                "<td>"+myJson.data[i]['key_device_1minutes']+"</td>"+
                                "<td>"+myJson.data[i]['Name_device']+"</td>"+
                                "<td>"+myJson.data[i]['irm_value_1minutes']+"</td>"+
                                "</tr>";
            }
        }else{
            output +=   "<tr>"+
            "<td colspan='5' style='color:red'>ไม่พบข้อมูลที่คุณต้องการ </td>"+
             "</tr>";

        }
       $("#table_tr_showdetal").html(output);
    });
 });



 $(document).on('show.bs.modal','#Modalshowdetal24h', function (e) {


    var datetime = $(e.relatedTarget).data('datetime');
    var keydevice = $(e.relatedTarget).data('keydevice');


    fetch('getdetal24h/'+encodeURIComponent(datetime)+'/'+encodeURIComponent(keydevice),{mode: 'cors'})
    .then((response) => {
        return response.json();
    })
    .then((myJson) => {

        var output = "";
        $("#table_tr_showdetal24h").empty();
        if(myJson.data.length > 0){
                for(var i=0;i < myJson.data.length ;i++){
                    output +=   "<tr>"+
                                "<td>"+myJson.data[i]['timedate']+"</td>"+
                                "<td>"+myJson.data[i]['key_rack']+"</td>"+
                                "<td>"+myJson.data[i]['key_device_1minutes']+"</td>"+
                                "<td>"+myJson.data[i]['Name_device']+"</td>"+
                                "<td>"+myJson.data[i]['irm_value_1minutes']+"</td>"+
                                "</tr>";
            }
        }else{
            output +=   "<tr>"+
            "<td colspan='5' style='color:red'>ไม่พบข้อมูลที่คุณต้องการ </td>"+
             "</tr>";
        }
       $("#table_tr_showdetal24h").html(output);
    });
 });



 $(document).on('show.bs.modal','#Modalmore', function (e) {


    var startdatetime = $(e.relatedTarget).data('datetimestart');

    var enddatetime   =  $(e.relatedTarget).data('dateenddate');

    var keyrack    =  $(e.relatedTarget).data('keyrack');


    fetch('getmoredeviceonrack/'+encodeURIComponent(startdatetime)+'/'+encodeURIComponent(enddatetime )+'/'+encodeURIComponent(keyrack),{mode: 'cors'})
    .then((response) => {
        return response.json();
    })
    .then((myJson) => {

        var output = "";
        $("#table_tr_more_detal").empty();
        for(var i=0;i<myJson.data['RACK'].length;i++){

            output  +=  "<tr>"+
                                "<td>"+myJson.data['RACK'][i]['keydevice']+"</td>"+
                                "<td>"+myJson.data['RACK'][i]['settinghigh']+" แอมป์</td>"+
                                "<td>"+myJson.data['RACK'][i]['settinglow']+" แอมป์</td>"+
                                "<td>"+myJson.data['RACK'][i]['min']+" แอมป์</td>"+
                                "<td>"+myJson.data['RACK'][i]['max']+" แอมป์</td>"+
                                "<td>"+ myJson.data['RACK'][i]['moresettinghigh'] +" ครั้ง</td>"+
                                "<td>"+ myJson.data['RACK'][i]['moresettinglow'] +" ครั้ง</td>"+
                        "</tr>"

        }

        $("#table_tr_more_detal").html(output);






    });


 });



 $(document).on('show.bs.modal','#Modeleditdevice', function (e) {


    var id = $(e.relatedTarget).data('id');
    var namedevice = $(e.relatedTarget).data('namedevice');
    var ip  =  $(e.relatedTarget).data('ip');

    
    fetch('/maintence/showdatapcb/'+id,{mode: 'cors'})
    .then((response) => {
        return response.json();
    })
    .then((myJson) => {

        $("#setNameDevice").empty();
        $("#setIPdevice").empty();
        $("#setNameDevice").val(namedevice);
        $("#setIPdevice").val(ip);
        $("#setID").val(id);
        
       
    });
  


 });

