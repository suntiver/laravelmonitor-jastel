    var startdate = ($("#startdate").text());
    var enddate = ($("#enddate").text());
    var keydevice= ($("#keydevice").text());


    // //ส่วนนี้คือส่วน เเสดงผลของกราฟ

        Highcharts.getJSON("queryData1Minutefromsearch/"+encodeURIComponent(startdate)+"/"+encodeURIComponent(enddate)+"/"+encodeURIComponent(keydevice), function (data) {

        if(data.length > 0 ){

            //ส่วนนี้คือส่วน เเสดงผลของกราฟ
            Highcharts.setOptions({
                time: {
                    timezoneOffset: -7 * 60
               }
            });


            Highcharts.stockChart('chart_1minutes', {

                exporting: {
                    enabled: false
                },

                rangeSelector: {

                    inputEnabled: false,
                    selected: 1,
                    buttons: [
                         {
                        type: 'minute',
                        count: 5,
                        text: '5m'
                    }, {
                        type: 'minute',
                        count: 15,
                        text: '15m'
                    },
                    {
                        type: 'minute',
                        count: 30,
                        text: '30m'
                    },
                    {
                        type: 'minute',
                        count: 60,
                        text: '1h'
                    },
                    {
                        type: 'minute',
                        count: 120,
                        text: '2h'
                    },
                    {
                        type: 'minute',
                        count: 180,
                        text: '3h'
                    },
                    {
                        type: 'all',
                        text: 'All'
                    }]
                },

                title: {
                    text: 'กระเเสไฟฟ้า หน่วยเป็นแอมป์'
                },


                series: [{
                    name: 'กระเเสไฟ',
                    data: data,
                    tooltip: {
                        valueDecimals: 1
                    }
                }]
            });

        }else{

            $("#chart_1minutes").html("<center><p class='text-muted'><------ไม่พบข้อมูลกราฟ------></p></center>");

        }

    });
