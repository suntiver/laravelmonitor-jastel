@extends('layouts.maintence')

@section('content')
<div class="container-fluid">


    <h1 class="mt-4">ตั้งค่าการเเจ้งเตือน</h1>

    <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">กระเเสไฟต่ำกว่าเกณฑ์ </li>
    </ol>

<!-- Page Heading -->
<div class="row">
    <!-- Area Chart -->

     <div class="col-md-12 mt-3 mb-5 ">


                    <div class="container-fluid" >

                            <div class="row justify-content-center">


                                    <div class="col-md-6 ">
                                        <div class="card">
                                            <div class="card-header">การเเจ้งเตือนเมื่อเกิดไฟฟ้าต่ำกว่าเกณฑ์ล่าสุด
                                            </span></div>

                                            <div class="card-body">


                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-12" >

                                                              <div class="form-group">
                                                                <label for="exampleFormControlInput1">สีสำหรับใช้เเสดงเมื่อเกิดเหตุการณ์ไฟฟ้าต่ำกว่าเกณฑ์</label>
                                                                <select id="mySelect1" data-show-content="true" class="form-control">
                                                                          <option data-content="<i class='fa fa-stop' style='color:{{$querydatacolor->color}};font-size: 25px;'></i> {{$querydatacolor->color}}"></option>

                                                                </select>
                                                              </div>

                                                            </div>

                                                        </div>

                                                        <div class="row">

                                                          <div class="col-md-12">

                                                            <div class="form-group">
                                                                <label for="exampleFormControlTextarea1">เนื้อหาข้อความที่จะเเจ้งเตือน</label>
                                                                <input type="text" class="form-control"  value="{{$querydatacolor->message}}"  maxlength="100" readonly/>
                                                              </div>
                                                           </div>




                                                      </div>


                                                        <div class="form-group">
                                                          <label for="exampleFormControlInput1">วันที่-เวลาการอัพเดทเเจ้งเตือนล่าสุด</label>
                                                          <input type="text" class="form-control"  value="{{$querydatacolor->datetime}}"  maxlength="100"  readonly/>
                                                        </div>



                                                    </div>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 ">
                                        <div class="card">
                                            <div class="card-header">การเเจ้งเตือนเมื่อเกิดไฟฟ้าต่ำกว่าเกณฑ์ </span></div>

                                            <div class="card-body">

                                                <form class="needs-validation" method="post"   action="{{route('maintence\updatelow')}}"  novalidate>
                                                    @csrf

                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-12" >

                                                              <div class="form-group">
                                                                <label for="exampleFormControlInput1">สีสำหรับใช้เเสดงเมื่อเกิดเหตุการณ์ไฟฟ้าต่ำกว่าเกณฑ์</label>
                                                                <select id="mySelect2" data-show-content="true" class="form-control" name="selectcolor">

                                                                    <option data-content="<i class='fa fa-stop' style='color:Gold;font-size: 25px;'></i> Gold" value="Gold"></option>
                                                                    <option data-content="<i class='fa fa-stop' style='color:Yellow;font-size: 25px;'></i> Yellow" value="Yellow"></option>
                                                                    <option data-content="<i class='fa fa-stop' style='color:LemonChiffon;font-size: 25px;'></i> LemonChiffon" value="LemonChiffon"></option>
                                                                    <option data-content="<i class='fa fa-stop' style='color:Khaki;font-size: 25px;'></i> Khaki" value="Khaki"></option>

                                                                </select>
                                                              </div>

                                                            </div>

                                                        </div>

                                                        <div class="row">

                                                          <div class="col-md-12">

                                                            <div class="form-group">
                                                                <label for="exampleFormControlTextarea1">เนื้อหาข้อความที่จะเเจ้งเตือน</label>
                                                                <input type="text" class="form-control"  name="content"  placeholder="กรุณาป้อนข้อความ"  maxlength="30" required>
                                                                <div class="invalid-feedback">
                                                                    กรุณาป้อนเนื้อหาข้อความที่จะเเจ้งเตือน
                                                                </div>
                                                              </div>
                                                           </div>




                                                      </div>


                                                        <div class="container mt-5">
                                                          <div class="row">
                                                            <div class="col text-center">
                                                                      <button type="submit" class="btn btn-primary" role="button"> อัพเดทการเเจ้งเตือน</button>
                                                            </div>
                                                          </div>
                                                     </div>
                                                    </div>

                                                </form>
                                        </div>
                                        </div>
                                    </div>
                                </div>
            </div>

     </div>
  </div>




  </div>





@endsection
