<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>register</title>
        <link href="../css/styles.css" rel="stylesheet" />
        <script src="../js/font-awesome.min.js" crossorigin="anonymous"></script>


        <style>

            body
            {

                background-image: url("img/blue-blur.jpg");
                background-size: cover;
                background-repeat: no-repeat;
                background-position: center;
                background-attachment: fixed;

            }

      </style>

    </head>
    <body >
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h4 class="text-center font-weight-light my-3">สมัครสมาชิกเข้าใช้งาน</h4></div>
                                    <div class="card-body">
                                        <form method="POST" action="{{ route('register') }}" oninput='password_confirmation.setCustomValidity(password_confirmation.value != password.value ? "Passwords do not match." : "")'  enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-row">
                                                <div class="col-md-12">
                                                    <div class="form-group"><label class="small mb-1" for="inputFirstName">ชื่อผู้ใช้ </label><input class="form-control py-4 @error('name') is-invalid @enderror" id="inputFirstName" type="text" placeholder="กรุณาป้อนชื่อ :ไทย หรือ อังกฤษ ก็ได้"   name="name"  required autocomplete="name" autofocus/>

                                                        @error('name')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror

                                                    </div>
                                                </div>

                                            </div>
                                            <div class="form-group"><label class="small mb-1" for="inputEmailAddress">อีเมล์ </label><input class="form-control py-4 @error('email') is-invalid @enderror" id="inputEmailAddress" type="email" aria-describedby="emailHelp" placeholder="กรุณาป้อนอีเมล์"  name="email"  required autocomplete="email"/>
                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group"><label class="small mb-1" for="inputPassword">รหัสผ่าน</label><input class="form-control py-4 @error('password') is-invalid @enderror"   id="inputPassword" type="password" placeholder="กรุณาป้อนรหัสผ่าน" name="password" required autocomplete="new-password" />

                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group"><label class="small mb-1" for="inputConfirmPassword">ยืนยันรหัสผ่าน</label><input class="form-control py-4"  id="password-confirm" type="password" placeholder="ยืนยันรหัสผ่าน"  name="password_confirmation"  required autocomplete="new-password" /></div>
                                                </div>
                                            </div>
                                            <div class="form-group mt-4 mb-0">
                                                <button type="submit" class="btn btn-primary btn-block"  {{ __('Register') }}>
                                                    สมัครสมาชิก
                                                </button>

                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small"><a href="{{route('login')}}">คุณมีแอคเค้าท์อยู่เเล้ว? กลับไปยังหน้าล็อกอิน</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>

        </div>
        <script src="../js/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="../js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="../js/scripts.js"></script>
    </body>
</html>
