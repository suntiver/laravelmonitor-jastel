<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>login</title>
        <link href="../css/styles.css" rel="stylesheet" />
        <script src="../js/font-awesome.min.js" crossorigin="anonymous"></script>

        <style>

                body
                {

                    background-image: url("img/blue-blur.jpg");
                    /* background-color:#f5f5f5; */
                    background-size: cover;
                    background-repeat: no-repeat;
                    background-position: center;
                    background-attachment: fixed;

                }

        </style>
    </head>
    <body >
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center mt-4">
                            <div class="col-lg-5">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4"> เข้าสู่ระบบ Phase 3 </h3></div>
                                    <div class="card-body">
                                        <form method="POST" action="{{ route('login') }}">
                                            @csrf
                                            <div class="form-group"><label class="small mb-1" for="inputEmailAddress">อีเมล์</label><input class="form-control py-4 @error('email') is-invalid @enderror " id="inputEmailAddress" type="email" placeholder="กรุณาป้อนอีเมล์" name="email"  />
                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>กรุณาตรวจสอบอีเมล์</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group"><label class="small mb-1" for="inputPassword">รหัสผ่าน</label><input class="form-control py-4  @error('password') is-invalid @enderror " id="inputPassword" type="password" placeholder="กรุณาป้อนรหัสผ่าน"  name="password" />
                                                @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>กรุณาตรวจสอบรหัสผ่าน</strong>
                                                    </span>
                                                @enderror

                                            </div>
                                            <div class="form-group">
                                                <div class="custom-control custom-checkbox"><input class="custom-control-input" id="rememberPasswordCheck" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} /><label class="custom-control-label" for="rememberPasswordCheck">จำรหัสผ่าน</label></div>
                                            </div>
                                            <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0"><a class="small" href="http://172.16.12.40:5050/">กลับหน้าเลือกเมนู</a>
                                                {{-- <a class="btn btn-primary"  type="submit"> {{ __('Login') }}</a></div> --}}
                                                <button type="submit" class="btn btn-primary"    {{ __('Login') }} >
                                                    เข้าสู่ระบบ
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                    <div class="small"><a href="{{ route('register') }}">คุณต้องการสมัครสมาชิก? โปรดลงชื่อ!</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>

        </div>
        <script src="../js/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="../js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="../js/scripts.js"></script>
    </body>
</html>
