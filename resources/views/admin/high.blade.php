@extends('layouts.admin')

@section('content')
<div class="container-fluid">


    <h1 class="mt-4">ตั้งค่าการเเจ้งเตือน</h1>

    <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">กระเเสไฟสูงกว่าเกณฑ์ </li>
    </ol>

<!-- Page Heading -->
<div class="row">
    <!-- Area Chart -->

     <div class="col-md-12 mt-3 mb-5 ">


                    <div class="container-fluid" >

                            <div class="row justify-content-center">


                                    <div class="col-md-6 ">
                                        <div class="card">
                                            <div class="card-header">การเเจ้งเตือนเมื่อเกิดไฟดับล่าสุด
                                            </span></div>

                                            <div class="card-body">



                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-12" >

                                                              <div class="form-group">
                                                                <label for="exampleFormControlInput1">สีสำหรับใช้เเสดงเมื่อเกิดเหตุการณ์ไฟฟ้าสูงกว่าเกณฑ์</label>
                                                                <select id="mySelect1" data-show-content="true" class="form-control">
                                                                          <option data-content="<i class='fa fa-stop' style='color:{{$querydatacolor->color}};font-size: 25px;'></i> {{$querydatacolor->color}}"></option>

                                                                </select>
                                                              </div>

                                                            </div>

                                                        </div>

                                                        <div class="row">

                                                          <div class="col-md-12">

                                                            <div class="form-group">
                                                                <label for="exampleFormControlTextarea1">เนื้อหาข้อความที่จะเเจ้งเตือน</label>
                                                                <input type="text" class="form-control"  value="{{$querydatacolor->message}}"  maxlength="100" readonly/>
                                                              </div>
                                                           </div>




                                                      </div>


                                                        <div class="form-group">
                                                          <label for="exampleFormControlInput1">วันที่-เวลาการอัพเดทเเจ้งเตือนล่าสุด</label>
                                                          <input type="text" class="form-control"  value="{{$querydatacolor->datetime}}"  maxlength="100"  readonly/>
                                                        </div>



                                                    </div>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 ">
                                        <div class="card">
                                            <div class="card-header">การเเจ้งเตือนเมื่อเกิดไฟฟ้าสูงกว่าเกณฑ์</span></div>

                                            <div class="card-body">
                                                <form class="needs-validation" method="post"   action="{{route('admin\updatehigh')}}"  novalidate>
                                                    @csrf

                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-12" >

                                                              <div class="form-group">
                                                                <label for="exampleFormControlInput1">สีสำหรับใช้เเสดงเมื่อเกิดเหตุการณ์ไฟฟ้าสูงกว่าเกณฑ์</label>
                                                                <select id="mySelect2" data-show-content="true" class="form-control" name="selectcolor" >

                                                                    <option data-content="<i class='fa fa-stop' style='color:orange;font-size: 25px;' ></i>orange" value="orange"></option>
                                                                    <option data-content="<i class='fa fa-stop' style='color:darkorange;font-size: 25px;'></i> darkorange" value="darkorange"></option>
                                                                    <option data-content="<i class='fa fa-stop' style='color:orangered;font-size: 25px;'></i> orangered" value="orangered"></option>
                                                                    <option data-content="<i class='fa fa-stop' style='color:coral;font-size: 25px;'></i> coral" value="coral"></option>

                                                                </select>
                                                              </div>

                                                            </div>

                                                        </div>

                                                        <div class="row">

                                                          <div class="col-md-12">

                                                            <div class="form-group">
                                                                <label for="exampleFormControlTextarea1">เนื้อหาข้อความที่จะเเจ้งเตือน</label>
                                                                <input type="text" class="form-control"  name="content"  placeholder="กรุณาป้อนข้อความ"  maxlength="30" required>
                                                                <div class="invalid-feedback">
                                                                    กรุณาป้อนเนื้อหาข้อความที่จะเเจ้งเตือน
                                                                </div>

                                                            </div>
                                                           </div>




                                                      </div>


                                                        <div class="container mt-5">
                                                          <div class="row">
                                                            <div class="col text-center">
                                                                      <button type="submit" class="btn btn-primary" role="button"> อัพเดทการเเจ้งเตือน</button>
                                                            </div>
                                                          </div>
                                                     </div>
                                                    </div>

                                                </form>
                                        </div>
                                        </div>
                                    </div>
                                </div>
            </div>

     </div>
  </div>




  </div>





@endsection
