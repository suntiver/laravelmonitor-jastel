@extends('layouts.admin')

@section('content')
<div class="container-fluid">


    <h1 class="mt-4"> จัดการสถานะอุปกรณ์</h1>

    <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"> จัดการสถานะอุปกรณ์ </li>
    </ol>




              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">

                  <h6 class="m-0 font-weight-bold ">สถานะอุปกรณ์ ทั้งหมด</h6>
                <!-- <button class="btn btn-primary"  data-toggle="modal" data-target="#Modeladddevicepcb"> <i class="fas fa-plus-circle"></i> เพิ่ม อุปกรณ์ </button> -->
                </div>


                <!-- Card Body -->
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="example" >
                      <thead>
                        <tr style="text-align: center;">
                                <th scope="col">ลำดับ</th>
                                <th scope="col">รูปภาพ</th>
                                <th scope="col">ไอพีอุปกรณ์วัด</th>
                                <th scope="col">ชื่ออุปกรณ์</th>
                                <th scope="col">อัพเดทล่าสุด</th>
                                <th scope="col">สถานะ</th>
                                <th scope="col"></th>
                                <th scope="col"></th>


                        </tr>
                      </thead>
                      <tfoot>
                        <tr style="text-align: center;">
                                <th scope="col">ลำดับ</th>
                                <th scope="col">รูปภาพ</th>
                                <th scope="col">ไอพีอุปกรณ์วัด</th>
                                <th scope="col">ชื่ออุปกรณ์</th>
                                <th scope="col">อัพเดทล่าสุด</th>
                                <th scope="col">สถานะ</th>
                                <th scope="col"></th>
                                <th scope="col"></th>


                         </tr>
                      </tfoot>
                      <tbody  style="text-align: center;">
                        <div style="display: none">
                            {{ $i = 0 }}
                         </div>
                        @foreach($displaydevicepcb  as $value)
                        <tr>
                            <div style="display: none">{{$i += 1}}</div>
                            <th scope="row">{{$i}}</th>
                            <td class="w-25">
                            <img src="{{asset('uploads/devicepcb/' .$value->image)}}" class="img-fluid img-thumbnail" width="150px;" height="150px;" >
                            </td>
                            <td>{{$value->ipdevice}}</td>
                            <td>{{$value->nameDevice}}</td>
                            <td>{{$value->datetime}}</td>
                            @if($value->signal_device == 1)
                                           <td>ออนไลน์  <i class="fas fa-wifi ml-3" style="color:greenyellow;"></i></td>
                            @else
                                            <td>ออฟไลน์  <i class="fas fa-wifi ml-3" style="color:red;"></i></td>
                            @endif

                            <td> <button class="btn btn-warning" role="button"   data-toggle="modal" data-target="#Modeleditdevice" data-id="{{$value->id}}" data-namedevice="{{$value->nameDevice}}" data-ip="{{$value->ipdevice}}"> จัดการ </button></td>
                            <td> <button class="btn btn-danger" role="button"   data-toggle="modal" data-target="#Modaledelete" data-id="{{$value->id}}"> ลบ </button></td>
                        </tr>
                    @endforeach
                      </tbody>
                    </table>
                  </div>

                </div>
              </div>






  </div>


<!-- Modal -->
<!-- <div class="modal fade" id="Modeladddevicepcb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
      <div class="modal-content">

      <form class="needs-validation" method="post"   action="{{route('admin\adddevicepcb')}}"  enctype="multipart/form-data"   novalidate>
        {{csrf_field()}}
        <div class="modal-header">

          <span class="modal-title">เพิ่มบอร์ดวัดกระเเสไฟ </span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label for="exampleInputEmail1">ไอพีอุปกรณ์วัด</label>
                <input type="text" class="form-control"  name="ip" placeholder="ex. 192.168.1.1"   pattern="^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"  required>
                <small  class="form-text text-muted">ต้องกรอกไอพีอุปกรณ์ที่มีอยู่จริงเท่านั้น</small>
                <div class="invalid-feedback">
                    กรุณาป้อนไอพีอุปกรณ์วัด
                </div>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">ชื่ออุปกรณ์วัด</label>
                <input type="text" class="form-control"  name="device" placeholder="กรุณากรอกชื่ออุปกรณ์วัด"  required>
                <small  class="form-text text-muted">ผู้ใช้สามารถตั้งชื่ออุปกรณ์วัดว่าอะไรก็ได้</small>
                <div class="invalid-feedback">
                    กรุณาป้อนชื่ออุปกรณ์วัด
                </div>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">กรุณาอัพโหลดรูปภาพอุปกรณ์</label>

                    <div  class="input-group">
                    <div class="custom-file">
                         <input type="file" name="uploadimg" id="image" class="custom-file-input"  >
                         <label class="custom-file-label" ><i class="fas fa-upload"></i> เลือกไฟล์ภาพจากพีซี</label>

                    </div>
                    </div>

                        <small id="emailHelp" class="form-text text-muted">กรุณาอัพโหลดรูปภาพเพื่อบ่งบอกว่าเป็นอุปกรณชนิดใด</small>
              </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>
          <button type="submit" class="btn btn-primary">บันทึก</button>
        </div>
        </form>
      </div>
    </div>
  </div> -->


<!-- Modal  show data -->
<div class="modal fade" id="Modeleditdevice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

      <form class="needs-validation" method="post"   action="{{route('admin\editpcbname')}}"    enctype="multipart/form-data"   novalidate>
        {{csrf_field()}}
        <div class="modal-header">
          <span class="modal-title">เพิ่มบอร์ดวัดกระเเสไฟ </span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div style="display:none">
                  <input type="text" class="form-control" id="setID"  name="id" > 
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">ไอพีอุปกรณ์วัด</label>
                <input type="text" class="form-control"  name="ip"  id="setIPdevice"  placeholder="ex. 192.168.1.1"   pattern="^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$" readonly>
              
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">ชื่ออุปกรณ์วัด</label>
                <input type="text" class="form-control" id="setNameDevice"  name="nameDevice" placeholder="กรุณากรอกชื่ออุปกรณ์วัด"  required>
                <small  class="form-text text-muted">ผู้ใช้สามารถตั้งชื่ออุปกรณ์วัดว่าอะไรก็ได้</small>
                <div class="invalid-feedback">
                    กรุณาป้อนชื่ออุปกรณ์วัด
                </div>
              </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>
          <button type="submit" class="btn btn-primary">บันทึก</button>
        </div>
        </form>
      </div>
    </div>
  </div> 





 <!-- Modal เวลาที่ผู้ใช้ต้องการลบอุปกรณ์พีซีบี -->
<div class="modal fade" id="Modaledelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
      <div class="modal-content">
        <div class="modal-header">

          <span class="modal-title"> คุณต้องการลบอุปกรณ์นี้ใช่หรือไม่ </span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

            <div class="container">
                <div class="row justify-content-center">

                    <i class="fas fa-times-circle  justify-content-center mt-3 mb-3" style="font-size: 150px;color:#dc3545;"></i>

                </div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>

           <form   method="post"  id="deletedevicepcb">
                @csrf
                <button  class="btn btn-danger">ยืนยันการลบ</button>
            </form>

        </div>
      </div>
    </div>
  </div>




@endsection
