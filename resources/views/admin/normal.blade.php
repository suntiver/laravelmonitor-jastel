@extends('layouts.admin')

@section('content')
<div class="container-fluid">


    <h1 class="mt-4">ตั้งค่าการเเจ้งเตือน</h1>

    <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">กระเเสไฟปกติ </li>
    </ol>

<!-- Page Heading -->
<div class="row">
    <!-- Area Chart -->

     <div class="col-md-12 mt-3 mb-5 ">


                    <div class="container-fluid" >

                            <div class="row justify-content-center">


                                    <div class="col-md-6 ">
                                        <div class="card">
                                            <div class="card-header">การเเจ้งเตือนเมื่อเกิดไฟดับล่าสุด
                                            </span></div>

                                            <div class="card-body">



                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-12" >

                                                              <div class="form-group">
                                                                <label for="exampleFormControlInput1">สีสำหรับใช้เเสดงเมื่อเกิดเหตุการณ์ไฟดับ</label>
                                                                <select id="mySelect1" data-show-content="true" class="form-control">
                                                                          <option data-content="<i class='fa fa-stop' style='color:{{$querydatacolor->color}};font-size: 25px;'></i> {{$querydatacolor->color}}"></option>

                                                                </select>
                                                              </div>

                                                            </div>

                                                        </div>

                                                        <div class="row">

                                                          <div class="col-md-12">

                                                            <div class="form-group">
                                                                <label for="exampleFormControlTextarea1">เนื้อหาข้อความที่จะเเจ้งเตือน</label>
                                                                <input type="text" class="form-control"  value="{{$querydatacolor->message}}"  maxlength="100" readonly/>
                                                              </div>
                                                           </div>




                                                      </div>


                                                        <div class="form-group">
                                                          <label for="exampleFormControlInput1">วันที่-เวลาการอัพเดทเเจ้งเตือนล่าสุด</label>
                                                          <input type="text" class="form-control"  value="{{$querydatacolor->datetime}}"   maxlength="100"  readonly/>
                                                        </div>



                                                    </div>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 ">
                                        <div class="card">
                                            <div class="card-header">การเเจ้งเตือนเมื่อเกิดไฟดับ </span></div>

                                            <div class="card-body">

                                                <form class="needs-validation" method="post"   action="{{route('admin\updatenormal')}}"  novalidate>
                                                    @csrf

                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-12" >

                                                              <div class="form-group">
                                                                <label for="exampleFormControlInput1">สีสำหรับใช้เเสดงเมื่อเกิดเหตุการณ์ไฟดับ</label>
                                                                <select id="mySelect2" data-show-content="true" class="form-control" name="selectcolor">

                                                                    <option data-content="<i class='fa fa-stop' style='color:Chartreuse;font-size: 25px;'></i> Chartreuse" value="Chartreuse"></option>
                                                                    <option data-content="<i class='fa fa-stop' style='color:LimeGreen;font-size: 25px;'></i> LimeGreen" value=" LimeGreen"></option>
                                                                    <option data-content="<i class='fa fa-stop' style='color:GreenYellow;font-size: 25px;'></i> GreenYellow" value="GreenYellow"></option>
                                                                    <option data-content="<i class='fa fa-stop' style='color:LawnGreen;font-size: 25px;'></i> LawnGreen" value="LawnGreen"></option>

                                                                </select>
                                                              </div>

                                                            </div>

                                                        </div>

                                                        <div class="row">

                                                          <div class="col-md-12">

                                                            <div class="form-group">
                                                                <label for="exampleFormControlTextarea1">เนื้อหาข้อความที่จะเเจ้งเตือน</label>
                                                                <input type="text" class="form-control"  name="content"   placeholder="กรุณาป้อนข้อความ"  maxlength="30" required>
                                                                <div class="invalid-feedback">
                                                                    กรุณาป้อนเนื้อหาข้อความที่จะเเจ้งเตือน
                                                                </div>
                                                              </div>
                                                           </div>




                                                      </div>


                                                        <div class="container mt-5">
                                                          <div class="row">
                                                            <div class="col text-center">
                                                                      <button type="submit" class="btn btn-primary" role="button"> อัพเดทการเเจ้งเตือน</button>
                                                            </div>
                                                          </div>
                                                     </div>
                                                    </div>

                                                </form>
                                        </div>
                                        </div>
                                    </div>
                                </div>
            </div>

     </div>
  </div>




  </div>





@endsection
