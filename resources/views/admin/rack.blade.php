@extends('layouts.admin')

@section('content')
<div class="container-fluid">


  <h1 class="mt-4">จัดการตู้เเร็ค</h1>

  <ol class="breadcrumb mb-4">
          <li class="breadcrumb-item active">จัดการตู้เเร็ค</li>
  </ol>


      <div class="row">
          <div class="col-md-7">
          <div  class="sizechartchartstats  box-monitor background-color"   >
                  <div class="very-specific-design" id="very-specific-design" >

                          <div id="rackall">
                              @foreach ($queryrack  as $value)
                              <a  href="{{route('admin\getcampinrack',$value->id)}}" style="text-decoration: none;"  data-placement="top" title="รหัสตู้เเร็ค:{{$value->key_rack_setting}} , ชื่อตู้ :{{$value->Ownwer}}"  >
                                  <div  style="position:{{$value->position}};z-index:{{$value->zindex}};right:{{$value->right_px}};top:{{$value->top_px}};">
                                      @if($value->status == 1)
                                          <div class='pulse' style="background-color:LimeGreen"></div>
                                      @else
                                          <div class='pulse' style="background-color:#fff"></div>
                                      @endif
                                  </div>
                              </a>

                              @endforeach

                          </div>

                          <img src="../img/SERVER-RACK-WITH-COMPONENTS.png"   style="position:relative;z-index:1; width:100%; height:90%;"  >
                  </div>
             


               </div>

          </div>

          <div class="col-md-5">
              <div class="card mb-4">
                  <div class="card-header   d-flex flex-row align-items-center justify-content-between ">

                      <h6 class="m-0  mr-1 " >  <i class="fas fa-table mr-1"></i>รายการอุปกรณ์ </h6>
                      <div class="dropleft no-arrow">

                          <a  style="color:black"  href="" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                            </a>

                          <div class="dropdown-menu">
                              <button class="dropdown-item btn1" type="button"> <i class="fas fa-door-closed"></i> อุปกรณ์ออนไลน์ทั้งหมดตามตู้เเร็ค</button>
                              <button class="dropdown-item btn3" type="button"><i class="fas fa-ad"></i> อุปกรณ์ที่เปิดเเต่ยังไม่ถูกเพิ่มไปในตู้เเร็ค</button>
                              <button class="dropdown-item btn2" type="button"><i class="fas fa-plus-circle"></i>  อุปกรณ์ที่เชื่อมต่อเข้ามาใหม่</button>


                          </div>
                        </div>

                  </div>
                  <div class="card-body">

                      <!-- อุปกรณ์ที่ออนไลน์ทั้งหมด -->
                      <div class=" table-responsive collapse1 deviceonlineall"  >
                      <table class="table table-bordered table-sm " >
                          <thead>
                              <td colspan="4" class="text-center">อุปกรณ์ที่ออนไลน์ทั้งหมดตามตู้เเร็ค [ จำนวน {{count($querydeviceonline)}} ]</td>
                            <tr class="text-center">
                              <th scope="col">รหัสตู้เเร็ค</th>
                              <th scope="col">รหัสอุปกรณ์วัด</th>
                              <th scope="col">กระเเสไฟ</th>
                              <th scope="col">#</th>


                            </tr>
                          </thead>
                          <tfoot>
                              <tr class="text-center">
                                  <th scope="col">รหัสตู้เเร็ค</th>
                                  <th scope="col">รหัสอุปกรณ์วัด</th>
                                  <th scope="col">กระเเสไฟ</th>
                                  <th scope="col">#</th>
                              </tr>
                          </tfoot>
                          <tbody>
                            @if(count($querydeviceonline) > 0)
                              @foreach($querydeviceonline as $value)
                                  <tr class="text-center">
                                      <td>{{$value->key_rack}}</td>
                                      <td>{{$value->key_device}}</td>
                                      <td>{{$value->irm_value}}</td>
                                      <td> <button  class=" btn-danger"  data-toggle="modal" data-target="#modeldeletedevice" data-id_device="{{$value->id_device}}">ลบ</button></td>
                                  </tr>
                               @endforeach
                            @else
                                     <td colspan="4" class="text-center">---ไม่พบข้อมูล---</td>
                            @endif

                          </tbody>
                        </table>
                      </div>

                      <!-- อุปกรณ์ที่ตรวจพบ -->
                      <div class=" table-responsive collapse1 deviceonlinenew" style="display: none;" >
                          <table class="table table-bordered table-sm " >
                              <thead>
                                  <td colspan="4" class="text-center">อุปกรณ์ที่เชื่อมต่อเข้ามาใหม่ [ จำนวน {{count($querydeviceonlinenew)}} ]</td>
                                <tr class="text-center">

                                  <th scope="col">รหัสอุปกรณ์วัด</th>
                                  <th scope="col">กระเเสไฟ</th>
                                   <th scope="col">สถานะ</th>
                                      <th scope="col">#</th>

                                </tr>
                              </thead>
                              <tfoot>
                                  <tr class="text-center">
                                      <th scope="col">รหัสอุปกรณ์วัด</th>
                                      <th scope="col">กระเเสไฟ</th>
                                      <th scope="col">สถานะ</th>
                                          <th scope="col">#</th>

                                  </tr>
                              </tfoot>
                              <tbody>

                                 @if(count($querydeviceonlinenew) > 0)

                                  @foreach($querydeviceonlinenew as $value)
                                      <tr class="text-center">
                                          <td>{{$value->key_device}}</td>
                                          <td>{{$value->irm_value}}</td>
                                          @if($value->status == 1)
                                                  <td><i class="fas fa-circle" style="font-size:10px;color:greenyellow;"></i>เปิด</td>
                                          @else
                                                  <td> <i class="fas fa-circle" style="font-size:10px;color:red;"></i> ปิด</td>
                                          @endif
                                          <td><button data-toggle="modal" data-target="#deviceconnectnew"   data-key_device="{{$value->key_device}}"  data-status="{{$value->status}}" data-id_device="{{$value->id_device}}" class="btn-warning">จัดการ</button></td>
                                      </tr>
                                   @endforeach
                                   @else
                                   <td colspan="4" class="text-center">---ไม่พบข้อมูล---</td>
                                   @endif

                              </tbody>
                            </table>
                          </div>


                      <!-- อุปกรณ์ที่เปิดเเต่ยังไม่ถูกเพิ่มไปในตู้เเร็ค -->
                      <div class=" table-responsive collapse1 querydeviceonlineaddtorack" style="display: none;" >
                          <table class="table table-bordered table-sm " >
                              <thead>
                                  <td colspan="5" class="text-center">อุปกรณ์ที่เปิดเเต่ยังไม่ถูกเพิ่มไปในตู้เเร็ค [ จำนวน {{count($querydeviceonlineaddtorack)}} ]</td>
                                <tr class="text-center">

                                  <th scope="col">รหัสอุปกรณ์วัด</th>
                                  <th scope="col">กระเเสไฟ</th>
                                  <th scope="col">สถานะ</th>
                                  <th scope="col">#</th>
                                  <th scope="col">#</th>


                                </tr>
                              </thead>
                              <tfoot>
                                  <tr class="text-center">
                                      <th scope="col">รหัสอุปกรณ์วัด</th>
                                      <th scope="col">กระเเสไฟ</th>
                                      <th scope="col">สถานะ</th>
                                      <th scope="col">#</th>
                                      <th scope="col">#</th>
                                  </tr>
                              </tfoot>
                              <tbody>
                                  @if(count($querydeviceonlineaddtorack) > 0)

                                  @foreach($querydeviceonlineaddtorack as $value)
                                      <tr class="text-center">
                                          <td>{{$value->key_device}}</td>
                                          <td>{{$value->irm_value}}</td>
                                          @if($value->status == 1)
                                                  <td><i class="fas fa-circle" style="font-size:10px;color:greenyellow;"></i> เปิด</td>
                                          @else
                                                  <td><i class="fas fa-circle" style="font-size:10px;color:red;"></i> ปิด</td>
                                          @endif
                                                 <td><button data-toggle="modal" data-target="#deviceopenfortoreack" data-status="{{$value->status}}"  data-id="{{$value->id_device}}" class="btn-warning">จัดการ</button></td>
                                                 <td>  <button  class="btn-danger"  data-toggle="modal" data-target="#modeldeletedevice" data-id_device="{{$value->id_device}}">ลบ</button></td>
                                          </tr>
                                   @endforeach

                                  @else
                                          <td colspan="5" class="text-center">---ไม่พบข้อมูล---</td>
                                  @endif


                              </tbody>
                            </table>
                          </div>
                  </div>
              </div>



          </div>


      </div>




<!-- Modal  อุปกรณ์ที่เชื่อมต่อเข้ามาใหม่ -->
<div class="modal fade" id="deviceconnectnew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">จัดการสถานะอุปกรณ์</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form   method="post"  enctype="multipart/form-data"  id="updatedeviceconnectnew">
      <div class="modal-body">


              @csrf
          <div class="form-row">
              <div class="col-md-6">
                  <div class="form-group">
                      <label for="exampleFormControlInput1">รหัสอุปกรณ์วัด</label>
                      <input type="text" class="form-control"  name="keydevice"  id="keydevice"   value=""  readonly/>
                       <!-- รับค่ามากจาก  model_query.js -->
                  </div>
          </div>
              <div class="col-md-6">
                  <div class="form-group">
                      <label for="exampleFormControlSelect1">สถานะ</label>

                      <select class="form-control"   name="status"   id="status"   value="">
                              <!-- รับค่ามากจาก  model_query.js -->
                      </select>

                    </div>

              </div>
              </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
        <button type="submit" class="btn btn-primary">บันทึก</button>
      </div>
       </form>
    </div>
  </div>
</div>



<!-- Modal  อุปกรณ์ที่เปิดเเต่ยังไม่ถูกเพิ่มไปในตู้เเร็ค -->
<div class="modal fade" id="deviceopenfortoreack" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" >กรุณาเลือกตู้เเร็คเพื่อวางอุปกรณ์ลงไป</h5>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form   method="post"  enctype="multipart/form-data"  id="updeviceopenfortoreack">

      <div class="modal-body">

          @csrf

          <div class="form-row">
              <div class="col-md-6">
                  <div class="form-group">
                      <label class="small mb-1" >กรุณาเลือกตู้เเร็คเพื่อวางอุปกรณ์</label>
                      <select class="form-control"  onfocus='this.size=3;' onblur='this.size=1;' onchange='this.size=1; this.blur();' name="selectrack">
                          <option value="#P3-100000">---กรุณาเลือกตู้เเร็ค---</option>
                          @foreach($anotherRack as $value)
                                  <option>{{$value->key_rack_setting}}</option>
                          @endforeach
                      </select>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                      <label class="small mb-1" >สถานะ</label>
                      <select class="form-control"    id="deviceopenfortoreackkeydevice" name="status" >
                           <!-- รับค่ามากจาก  model_query.js -->

                      </select>
                  </div>
              </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
        <button type="submit" class="btn btn-primary">บันทึก</button>
      </div>

      </form>

    </div>
  </div>
</div>

<!-- Modal เวลาที่ผู้ใช้อยากลบอุปกรณ์วัด -->
<div class="modal fade" id="modeldeletedevice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered " role="document">
    <div class="modal-content">
      <div class="modal-header">

        <span class="modal-title"> คุณต้องการลบอุปกรณ์นี้ใช้หรือไม่? </span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

          <div class="container">
              <div class="row justify-content-center">

                  <i class="fas fa-times-circle  justify-content-center mt-3 mb-3" style="font-size: 150px;color:#dc3545;"></i>

              </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>

          <form   method="post"  id="formdeletedevice">
              @csrf
              <button  class="btn btn-danger">ยืนยันการลบ</button>
          </form>

      </div>
    </div>
  </div>
</div>






</div>








@endsection
