@extends('layouts.admin')

@section('content')
<div class="container-fluid">



    <h1 class="mt-4">จัดการผู้ใช้</h1>

    <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">จัดการผู้ใช้</li>
   </ol>
   <div class="table-responsive">
   <table class="table table-bordered" id="example" width="100%" cellspacing="0">
    <thead>
        <tr class="text-center">
            <th>ชื่อผู้ใช้</th>
            <th>อีเมล์</th>
            <th>บทบาท</th>
            <th>การเข้าใช้งาน</th>
            <th>สถานะเสียง</th>
            <th>สถานะเข้าใช้งาน</th>
            <th></th>
            <th></th>

        </tr>
    </thead>
    <tbody>
        @foreach($queryuser  as $value)
            <tr class="text-center">
                <td>{{$value->name}}</td>
                <td>{{$value->email}}</td>

                @if($value->type == 0)
                    <td>User</td>
                @else
                    <td>Maintance</td>
                @endif

                @if($value->status == 1)

                        <td>  <i class="fas fa-circle" style="font-size:10px;color:greenyellow;"></i>  เปิดใช้งาน</td>
                @else
                        <td > <i class="fas fa-circle" style="font-size:10px;color:red;"></i> ปิดใช้งาน</td>

                @endif

                @if($value->sound == 1)

                     <td> <i class="fas fa-circle" style="font-size:10px;color:greenyellow;"></i> เปิดเสียง</td>
                @else
                     <td> <i class="fas fa-circle" style="font-size:10px;color:red;"></i> ปิดเสียง</td>

                @endif

                @if(Cache::has('user-is-online-' . $value->id))
                    <td > <i class="fas fa-circle" style="font-size:10px;color:greenyellow;"></i> ออนไลน์</td>
                @else
                    <td > <i class="fas fa-circle" style="font-size:10px;color:red;"></i> ออฟไลน์</td>
                @endif

                <td><button  class="btn btn-warning" data-toggle="modal" data-target="#exampleModal-edit"  data-id="{{$value->id}}"  data-name="{{$value->name}}"   data-email="{{$value->email}}"  data-type="{{$value->type}}"  data-status="{{$value->status}}"  data-sound="{{$value->sound}}">จัดการ</button></td>
                <td> <button  class="btn btn-danger"   data-toggle="modal" data-target="#modeldeleteuser"  data-id="{{$value->id}}">ลบ</button>


                </td>

            </tr>



        @endforeach

    </tbody>
    <tfoot>
        <tr class="text-center">
            <th>ชื่อผู้ใช้</th>
            <th>อีเมล์</th>
            <th>บทบาท</th>
            <th>การเข้าใช้งาน</th>
            <th>สถานะเสียง</th>
            <th>สถานะเข้าใช้งาน</th>
            <th></th>
            <th></th>

        </tr>
    </tfoot>
</table>
</div>
</div>




 <!-- Modal exampleModal-edit -->
 <div class="modal fade" id="exampleModal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">ข้อมูลสมาชิก</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
            <form   method="post"  enctype="multipart/form-data"  id="editformuser">

                    @csrf
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleFormControlInput1">ชื่อผู้ใช้</label>
                                <input type="text" class="form-control"  name="name"  id="name"  placeholder="ชื่อผู้ใช้" value="" >
                              </div>
                    </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">บทบาท</label>

                                <select class="form-control"   name="type"   id="type"   value="">
                                        <!-- รับค่ามากจาก  model_query.js -->
                                </select>

                              </div>

                        </div>
                        </div>


                    <div class="form-group">
                        <label for="exampleFormControlInput1">อีเมล์</label>
                        <input type="text" class="form-control"  name="email"  id="email"  placeholder="innovation@jastel.com" value="" readonly/>
                      </div>
                        <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">การเข้าใช้งาน</label>
                                                <select class="form-control"   name="status"   id="status">

                                                             <!-- รับค่ามากจาก  model_query.js -->

                                                    </select>
                                              </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">สถานะเสียง</label>
                                                <select class="form-control"  name="sound"   id="sound" >
                                                              <!-- รับค่ามากจาก  model_query.js -->
                                                </select>
                                            </div>
                                        </div>
                          </div>
                         <a href="#" data-toggle="modal" data-target="#modelrestpassword"  data-dismiss="modal">แก้ไขรหัสผ่าน</a>

            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
            <button type="submit" class="btn btn-primary">บันทึก</button>
            </div>
        </form>
        </div>
    </div>
</div>




<!-- Modal  resetpassword-->
<div class="modal fade" id="modelrestpassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">แก้ไขรหัสผ่าน</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="#" oninput='password_confirmation.setCustomValidity(password_confirmation.value != password.value ? "Passwords do not match." : "")'    enctype="multipart/form-data">
        <div class="modal-body">
            <div class="form-row">
                <div class="col-md-12">
                    <div class="form-group"><label class="small mb-1" for="inputPassword">รหัสผ่านใหม่</label><input class="form-control py-4 @error('password') is-invalid @enderror" id="inputPassword" type="password" placeholder="กรุณาป้อนรหัสผ่าน" name="password" required autocomplete="new-password" />
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-12">
                    <div class="form-group"><label class="small mb-1" for="inputConfirmPassword">ยืนยันรหัสผ่าน</label><input class="form-control py-4"  id="password-confirm" type="password" placeholder="ยืนยันรหัสผ่าน"  name="password_confirmation"  required autocomplete="new-password" />
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
          <button type="บันทึก" class="btn btn-primary">Save changes</button>
        </div>
        </form>

      </div>
    </div>
  </div>




   <!-- Modal เวลาที่ผู้ใช้อยากลบผู้ใช้ -->
<div class="modal fade" id="modeldeleteuser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
      <div class="modal-content">
        <div class="modal-header">

          <span class="modal-title"> คุณต้องการลบผู้ใช้นี้ใช้หรือไม่? </span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

            <div class="container">
                <div class="row justify-content-center">

                    <i class="fas fa-times-circle  justify-content-center mt-3 mb-3" style="font-size: 150px;color:#dc3545;"></i>

                </div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>

           <form   method="post"  id="deleteuser">
                @csrf
                <button  class="btn btn-danger">ยืนยันการลบ</button>
            </form>

        </div>
      </div>
    </div>
  </div>

@endsection
