@extends('layouts.admin')

@section('content')
<div class="container-fluid">


    <h1 class="mt-4">เเจ้งเตือนผู้ใช้</h1>

    <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">เเจ้งเตือนผู้ใช้  </li>
    </ol>

<!-- Page Heading -->
<div class="row">
    <!-- Area Chart -->

     <div class="col-md-12 mt-3 mb-5 ">


                    <div class="container-fluid" >

                            <div class="row justify-content-center">


                                    <div class="col-md-6 ">
                                        <div class="card">
                                            <div class="card-header">ข้อความการเเจ้งเตือนล่าสุด</span></div>

                                            <div class="card-body">


                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-12" >

                                                              <div class="form-group">
                                                                <label for="exampleFormControlInput1">หัวข้อการเเจ้งเตือน</label>
                                                              <input type="text" class="form-control" value="{{$querylastnotification->subject}}"  maxlength="100" readonly/>
                                                              </div>

                                                            </div>

                                                        </div>

                                                        <div class="row">

                                                          <div class="col-md-12">

                                                            <div class="form-group">
                                                                <label for="exampleFormControlTextarea1">เนื้อหาข้อความที่จะเเจ้งเตือน</label>
                                                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"   placeholder="{{$querylastnotification->textvalue}}"   readonly></textarea>
                                                              </div>
                                                           </div>




                                                      </div>


                                                        <div class="form-group">
                                                          <label for="exampleFormControlInput1">วันที่-เวลาเเจ้งเตือนล่าสุด</label>
                                                        <input type="text" class="form-control"  value="{{$querylastnotification->datetime}}"  maxlength="100"  readonly/>
                                                        </div>



                                                    </div>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 ">
                                        <div class="card">
                                            <div class="card-header">เเสดงข้อความเเจ้งเตือนผู้ใช้ </span></div>

                                            <div class="card-body">

                                                <form class="needs-validation" method="post"   action="{{route('admin\updatenotificationnew')}}"  novalidate>

                                                    @csrf
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-12" >

                                                              <div class="form-group">
                                                                <label for="exampleFormControlInput1">หัวข้อการเเจ้งเตือน</label>
                                                                <input type="text" class="form-control"  name="subject"  placeholder="กรุณาป้อนหัวข้อ"  maxlength="100" required>

                                                                <div class="invalid-feedback">
                                                                    กรุณาป้อนหัวข้อการเเจ้งเตือน
                                                                </div>

                                                              </div>

                                                            </div>

                                                        </div>

                                                        <div class="row">

                                                          <div class="col-md-12">

                                                            <div class="form-group">
                                                                <label for="exampleFormControlTextarea1">เนื้อหาข้อความที่จะเเจ้งเตือน</label>
                                                                <textarea class="form-control"  name="content"   id="exampleFormControlTextarea1" rows="3" maxlength="200"  placeholder="กรุณาป้อนข้อความที่จะเเจ้งเตือน" required></textarea>

                                                                <div class="invalid-feedback">
                                                                    กรุณาป้อนเนื้อหาข้อความที่จะเเจ้งเตือน
                                                                </div>

                                                            </div>
                                                           </div>




                                                      </div>


                                                        <div class="container mt-5">
                                                          <div class="row">
                                                            <div class="col text-center">
                                                                      <button type="submit" class="btn btn-primary" role="button" > อัพเดทการเเจ้งเตือน</button>
                                                            </div>
                                                          </div>
                                                     </div>
                                                    </div>

                                                </form>
                                        </div>
                                        </div>
                                    </div>
                                </div>
            </div>

     </div>
  </div>




  </div>





@endsection
