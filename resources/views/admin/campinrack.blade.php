@extends('layouts.admin')

@section('content')
<div class="container-fluid">


    <h1 class="mt-4">จัดการตู้เเร็ค</h1>


    <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">จัดการตู้เเร็ค  <span class="text-primary">[ {{$key_rack_setting_value}} ] </span></li>
            <li class="breadcrumb-item active">
                @if($querynamerack->status == 1)
                   <span style="color:LimeGreen">สถานะตู้ เปิด</span>
            @else
                    <span style="color:red">สถานะตู้ ปิด</span>
            @endif

            </li>

            <li class="breadcrumb-item active"> <a href="" data-toggle="modal" data-target="#Modaleditrack">  <i class="fas fa-cog"></i> </a></li>



    </ol>



    <div class="row justify-content-center">

        <div class="col-md-8">
    <div class="table-responsive">
        <table class="table table-bordered" id="example" width="100%" cellspacing="0">
         <thead>
             <tr class="text-center">
                 <th>ลำดับ</th>
                 <th>รหัสอุปกรณ์วัด</th>
                 <th>กระเเสไฟ </th>
                 <th>เกณฑ์ค่าต่ำ </th>
                 <th>เกณฑ์ค่าสูง </th>
                 <th>#</th>
                 <th>#</th>
  

             </tr>
         </thead>
         <tbody>
            <div style="display:none">{{$i= 1}}</div>
            @foreach($queryrack as $value)
                <tr class="text-center">
                        <td>{{$i}}</td>
                        <td>{{$value->key_device}}</td>
                        <td>{{$value->irm_value}} แอมป์</td>
                        <td>{{$value->settingLow}} แอมป์</td>
                        <td>{{$value->settingHight}} แอมป์</td>

                    <td><button  class="btn btn-warning"  data-toggle="modal" data-target="#modelmoverack"    data-id="{{$value->id_device}}"   data-Low="{{$value->settingLow}}"  data-High="{{$value->settingHight}}"  data-notification="{{$value->status_irm}}">จัดการ</button></td>
                   
                    <td><button  class="btn btn-danger"  data-toggle="modal" data-target="#modeldeletedevice" data-id_device="{{$value->id_device}}">ลบ</button> </td>

                </tr>
                    <div style="display:none">{{$i= $i+1}}</div>
            @endforeach

         </tbody>
         <tfoot>
             <tr class="text-center">

                <th>ลำดับ</th>
                <th>รหัสอุปกรณ์วัด</th>
                <th>กระเเสไฟ</th>
                <th>เกณฑ์ค่าต่ำ</th>
                <th>เกณฑ์ค่าสูง</th>

                <th>#</th>
                <th>#</th>
           

             </tr>
         </tfoot>
     </table>
     </div>
    </div>
    <div class="col-md-12">
        <a href="{{route('admin\rack')}}">
            <span><-- ย้อนกลับ</span>
        </a>

    </div>


</div>

</div>



<!-- Modal เวลาที่ผู้ใช้อยากย้ายอุปกรณ์ไปอยู่ตู้เเร็คอื่น -->
<div class="modal fade" id="modelmoverack" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
      <div class="modal-content">
        <div class="modal-header">

          <span class="modal-title"> จัดการอุปกรณ์วัด</span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form   method="post"  id="movedevice">
        <div class="modal-body">
            @csrf
            <div class="form-row">
                <label for="exampleFormControlSelect1">คุณต้องการย้ายอุปกรณ์วัดไปตู้เเร็คใหม่</label>
                <select class="form-control" onfocus='this.size=5;' onblur='this.size=1;' onchange='this.size=1; this.blur();' name="keyrackselect" >
                    <option value="0">----- เลือกปลายทางที่จะไป ----- </option>
                    @foreach ($showrackallformove as $item)

                            <option>{{$item->key_rack_setting}}</option>

                  @endforeach
                            <option value="#P3-100000">ย้ายกลับไปรายการอุปกรณ์</option>

                </select>

                <label for="exampleFormControlSelect1" class="mt-3">ค่าเกณฑ์ในการเเจ้งเตือนกระเเสไฟเมื่อเกิดปัญหา</label>

                 <div class="col-md-6 ">

                    <div class="form-group">
                        <label class="medium mb-1" >ค่าต่ำ  <span style="color:red"> [ ห้ามต่ำกว่า 1 ] </span></label>
                        <div class="input-group ">

                            <input class="form-control "  type="number" pattern="/^-?\d+\.?\d*$/"    step="any"  id="Low"  name="Low">
                            <div class="input-group-append">
                                    <span class="input-group-text"><i class="fas fa-cog"></i></span>
                            </div>
                     </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <label class="medium mb-1" for="inputPassword">เกณฑ์ค่าสูง</label>
                    <div class="input-group ">
                        <input class="form-control " type="number" pattern="/^-?\d+\.?\d*$/"    step="any"  id="High"  name="High">
                        <div class="input-group-append">
                                <span class="input-group-text"><i class="fas fa-cog"></i></span>
                        </div>
                 </div>
                </div>
              </div>
              <div class="form-group">
                  <label for="exampleFormControlSelect1">สถานะการเเจ้งเตือนอุปกรณ์นี้</label>
                  <select class="form-control" id="notificationSelect" name="selectNotification">
                                    
                  </select>
             </div>
        </div>
        <hr/>
        <div class="col-md-12 ">
                <label for="หมายเหตุ" style="color:red">หมายเหตุ เกณฑ์ค่าต่ำที่อุปกรณ์ตั้งไว้คือ 0.4 ต่ำกว่านี้ไม่ได้ ฉะนั่น ถ้าค่ากระเเส น้อย กว่า 1 เเต่ยังไปไม่ถึง 0.4 ระบบจะมองว่ากระเเสไฟต่ำเเต่ยังไม่ร้ายเเรง</label>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>
          <button type="submit" class="btn btn-primary">ยืนยันการย้าย</button>
        </div>
    </form>
      </div>
    </div>
  </div>








  <!-- Modal เวลาที่ผู้ใช้อยากลบอุปกรณ์วัด -->
<div class="modal fade" id="modeldeletedevice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
      <div class="modal-content">
        <div class="modal-header">

          <span class="modal-title"> คุณต้องการลบอุปกรณ์นี้ใช้หรือไม่? </span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

            <div class="container">
                <div class="row justify-content-center">

                    <i class="fas fa-times-circle  justify-content-center mt-3 mb-3" style="font-size: 150px;color:#dc3545;"></i>

                </div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>

            <form   method="post"  id="formdeletedevice">
                @csrf
                <button  class="btn btn-danger">ยืนยันการลบ</button>
            </form>

        </div>
      </div>
    </div>
  </div>



  <!-- Modal -->
<div class="modal fade" id="Modaleditrack" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <form  class="needs-validation" action="{{route('admin\updateRack',$querynamerack->id)}}"  enctype="multipart/form-data"  method="post" novalidate>
            @csrf
        <div class="modal-header">
          <span class="modal-title"> แก้ไขข้อมูลตู้เเร็คนี้ </span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

            <div class="form-group">
                <label for="exampleFormControlInput1">KEY-RACK</label>
            <input type="text" class="form-control" value="{{$querynamerack->key_rack_setting}}"    name="key_rack"  readonly />
              </div>
              <div class="form-group">

                <label for="exampleFormControlInput1">สถานะ Rack  ปัจจุบัน
              </label>
                <select class="form-control" name="status" >
                    @if($querynamerack->status == 1)
                        <option value="1">เปิด</option>
                        <option value="0">ปิด</option>
                    @else
                    <option value="0">ปิด</option>
                    <option value="1">เปิด</option>
                    @endif

                </select>

              </div>
              <div class="form-group">
                <label for="exampleFormControlInput1">ชื่อตู้ Rack</label>
                <input type="text" class="form-control"    placeholder="กรุณาป้อนชื่อ" value="{{$querynamerack->Ownwer}}"  name="Ownwer"  maxlength="20" required>
                <div class="invalid-feedback">
                    กรุณาป้อนหัวข้อการเเจ้งเตือน
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>
          <button type="submit" class="btn btn-primary">บันทึก</button>
        </div>
    </form>
      </div>
    </div>
  </div>


@endsection
