@extends('layouts.admin')

@section('content')
<div class="container-fluid">


    <h1 class="mt-4"> กิจกรรมที่เกิดขึ้น</h1>

    <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"> กิจกรรมที่เกิดขึ้น </li>
    </ol>

    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">

          <h6 class="m-0 font-weight-bold ">กิจกรรมที่เกิดขึ้นทั้งหมด</h6>

        </div>


        <!-- Card Body -->
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered" id="example" >
              <thead>
                <tr style="text-align: center;">
                        <th scope="col">ลำดับ</th>
                        <th scope="col">วันที่-เวลา</th>
                        <th scope="col">บทบาท</th>
                        <th scope="col">อีเมล์</th>
                        <th scope="col">ข้อความ</th>


                </tr>
              </thead>
              <tfoot>
                <tr style="text-align: center;">
                    <th scope="col">ลำดับ</th>
                    <th scope="col">วันที่-เวลา</th>
                    <th scope="col">บทบาท</th>
                    <th scope="col">อีเมล์</th>
                    <th scope="col">ข้อความ</th>


                 </tr>
              </tfoot>
              <tbody  style="text-align: center;">
                <div style="display: none"> {{ $i = 0 }}</div>
                @foreach ($querylog as $item)
                    <tr class="text-center">
                            <div style="display: none">{{$i += 1}}</div>
                            <th scope="row">{{$i}}</th>
                            <td>{{$item->date}}</td>
                            <td>{{$item->type}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->message_problem}}</td>


                    </tr>

                @endforeach
              </tbody>
            </table>
          </div>

        </div>
      </div>





  </div>





@endsection
