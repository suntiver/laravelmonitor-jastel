@extends('layouts.user')

@section('content')
<div class="container-fluid">


    <h4 class="mt-4 text-muted">ข้อมูลกระเเสไฟทุกๆ 1 นาที</h4>

    <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">ข้อมูลกระเเสไฟทุกๆ 1 นาที </li>
            <li class="breadcrumb-item active">วันที่เริ่ม :  <span id="startdate"> {{$data['startdate']}} </span>     วันสิ้นสุด :   <span id="enddate"> {{$data['enddate']}} </span>   </li>
            <li class="breadcrumb-item active">รหัสอุปกรณ์ :  <span id="keydevice">{{$data['keydevice']}}</span> </li>
    </ol>
    <a href="{{route('searchhistory')}}" class="text-decoration-none"><--- ย้อนกลับ</a>
    <div id="chart_1minutes"    >
        <div class="mt-5">
            <center class="text-muted">กรุณารอสักครู่กำลังโหลดข้อมูลกราฟ.....</center>

        </div>
</div>



</div>

@endsection
