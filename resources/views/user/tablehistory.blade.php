@extends('layouts.user')

@section('content')
<div class="row justify-content-center">
    <div class="col-lg-6">
        <div class="card shadow-lg border-0 rounded-lg mt-5">
            <div class="card-header text-center">
                <span class=" font-weight-light  text-muted">ค้นหากระเเสไฟย้อนหลัง ชนิดข้อมูล 1 นาที </span>
            </div>
            <div class="card-body">
                <form method="POST" action="#"  enctype="multipart/form-data">
                    @csrf

                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="medium mb-1" for="inputPassword">วันเริ่มต้น</label>
                                <div class="input-group ">

                                    <input class="form-control"  id="startingDate"    name="startdate"  type="date" value="<?php echo date("Y-m-d"); ?>" id="example-date-input">
                                    <div class="input-group-append">
                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                    </div>
                             </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="medium mb-1" for="inputPassword">วันสิ้นสุด</label>
                            <div class="input-group ">
                                <input class="form-control"  id="endingDate"   name="enddate"  type="date" value="<?php echo date("Y-m-d"); ?>" id="example-date-input">
                                <div class="input-group-append">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                </div>
                         </div>
                        </div>
                    </div>


                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="medium mb-1" for="inputPassword">รหัสตู้เเร็ค</label>
                                <div class="input-group ">

                                    <select class="form-control" onfocus='this.size=5;' onblur='this.size=1;' onchange='this.size=1; this.blur();' name="keyrack">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                      </select>
                                    <div class="input-group-append">
                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                    </div>
                             </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="medium mb-1" for="inputPassword">รหัสอุปกรณ์วัด</label>
                            <div class="input-group ">
                                <select class="form-control" onfocus='this.size=5;' onblur='this.size=1;' onchange='this.size=1; this.blur();' name="keydevice">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                  </select>
                                <div class="input-group-append">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                </div>
                         </div>
                        </div>
                    </div>
                    <div class="form-group mt-4 mb-0">
                        <button type="submit" class="btn btn-primary btn-block" >
                            ค้นหาข้อมูลรูปแบบตาราง
                        </button>

                    </div>
                </form>
            </div>
            <div class="card-footer text-center  ">
                <div class="small" > <span class="font-weight-bold" style="color:red"> **คำเเนะนำ  </span> <span class="text-muted"> การค้นหาข้อมูลกระเเสไฟมากกว่า 1 เดือน อาจใช้เวลานาน </span>  </div>
            </div>
        </div>
    </div>
</div>


@endsection
