@extends('layouts.user')

@section('content')
<div class="container-fluid">

    <h4 class="mt-4 text-muted">ข้อมูลกระเเสไฟทุกๆ 1 นาที</h4>


    <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">ข้อมูลกระเเสไฟทุกๆ 1 นาที </li>
            <li class="breadcrumb-item active">วันที่เริ่ม :  {{$data['startdate']}}    วันสิ้นสุด :   {{$data['enddate']}}   </li>
            <li class="breadcrumb-item active"> รหัสอุปกรณ์ :  {{$data['keydevice']}}</li>
    </ol>
    <a href="{{route('searchhistory')}}" class="text-decoration-none"><--- ย้อนกลับ</a>

    <div class="row">
        <div class="col-md-2 col-sm-2 col-lg-2 col-2 mt-2">
         <select id="exportLink"  class="form-control">
             <option>เอ็กพอร์ต </option>
             <option id="excel">Export as excel</option>
         </select>
        </div>
    </div>

<div class="table-responsive">
    <table class="table table-bordered" id="example" width="100%" cellspacing="0">
     <thead>
         <tr class="text-center text-muted">
             <th>รหัสตู้เเร็ค</th>
             <th>รหัสอุปกรณ์</th>
             <th>ชื่ออุปกรณ์วัด</th>
             <th>เวลา-วันที่</th>
             <th>กระเเสไฟ</th>


         </tr>
     </thead>
     <tbody  class="text-center text-muted">
        @foreach ($query as $value)
            <tr>
                <td>{{$value->key_rack}}</td>
                <td>{{$value->key_device_1minutes}}</td>
                <td>{{$value->Name_device}}</td>
                <td>{{$value->timedate}}</td>
                <td>{{$value->irm_value_1minutes}}</td>

            </tr>
        @endforeach

     </tbody>
     <tfoot>
         <tr class="text-center text-muted">
            <th>รหัสตู้เเร็ค</th>
            <th>รหัสอุปกรณ์</th>
            <th>ชื่ออุปกรณ์วัด</th>
            <th>เวลา-วันที่</th>
            <th>กระเเสไฟ</th>

         </tr>
     </tfoot>
 </table>
 </div>
</div>




@endsection
