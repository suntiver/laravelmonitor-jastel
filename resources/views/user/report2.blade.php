@extends('layouts.user')

@section('content')
<div class="container-fluid">

    <h4 class="mt-4 text-muted">สรุปการใช้กระเเสไฟ</h4>


    <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">สรุปการใช้กระเเสไฟตามการค้นหา </li>
            <li class="breadcrumb-item active">วันที่เริ่ม :  {{$data['startdate']}}    วันสิ้นสุด :   {{$data['enddate']}}   </li>
            <li class="breadcrumb-item active"> รหัสตู้เเร็ค :  {{$data['keyrack']}} </li>
    </ol>
    <a href="{{route('searchreport')}}" class="text-decoration-none"><--- ย้อนกลับ</a>

           <div class="row">
               <div class="col-md-2 col-sm-2 col-lg-2 col-2 mt-2">
                <select id="exportLink"  class="form-control">
                    <option>เอ็กพอร์ต </option>
                    <option id="excel">Export as excel</option>
                </select>
               </div>
           </div>

    <div class="table-responsive">
    <table class="table table-bordered " id="example" width="100%" cellspacing="0">
     <thead>
         <tr class="text-center text-muted">
             <th>รหัสตู้เเร็ค</th>
             <th>ชื่อตู้เเร็ค</th>
             <th>กระเเสไฟ</th>
             <th>ยูนิต</th>
             <th>#</th>
         </tr>
     </thead>
     <tbody  class="text-center text-muted">
       

     </tbody>
     <tfoot>
         <tr class="text-center text-muted">
            <th>รหัสตู้เเร็ค</th>
             <th>ชื่อตู้เเร็ค</th>
             <th>กระเเสไฟ</th>
             <th>ยูนิต</th>
             <th>#</th>
         </tr>
     </tfoot>
 </table>
 </div>
</div>




<!-- Modal -->
<div class="modal fade" id="Modalmore" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background-color:#4e73df;">

          <span class="modal-title text-light" > ข้อมูลเพิ่มเติมในตู้เเร็คว่ามีอุปกรณ์วัดอะไรใช้งานอยู่ตามช่วงเวลาที่ค้นหา</span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="table-responsive" style="overflow-y: scroll;height:300px;">
                <table class="table table-bordered table-sm"  width="100%" cellspacing="0">
                 <thead>
                     <tr class="text-center text-muted">
                         <th>รหัสอุปกรณ์</th>
                         <th>เกณฑ์สูง</th>
                         <th>เกณฑ์ต่ำ</th>
                         <th>กระเเสไฟต่ำสุด </th>
                         <th>กระเเสไฟสูงสุด</th>
                         <th>เกินเกณฑ์สูง</th>
                         <th>เกินเกณฑ์ต่ำ</th>

                     </tr>
                 </thead>
                 <tbody  style="text-align: center;" class="text-muted" id="table_tr_more_detal">
                                    <!-- ข้อมูลในตารางมาจาก model_query.js -->


                 </tbody>

             </table>
             </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>

        </div>
      </div>
    </div>
  </div>

@endsection
