@extends('layouts.user')

@section('content')
<div class="container-fluid">

    <h4 class="mt-4 text-muted">ตรวจพบกระเเสไฟที่มีค่าต่ำกว่าเกณฑ์กำหนด</h4>


    <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">ตรวจพบกระเเสไฟที่มีเกณฑ์ต่ำกว่าที่กำหนด</li>
   </ol>
<div class="table-responsive">
    <table class="table table-bordered" id="example" width="100%" cellspacing="0">
     <thead>
         <tr class="text-center text-muted">
             <th>รหัสตู้เเร็ค</th>
             <th>รหัสอุปกรณ์</th>
             <th>ชื่อ</th>
             <th>เวลาที่พบล่าสุด</th>
             <th>กระเเสไฟ</th>


         </tr>
     </thead>
     <tbody  class="text-center text-muted">
        @foreach ($querylow as $value)
            <tr>
                <td>{{$value->key_rack}}</td>
                <td>{{$value->key_device}}</td>
                <td>{{$value->Name}}</td>
                <td>{{$value->datetime}}</td>
                <td><i class="fas fa-stop" style="font-size:15px;color:{{$color['color_low']}};"></i> {{$value->irm_value}} แอมป์</td>
            </tr>
        @endforeach

     </tbody>
     <tfoot>
         <tr class="text-center text-muted">
            <th>รหัสตู้เเร็ค</th>
            <th>รหัสอุปกรณ์</th>
            <th>ชื่อ</th>
            <th>เวลาที่พบล่าสุด</th>
            <th>กระเเสไฟ</th>

         </tr>
     </tfoot>
 </table>
 </div>
</div>




@endsection
