@extends('layouts.user')

@section('content')

<div class="container-fluid">


    <h4 class="mt-4 text-muted">อธิบายการใช้งานเว็บไซต์เบื้องต้น</h4>

    <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">อธิบายการใช้งานเว็บไซต์เบื้องต้น  ถ้าต้องการความช่วยเหลือโปรดติดต่อทีม innovation </li>

    </ol>


<div class="row">
    <div class="col-4">
      <div class="list-group " id="list-tab" role="tablist">
        <a class="list-group-item list-group-item-action active " id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">สถานะตู้เเร็คปัจจุบัน</a>
        <a class="list-group-item list-group-item-action  " id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">ความผิดปกติร้ายเเรง</a>
        <a class="list-group-item list-group-item-action  " id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages">ความผิดปกติทั้งหมด</a>
        <a class="list-group-item list-group-item-action  " id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings">การเปิดปิดเสียง</a>
      </div>
    </div>
    <div class="col-8">
      <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">


            <div class="row">
                <div class="col-md-11 text-muted  ">

                      <img src="img/1.png" class="img-fluid mb-3" >

                    <hr>
                    <div class="mb-3">
                        <span class="text-muted">สถานะเเร็คปัจจุบัน</span><br>
                        <span>สีที่ท่านเห็นในตู้เเร็ค จะเเสดงสีตามสถานะตัววัดที่เกิดขึ้นขณะนั้น ซึ่งสีจะเเสดงได้ 4 เเบบ </span><br>
                        <span>1.สีเขียว   ปกติไม่มีความผิดพลาดอะไร</span><br>
                        <span>2.สีเหลือง   ตัววัดกระเเสไฟตรวจพบการวัดกระเเสที่ต่ำกว่าเกณฑ์มาตราฐานที่ตั้งไว้</span><br>
                        <span>3.สีส้ม      ตัววัดกระเเสไฟตรวจพบการวัดกระเเสที่สูงกว่าเกณฑ์มาตราฐานที่ตั้งไว้</span><br>
                        <span>4.สีเเดง     ตัววัดกระเเสไฟตรวจพบกระเเสไฟที่ต่ำคาดการณ์ว่าน่าจะมีปัญหากับแหล่งจ่ายไฟ</span><br>
                        <span style="color:red">เวลาในการตรวจเช็คจะรีเฟชรข้อมูลทุกๆ 60 วินาที</span>
                    </div>

                </div>

            </div>

        </div>
        <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">

            <div class="row">
                <div class="col-md-11 text-muted  ">

                      <img src="img/2.png" class="img-fluid mb-3" >

                    <hr>
                    <div class="mb-3">
                        <span class="text-muted">ความผิดปกติร้ายเเรง</span><br>
                        <span>ข้อความที่จะเเสดงในความผิดปกติร้ายเเรงจะเเสดงก็ต่อเมื่อระบบตรวจพบว่าตู้เเร็คมีผลกระทบต่อเเหล่งจ่ายไฟ </span><br>
                        <span>ซึ่งจำนวนข้อความที่จะเเสดงจะขึ้นอยู่ว่าระบบตรวจพบเท่าไหร่ ผู้ใช้สามารถคลิกดูเหตุการณ์ได้</span><br>
                        <span style="color:red">เวลาในการตรวจเช็คจะรีเฟชรข้อมูลทุกๆ 60 วินาที</span>
                </div>
            </div>
        </div>
        </div>
        <div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">

            <div class="row">
                <div class="col-md-11 text-muted  ">

                      <img src="img/3.png" class="img-fluid mb-3" >

                    <hr>
                    <div class="mb-3">
                        <span class="text-muted">ความผิดปกติทั้งหมดที่ตรวจพบ</span><br>
                        <span>1.ความผิกปกติ </span><br>
                        <span>ระบบจะบอกว่าตอนนี้ระบบตรวจความผิดอะไรบ้างเช่น กระเเสไฟสูง หรือเปล่า กระเเสไฟต่ำหรือเปล่า ตู้เเร็คไหนมีผลกระทบต่อเเหล่งจ่ายไฟ</span><br>
                        <span>2.เคลียร์เเล้ว </span><br>
                        <span>ระบบจะบอกว่าเหตุการณ์ไหนที่กลับมาปกติเเล้ว</span><br>
                        <span>3.อุปกรณ์ออนไลน์</span><br>
                        <span>ระบบจะบอกว่าอุปกรณ์ PCB ที่ใช้ตรวจวัดกระเเสไฟอันไหนที่ออนไลน์อยู่</span><br>
                        <span>4.อุปกรณ์ออฟไลน์</span><br>
                        <span>ระบบจะบอกว่าอุปกรณ์ PCB ที่ใช้ตรวจวัดกระเเสไฟอันไหนที่ออฟไลน์อยู่</span><br>
                        <span style="color:red">เวลาในการตรวจเช็คจะรีเฟชรข้อมูลทุกๆ 60 วินาที</span>


                </div>
            </div>
        </div>




        </div>
        <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">

            <div class="row">
                <div class="col-md-11 text-muted  ">

                      <img src="img/4.png" class="img-fluid mb-3" >

                    <hr>
                    <div class="mb-3">
                        <span class="text-muted">การเปิดปิดเสียง</span><br>
                        <span>เมือเกิดเหตุการณ์ร้ายเเรงที่ส่งผลกระทบต่อเเหล่งจ่ายไฟ </span><br>
                        <span>จะเกิดเสียงดัง ผู้ใช้สามารถกดปิดเปิดเสียงได้ โดยคลิกที่ปุ่มตามภาพ </span><br>



                </div>
            </div>
        </div>



        </div>
      </div>
    </div>
  </div>

</div>
@endsection
