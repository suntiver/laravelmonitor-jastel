@extends('layouts.user')

@section('content')
<div class="container-fluid">

    <h4 class="mt-4 text-muted">ข้อมูลกระเเสไฟเชิกลึกย้อนหลัง ชนิดข้อมูล 15 นาที </h4>


    <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">ข้อมูลกระเเสไฟเชิกลึกย้อนหลัง </li>
            <li class="breadcrumb-item active">วันที่เริ่ม :  {{$data['startdate']}}    วันสิ้นสุด :   {{$data['enddate']}}   </li>
            <li class="breadcrumb-item active"> รหัสตู้เเร็ค :  {{$data['keyrack']}} </li>
    </ol>
    <a href="{{route('searchtecnical')}}" class="text-decoration-none"><--- ย้อนกลับ</a>
    <div class="row">
        <div class="col-md-2 col-sm-2 col-lg-2 col-2 mt-2">
         <select id="exportLink"  class="form-control">
             <option>เอ็กพอร์ต </option>
             <option id="excel">Export as excel</option>
         </select>
        </div>
    </div>

<div class="table-responsive">
    <table class="table table-bordered" id="example" width="100%" cellspacing="0">
     <thead>
         <tr class="text-center text-muted">
             <th>รหัสตู้เเร็ค</th>
             <th>รหัสอุปกรณ์</th>
             <th>วันที่-เวลา</th>
             <th>ค่าสูง</th>
             <th>ค่าต่ำ</th>
             <th>ค่าสูงสุด</th>
             <th>ค่าต่ำสุด</th>
             <th>ผลรวมค่าสูงสุด</th>
             <th></th>


         </tr>
     </thead>
     <tbody  class="text-center text-muted">
        <!-- ข้อมูล ต้องมีถึงจะเข้าเงื่อนไขได้-->
        @if(count($query) > 0)
            @for($i = 0;$i < count($manage['RACK']) ;$i++ )
            <tr>
                    <td>{{$manage['RACK'][$i]['keyrack']}} </td>
                    <td>{{$manage['RACK'][$i]['keydevice']}} </td>
                    <td>{{$manage['RACK'][$i]['datetimeend']}} </td>
                    <td>{{$manage['RACK'][$i]['countmoresettinghigh']}} ครั้ง</td>
                    <td>{{$manage['RACK'][$i]['countmoresettinglow']}} ครั้ง</td>
                    @if($manage['RACK'][$i]['max'] == null)
                            <td> 0.0 แอมป์</td>
                    @else
                            <td>{{$manage['RACK'][$i]['max']}} แอมป์</td>

                    @endif

                    @if($manage['RACK'][$i]['min'] == null)
                             <td> 0.0 แอมป์</td>
                    @else
                            <td>{{$manage['RACK'][$i]['min']}} แอมป์</td>
                    @endif

                    <td>{{number_format($manage['RACK'][$i]['sumcountmoresettinghigh'],1)}} แอมป์</td>

                    @if($manage['RACK'][$i]['countmoresettinghigh'] == 0  and $manage['RACK'][$i]['countmoresettinglow'] ==0 )
                            <td></td>
                    @else
                            <td><button class="btn btn-warning" data-toggle="modal" data-target="#Modalshowdetal" data-datetime="{{$manage['RACK'][$i]['datetimeend']}}" data-keydevice="{{$manage['RACK'][$i]['keydevice']}}">ดูข้อมูลย่อย</button></td>
                    @endif


            </tr>

            @endfor
        @endif

     </tbody>
     <tfoot>
         <tr class="text-center text-muted">
            <th>รหัสตู้เเร็ค</th>
            <th>รหัสอุปกรณ์</th>
            <th>วันที่-เวลา</th>
            <th>ค่าสูง</th>
            <th>ค่าต่ำ</th>
            <th>ค่าสูงสุด</th>
            <th>ค่าต่ำสุด</th>
            <th>ผลรวมค่าสูงสุด</th>
            <th></th>
         </tr>
     </tfoot>
 </table>
 </div>
</div>



 <!-- Modalนี้เอาไว้เเสดงรายละเอียดข้อมูลที่ผู้ใช้เลือก-->
 <div class="modal fade" id="Modalshowdetal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
      <div class="modal-content">
        <div class="modal-header"  style="background-color:#4e73df;">

          <span class="modal-title text-light"> ข้อมูลการเกิดเหตุการณ์ผิดปกติตามช่วงเวลา </span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" class="text-light">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="table-responsive" style="overflow-y: scroll;height:300px;">
                <table class="table table-bordered table-sm"  width="100%" cellspacing="0">
                 <thead>
                     <tr class="text-center text-muted">
                         <th>เวลาที่พบ</th>
                         <th>รหัสตู้เเร็ค</th>
                         <th>รหัสอุปกรณ์วัด</th>
                         <th>ชื่ออุปกรณ์วัด</th>
                         <th>กระเเสไฟ</th>
                     </tr>
                 </thead>
                 <tbody  style="text-align: center;"  class="text-muted"  id="table_tr_showdetal">
                         <!-- ข้อมูลในตารางมาจาก model_query.js -->
                 </tbody>

             </table>
             </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>

        </div>
      </div>
    </div>
  </div>



@endsection
