@extends('layouts.user')

@section('content')
<div class="row justify-content-center">
    <div class="col-lg-5">
        <div class="card shadow-lg border-0 rounded-lg mt-4">
            <div class="card-header text-center py-3" style="background-color:#4e73df;">
                <span class=" font-weight-light  text-light">ค้นหาประวัติการเเจ้งเตือนที่เกินขึ้นย้อนหลัง</span>
            </div>
            <div class="card-body">
                <form method="POST" action="{{route('historycurrent')}}"  enctype="multipart/form-data">
                    @csrf

                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="medium mb-1" for="inputPassword">วันเริ่มต้น</label>
                                <div class="input-group ">

                                    <input class="form-control "  id="startingDate"    name="startdate"  type="date" value="<?php echo date("Y-m-d"); ?>" id="example-date-input">
                                    <div class="input-group-append">
                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                    </div>
                             </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="medium mb-1" for="inputPassword">วันสิ้นสุด</label>
                            <div class="input-group ">
                                <input class="form-control"  id="endingDate"   name="enddate"  type="date" value="<?php echo date("Y-m-d"); ?>" id="example-date-input">
                                <div class="input-group-append">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                </div>
                         </div>
                        </div>
                    </div>



                    <div class="form-row">
                        <div class="col-md-12">
                            <label class="medium mb-1" for="inputFirstName">รหัสตู้เเร็ค </label>
                            <div class="input-group  ">
                                <select class="form-control"   onfocus='this.size=5;' onblur='this.size=1;' onchange='this.size=1; this.blur();'  name="keyrack">
                                    <option value="all">ทั้งหมด</option>
                                    @foreach ($optionkeyrack as $value)
                                             <option>{{$value->key_rack_setting}}</option>
                                     @endforeach

                                  </select>

                         </div>
                        </div>

                    </div>

                    <div class="form-group mt-5 mb-0">
                        <button type="submit" class="btn btn-primary btn-block" >
                            ค้นหาข้อมูล
                        </button>

                    </div>
                </form>
            </div>
            <div class="card-footer text-center  ">
                <div class="small" > <span class="font-weight-bold" style="color:red"> **คำเเนะนำ  </span> <span class="text-muted"> การค้นหาข้อมูลกระเเสไฟมากกว่า 1 เดือน อาจใช้เวลานาน </span>  </div>
            </div>
        </div>
    </div>
</div>


@endsection
