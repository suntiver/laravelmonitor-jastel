@extends('layouts.user')

@section('content')

    <div class="row">
    <div class="col-md-9  ">
        <div class="card shadow mb-12 " >
            <!-- Card Header - Dropdown -->
            <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between " style="background-color:#4e73df">

              <span class="text-light">สถานะตู้ Rack ณ ปัจจุบัน </span>
              <div class="row">

                        <div class="col text-right ">

                            <i class="fas fa-stopwatch text-light" style="font-size:20px;"></i> <span id="timnow"  class="text-light"></span>

                        </div>
                </div>
            </div>
            <!-- Card Body -->
            <div class="card-body shadow">

                 <div  class="sizechartchartstats  box-monitor background-color"   >
                        <div class="very-specific-design" id="very-specific-design" >

                                <div id="rackall">
                                    @foreach ($queryrack  as $value)
                                    <a href="" data-toggle="modal" data-target="#Modalpopuprack" data-key_rack_setting="{{$value->key_rack_setting}}" data-ownwer="{{$value->Ownwer}}" data-id="{{$value->id}}" data-placement="top" title="รหัสตู้เเร็ค:{{$value->key_rack_setting}} , ชื่อตู้ :{{$value->Ownwer}}" >
                                        <div  style="position:{{$value->position}};z-index:{{$value->zindex}};right:{{$value->right_px}};top:{{$value->top_px}};">
                                               <div class='pulse' style="background-color:{{$value->status_color}}"></div>
                                        </div>
                                    </a>

                                    @endforeach

                                </div>

                                <img src="img/SERVER-RACK-WITH-COMPONENTS.png"   style="position:relative;z-index:1; width:100%; height:90%;"  >
                        </div>
                </div>

                <div class="col-md-12 mt-5  text-center text-muted">
                        <i class="fas fa-square ml-4" style="font-size:100%;color:{{$color['color_normal']}};text-shadow: 0 0 0px #000; "></i>
                        <span >ปกติ</span>
                        <i class="fas fa-square ml-4" style="font-size:100%;color:{{$color['color_low']}};text-shadow: 0 0 0px #000;"></i>
                        <span>ต่ำกว่าที่กำหนด</span>
                        <i class="fas fa-square ml-4" style="font-size:100%;color:{{$color['color_high']}};text-shadow: 0 0 0px #000;"></i>
                        <span>สูงกว่าที่กำหนด</span>
                        <i class="fas fa-square ml-4" style="font-size:100%;color:{{$color['color_critical']}};text-shadow: 0 0 0px #000;"></i>
                        <span>ร้ายเเรง</span>
                </div>

            </div>

          </div>
    </div>

    <div class="col-md-3">

        <div class="card shadow mb-12">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between"  style="background-color:#4e73df">

              <span class="text-light"  > ความผิดปกติร้ายเเรง  </span>

            </div>
            <!-- Card Body -->

                 <div   class="box-notifaction" id="alamcritical">
                 <!-- ถ้าระบบตรวจไม่พบความผิดพลาดก็จะเข้าเงื่อนไขนี้ -->
                @if(count($notificationProm) == 0)

                            <div class="container">
                                <div class="row ">
                                    <div class="col-md-12 ">
                                        <center>  <i class="fas fa-check-circle    mt-5 mb-3" style="font-size: 150px;color:LimeGreen;"></i> </center>
                                    </div>
                                    <div class="col-md-12">
                                            <p class="text-muted text-center  " >ไม่พบความผิดปกติร้ายเเรง</p>
                                    </div>

                                </div>
                            </div>


                     @else

                        <!-- ถ้าระบบตรวจพบว่าตัวแคมป์มีค่าวัดกระเเสไฟต่ำกว่า  0 ให้เข้าเงื่อนไขนี้ทันที -->
                            <ul class="list-group  scorllerbar "  type="button" >
                                @foreach($notificationProm  as $value)
                                <li class="list-group-item  mouseover  lilist"  data-toggle="modal" data-target="#Modalcritical" data-keyrack="{{$value->key_rack_setting}}" data-datetime="{{$value->date_table_rack}}">
                                    <div class="row no-gutters align-items-center driver">
                                        <div class="col mr-2">
                                            <span class="text-dark">พบปัญหาตู้เเร็ค  </span>
                                            <div>
                                                <span  class="text-muted">{{$value->key_rack_setting}}</span >
                                            </div>
                                            <span class="text-dark">ตรวจพบเมื่อ  </span>
                                            <div>
                                                <span class="text-muted">{{$value->date_table_rack }} </span>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-exclamation-triangle" style="color:red;font-size:30px"></i>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                    @endif

            </div>
          </div>



          <div class="card shadow mt-3  ">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between" style="background-color:#4e73df">

                  <span class="text-light">ความผิดปกติทั้งหมด </span>

                </div>
                <ul class="list-group ">
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <a href="#"  class="text-muted text-decoration-none"   data-toggle="modal" data-target="#Modalcurrentunclear" > ความผิดปกติ </a>
                        <div id="currentview">
                            @if($countproble['current_view'] >0)
                                <span class="badge badge-danger badge-pill">{{$countproble['current_view']}}</span>
                            @endif

                        </div>

                    </li>

                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <a href="#"  class="text-muted text-decoration-none"  data-toggle="modal" data-target="#Modalcurrentclear"> เคลียร์เเล้ว </a>
                        <div id="current_view_clear">
                            @if($countproble['current_view_clear'] >0)
                                    <span class="badge badge-success badge-pill">{{$countproble['current_view_clear']}}</span>
                             @endif
                        </div>


                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <a href="#"   class="text-muted text-decoration-none"  data-toggle="modal" data-target="#Modaldeviceonline">  อุปกรณ์ออนไลน์</a>
                        <div id="deviceonline">
                            @if($countproble['deviceonline'] >0)
                            <span class="badge badge-success badge-pill">{{$countproble['deviceonline']}}</span>
                             @endif

                        </div>


                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <a href="#"   class="text-muted text-decoration-none"   data-toggle="modal" data-target="#Modaldeviceofline"> อุปกรณ์ออฟไลน์ </a>

                        <div id="deviceofline">
                            @if($countproble['deviceofline'] >0)
                            <span class="badge badge-danger badge-pill">{{$countproble['deviceofline']}}</span>
                            @endif
                        </div>

                    </li>

                  </ul>

              </div>
    </div>

    </div>




 <!-- Modalนี้เอาไว้เเสดงว่าในตู้เเร็ค มีตัววัดกระเเสไฟอะไรบ้าง-->
 <div class="modal fade" id="Modalpopuprack" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
      <div class="modal-content">
        <div class="modal-header" style="background-color:#4e73df;">

          <span class="modal-title text-light" id="title">  </span>

          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" class="text-light">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="table-responsive " style="overflow-y: scroll;">
                <table class="table table-bordered "    cellspacing="0">
                 <thead>
                     <tr class="text-center text-muted" >

                         <th>รหัสอุปกรณ์วัด</th>
                         <th>ชื่ออุปกรณ์วัด</th>
                         <th>เกณฑ์สูงสุด</th>
                         <th>เกณฑ์ต่ำสุด</th>
                         <th>ค่ากระเเสล่าสุด</th>
                         <th>ปุ่มกด</th>

                     </tr>
                 </thead>
                 <tbody  class="text-muted"  style="text-align: center;" id="table_tr_campinrack">

                              <!-- ข้อมูลในตารางมาจาก model_query.js -->
                 </tbody>

             </table>
             </div>

        </div>
        <div class="modal-footer">


          <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>

        </div>
      </div>
    </div>
  </div>



  <!-- Modalนี้เอาไว้เเสดงกราฟที่เลือก-->
 <div class="modal fade" id="Modalchart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg  " role="document">
      <div class="modal-content">
        <div class="modal-header" style="background-color:#4e73df;">

          <span class="modal-title text-light" > หน้าต่างเเสดงกราฟย้อนหลัง รหัสอุปกรณ์วัด <span class="font-weight-bold" style="color:#000" id="modelid_keydevice"></span> </span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" class="text-light">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="container-fluid">
                <!-- Earnings (Monthly) Card Example -->
                <div class="row">
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-danger shadow  ">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                <div class="col">
                                    <div class="text-xs font-weight-bold text-danger text-uppercase mb-1" >กระเเสไฟสูงสุด</div>
                                    <div class="col-auto">
                                        <span id="valuemax" class="text-muted">0.00 แอมป์ </span>
                                </div>

                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-bolt fa-2x text-gray-300"></i>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-warning shadow ">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                <div class="col ">
                                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1" >กระเเสไฟต่ำสุด</div>
                                    <div class="col-auto">
                                        <span id="valuemin" class="text-muted">0.00 แอมป์ </span>
                                </div>

                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-bolt fa-2x text-gray-300"></i>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>


                            <!-- Earnings (Monthly) Card Example -->
                            <div class="col-xl-4 col-md-6 mb-4">
                                <div class="card border-left-success shadow ">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                    <div class="col ">
                                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1" >กระเเสไฟ 1 นาที</div>
                                        <div class="row no-gutters align-items-center">
                                        <div class="col-auto">
                                                <span id="valuenow" class="text-muted">0.00 แอมป์ </span>
                                        </div>
                                        </div>

                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-bolt fa-2x text-gray-300"></i>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                </div>

                <div id="container"   style="height: 300px" >
                               <div class="mt-5">
                                   <center class="text-muted">กรุณารอสักครู่กำลังโหลดข้อมูลกราฟ.....</center>

                               </div>
                </div>

            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>

        </div>
      </div>
    </div>
  </div>




 <!-- Modalนี้เอาไว้เเสดงเมื่อผู้ใช้กดคลิกที่เหตุการณ์ร้ายเเรงถึงจะขึ้น-->
<div class="modal fade" id="Modalcritical" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
      <div class="modal-content">
        <div class="modal-header" style="background-color:#4e73df;">

          <span class="modal-title text-light" >เหตุการณ์ที่เกิดขึ้น</span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" class="text-light">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <center style="color:red" >
            <span>ตัววัดกระเเสไฟตรวจพบกระเเสที่ต่ำกว่า 0.5  </span><br>
                <span>คาดการณ์ว่าน่าจะมีผลกระทบต่อกระเเสไฟ โปรดตรวจสอบ !!  </span>
            <p>-------------------------------------------------</p> </center>
             <span class="text-muted">รหัสตู้เเร็ค : <span style="color:red" id="key_rack" >  </span> </span><br>
             <span class="text-muted">ตรวจพบเมื่อ : <span style="color:red" id="datetime" > </span></span><br>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>

        </div>
      </div>
    </div>
  </div>



  <!-- Modalนี้เอาไว้เเสดง ความผิดปกติทั้งหมดที่ยังไม่เคลียร์ currentview-->
  <div class="modal fade" id="Modalcurrentunclear" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
      <div class="modal-content">
        <div class="modal-header"  style="background-color:#4e73df;">

          <span class="modal-title text-light" > ความผิดปกติที่ตัววัดไฟตรวจพบ </span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" class="text-light">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="table-responsive" style="overflow-y: scroll;height:300px;">
                <table class="table table-bordered table-sm"  width="100%" cellspacing="0">
                 <thead>
                     <tr class="text-center text-muted">
                         <th>เวลาที่พบล่าสุด</th>
                         <th>รหัสอุปกรณ์วัด</th>
                         <th>รหัสตู้เเร็ค</th>
                         <th>กระเเสไฟ</th>

                     </tr>
                 </thead>
                 <tbody id="table_tr_currenctunclear" style="text-align: center;" class="text-muted">
                                    <!-- ข้อมูลในตารางมาจาก model_query.js -->

                 </tbody>

             </table>
             </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>

        </div>
      </div>
    </div>
  </div>



    <!-- Modalนี้เอาไว้เเสดง ความผิดปกติทั้งหมดที่เคลียร์เเล้ว currentview-->
    <div class="modal fade" id="Modalcurrentclear" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
          <div class="modal-content">
            <div class="modal-header"  style="background-color:#4e73df;">

              <span class="modal-title text-light"> การเเจ้งเตือนที่เคลียร์เเล้ว </span>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" class="text-light">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive" style="overflow-y: scroll;height:300px;">
                    <table class="table table-bordered table-sm"  width="100%" cellspacing="0">
                     <thead>
                         <tr class="text-center text-muted">
                            <th>เวลาที่พบล่าสุด</th>
                            <th>รหัสอุปกรณ์วัด</th>
                            <th>รหัสตู้เเร็ค</th>
                            <th>กระเเสไฟ</th>
                         </tr>
                     </thead>
                     <tbody  style="text-align: center;" id="table_tr_currenctclear" class="text-muted">
                             <!-- ข้อมูลในตารางมาจาก model_query.js -->
                     </tbody>

                 </table>
                 </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>

            </div>
          </div>
        </div>
      </div>


    <!-- Modalนี้เอาไว้เเสดง อุปกรณ์ออนไลน์-->
    <div class="modal fade" id="Modaldeviceonline" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
          <div class="modal-content">
            <div class="modal-header"  style="background-color:#4e73df;">

              <span class="modal-title  text-light"> อุปกรณ์ที่มีสถานะออนไลน์ ข้อมูลอัพเดททุกๆ <span style="color:red">[  5 วินาที ]</span></span>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" class="text-light">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive" style="overflow-y: scroll;height:300px;">
                    <table class="table table-bordered table-sm"  width="100%" cellspacing="0">
                     <thead>
                         <tr class="text-center text-muted">
                            <th scope="col">รูปภาพ</th>
                            <th scope="col">ไอพีอุปกรณ์วัด</th>
                            <th scope="col">ชื่ออุปกรณ์</th>
                            <th scope="col">อัพเดทล่าสุด</th>
                            <th scope="col">สถานะ</th>

                         </tr>
                     </thead>
                     <tbody  style="text-align: center;" id="table_tr_device_onlnie" class="text-muted">

                             <!-- ข้อมูลในตารางมาจาก model_query.js -->
                     </tbody>

                 </table>
                 </div>

            </div>
            <div class="modal-footer">


              <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>

            </div>
          </div>
        </div>
      </div>


      <!-- Modalนี้เอาไว้เเสดง อุปกรณ์ออฟไลน์-->
    <div class="modal fade" id="Modaldeviceofline" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
          <div class="modal-content">
            <div class="modal-header"  style="background-color:#4e73df;">

              <span class="modal-title text-light" > อุปกรณ์ที่มีสถานะออฟไลน์  ข้อมูลอัพเดททุกๆ <span style="color:red">[ 20 นาที ]</span></span>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" class="text-light">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive" style="overflow-y: scroll;height:300px;">
                    <table class="table table-bordered table-sm"  width="100%" cellspacing="0">
                     <thead>
                         <tr class="text-center text-muted">
                            <th scope="col">รูปภาพ</th>
                            <th scope="col">ไอพีอุปกรณ์วัด</th>
                            <th scope="col">ชื่ออุปกรณ์</th>
                            <th scope="col">อัพเดทล่าสุด</th>
                            <th scope="col">สถานะ</th>


                         </tr>
                     </thead>
                     <tbody  style="text-align: center;"  id="table_tr_device_oflnie" class="text-muted">
                            <!-- ข้อมูลในตารางมาจาก model_query.js -->

                     </tbody>

                     </tbody>
                 </table>
                 </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>

            </div>
          </div>
        </div>
      </div>


@endsection
