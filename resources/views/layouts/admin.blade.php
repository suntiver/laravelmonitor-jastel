<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>IDC-{{ Auth::user()->name }} </title>
        <link rel="icon" href="{{ URL::asset('/img/Icon-title.png') }}" type="image/x-icon" />


        <link href="{{ URL::asset('/css/styles.css') }}" rel="stylesheet" type="text/css" >
        <link href="{{ URL::asset('/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" >
        <link href="{{ URL::asset('/css/toastr.min.css') }}" rel="stylesheet" type="text/css" >
        <link href="{{ URL::asset('/css/mfb.css') }}" rel="stylesheet" type="text/css" >
        <!-- bootstrao-select -->
        <link href="{{ URL::asset('/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" >

        <!-- เอาวัดจัดการภาพ 3D -->
        <link href="{{ URL::asset('/css/responsize.css') }}" rel="stylesheet" type="text/css" >

        <!-- animation ปุ่มเเดงๆ บน top menu -->
        <link href="{{ URL::asset('/css/animatio.css') }}" rel="stylesheet" type="text/css" >

        <link href="{{ URL::asset('/css/style2.css') }}" rel="stylesheet" type="text/css" >

        <!-- จุด location บนตู้ rack -->
        <link href="{{ URL::asset('/css/location.css') }}" rel="stylesheet" type="text/css" >


        <style>

        .collapse1{
        overflow-y:scroll;
        height:310px;
        }
        .bgImgCenter{
                background-image: url('../img/Phase1_admin.png');
                left: 2px;

                padding: 0;
                position: relative;

                z-index: 3;
                height: 410px; width: 700px;
        }
        </style>


    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    </head>
    <body class="sb-nav-fixed sb-sidenav-toggled">
        <nav class="sb-topnav navbar navbar-expand navbar-light bg-light">
            <a class="navbar-brand" href="http://172.16.12.40:5050/">JASTEL-IDC </a><button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" ><i class="fas fa-bars"></i></button
            ><!-- Navbar Search-->
            <p class="navbar-nav "> การจัดการสำหรับ Phase 3</p>
            <div class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                         <p class="navbar-nav"> ยินดีต้อนรับคุณ : {{ Auth::user()->name }} </p>
            </div>

            <!-- Navbar-->
            <ul class="navbar-nav ml-auto ml-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">

                        <a class="dropdown-item"   href="{{ route('logout') }}"  onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();" >
                            ออกจากระบบ
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                            </form>
                        </a>
                    </div>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-light" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Core</div>
                        <a class="nav-link" href="{{route('admin\dashboard')}}">
                                <div class="sb-nav-link-icon">
                                    <i class="fas fa-user"></i>
                                </div>
                               จัดการผู้ใช้
                            </a >
                             <a class="nav-link" href="{{route('admin\rack')}}">
                                 <div class="sb-nav-link-icon">
                                     <i class="fas fa-door-closed"></i>

                                </div>
                              จัดการตู้เเร็ค
                            </a >
                            <div class="sb-sidenav-menu-heading">Core</div>

                            <a class="nav-link" href="{{route('admin\notificationnew')}}"
                                ><div class="sb-nav-link-icon"><i class="fas fa-bell"></i></div>
                               เเจ้งเตือนผู้ใช้</a >
                            <div class="sb-sidenav-menu-heading">Interface</div>
                            <a class="nav-link" href="{{route('admin\devicepcb')}}" ><div class="sb-nav-link-icon">
                                <i class="fas fa-cog"></i>
                            </div>
                                 จัดการสถานะอุปกรณ์
                             </a >
                             <a class="nav-link" href="{{route('admin\log')}}" ><div class="sb-nav-link-icon">
                                <i class="fas fa-snowboarding"></i>
                            </div>
                                 กิจกรรมที่เกิดขึ้น
                             </a >

                             <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                 <div class="sb-nav-link-icon">
                                      <i class="fas fa-hammer"></i>
                                </div>
                                ตั้งค่าการเเจ้งเตือน
                             <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                         <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                             <nav class="sb-sidenav-menu-nested nav">
                                 <a class="nav-link" href="{{route('admin\critical')}}">ไฟดับ</a>
                                 <a class="nav-link" href="{{route('admin\low')}}">กระเเสไฟต่ำกว่าเกณฑ์</a>
                                 <a class="nav-link" href="{{route('admin\high')}}">กระเเสไฟสูงกว่าเกณฑ์</a>
                                 <a class="nav-link" href="{{route('admin\normal')}}">กระเเสไฟปกติ</a>
                             </nav>
                         </div>



                        <div class="sb-sidenav-menu-heading">Help</div>
                            <a class="nav-link" href="{{route('admin\help')}}">
                                <div class="sb-nav-link-icon">
                                    <i class="fas fa-question-circle"></i>
                                </div>
                                    ช่วยเหลือ
                            </a >

                    </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        Start Bootstrap
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>

                    @yield('content')

                </main>
                <footer class="py-3 bg-light mt-auto ">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; IDC-JASTEL {{date("Y")}}</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>



    </body>


        <!-- jquery -->
        <script type="text/javascript" src="{{ URL::asset('/js/jquery-3.4.1.min.js') }}"></script>

        <!-- bootstrap -->
        <script type="text/javascript" src="{{ URL::asset('/js/bootstrap.bundle.min.js') }}"></script>

        <!--  ธีม   -->
        <script type="text/javascript" src="{{ URL::asset('/js/scripts.js') }}"></script>


        <!-- icon -->
        <script type="text/javascript" src="{{ URL::asset('/js/font-awesome.min.js') }}"></script>

        <!-- table -->
        <script type="text/javascript" src="{{ URL::asset('/js/jquery.dataTables.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('/js/dataTables.bootstrap4.min.js') }}"></script>

        <!-- การเเจ้งเตือน -->
        <script type="text/javascript" src="{{ URL::asset('/js/toastr.min.js') }}"></script>

      

        <!-- /bootstrap-select -->

        <script type="text/javascript" src="{{ URL::asset('/js/bootstrap-select.min.js') }}"></script>


        <!-- (Optional) Latest compiled and minified JavaScript translation files -->
        {{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/i18n/defaults-*.min.js"></script> --}}



        <!--Javascript  ข้างนอก -->
        <!--ดึงข้อมูลลงตาราง -->
        <script type="text/javascript" src="{{ URL::asset('/js/table.js') }}"></script>
        <!-- เกี่ยวกับ pop up model -->
        <script type="text/javascript" src="{{ URL::asset('/js/model_query.js') }}"></script>


        <!-- ปรับ layout 3D ให้คงที่ -->
        <script type="text/javascript" src="{{ URL::asset('/js/resize2.js') }}"></script>


        <!-- เอาไว้เเจ้งเตือนเมื่อเข้าสู่ระบบ เเละ  มีการแก้ไขหรืออัพเดทอะไรสักอย่าง -->
        <script>
            @if(Session::get('alert-type') == "success" )
                toastr.success("{{ Session::get('message') }}");

            @elseif(Session::get('alert-type') == "warning")
                toastr.warning("{{ Session::get('message') }}");

            @elseif(Session::has('message'))
                    toastr.success("{{ Session::get('message') }}");
            @endif
        </script>



<script>
    $(document).ready(function(){
      $(".btn1").click(function(){
            $(".deviceonlineall").show();
            $(".deviceonlinenew").hide();
            $(".querydeviceonlineaddtorack").hide();
      });
      $(".btn2").click(function(){
            $(".deviceonlinenew").show();
            $(".deviceonlineall").hide();
            $(".querydeviceonlineaddtorack").hide();
      });

      $(".btn3").click(function(){
            $(".querydeviceonlineaddtorack").show();
            $(".deviceonlineall").hide();
            $(".deviceonlinenew").hide();
      });
    });
    </script>


    <!--เอาไว้เลือกสีการเเจ้งเตือน -->
    <script>
    $('#mySelect1').selectpicker();
    $('#mySelect2').selectpicker();
    </script>


   <!-- เอา validation การกรอกข้อมูลว่าถูกต้องหรือไม่ -->
    <script>
    (function() {
                'use strict';
                window.addEventListener('load', function() {

                    var forms = document.getElementsByClassName('needs-validation');

                    var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                    });
                }, false);
                })();

    </script>

 <!-- เอา โชว์ว่าผู้ใช้อัพโหลดรูปภาพอะไรมา -->
<script >

        $("#image").on("change",function(e){
            if (e.target.files.length) {
            $(this).next('.custom-file-label').html(e.target.files[0].name);
        }

        });


    </script>
</html>
