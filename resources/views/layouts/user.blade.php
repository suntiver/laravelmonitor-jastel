<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>IDC-{{ Auth::user()->name }} </title>
  <link rel="icon" href="{{ URL::asset('/img/Icon-title.png') }}" type="image/x-icon" />



  <!-- icon -->
  <link href="{{ URL::asset('/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css" >

  <!-- Custom fonts for this template-->
  <link href="{{ URL::asset('/css/Nunito.css') }}" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{{ URL::asset('/css/sb-admin-2.min.css') }}" rel="stylesheet" type="text/css" >
  <link href="{{ URL::asset('/css/styles.css') }}" rel="stylesheet" type="text/css" >
  <link href="{{ URL::asset('/css/style2.css') }}" rel="stylesheet" type="text/css" >

  <!-- เอาไวัจัดการ เเจ้งเตือน -->
  <link href="{{ URL::asset('/css/toastr.min.css') }}" rel="stylesheet" type="text/css" >

  <!-- เอาวัดจัดการภาพ 3D -->
  <link href="{{ URL::asset('/css/responsize.css') }}" rel="stylesheet" type="text/css" >

  <!-- animation ปุ่มเเดงๆ บน top menu -->
  <link href="{{ URL::asset('/css/animatio.css') }}" rel="stylesheet" type="text/css" >

  <!-- จุด location บนตู้ rack -->
  <link href="{{ URL::asset('/css/location.css') }}" rel="stylesheet" type="text/css" >

  <!-- bootstrap select -->
  <link href="{{ URL::asset('/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" >

  <!-- table -->
  <link href="{{ URL::asset('/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" >
  <link href="{{ URL::asset('/css/buttons.dataTables.min.css') }}" rel="stylesheet" type="text/css" >


  <!-- เวลาเลือกข้อความเเล้วจะเกิดเส้น -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" type="text/css">

  <!-- เกี่ยวกับวันที่ pickadate -->
  <link rel="stylesheet" href="{{ URL::asset('/css/classic.css') }}" >
  <link rel="stylesheet" href="{{ URL::asset('/css/classic.date.css') }}" >


</head>


<!-- ถ้า admin เปิด ค่า ststus == 1 ถึงจะเข้าถึงเว็บไซต์ได้ -->
@if( Auth::user()->status  == 1 )

<body id="page-top"  class="sidebar-toggled"  >

    <!-- ฝากไว้เอาไว้สำหรับใช้หา id ของ user -->
    <div id="numID" style="display:none"> {{ Auth::user()->id }} </div>

    <!-- Page Wrapper -->
    <div id="wrapper">

      <!-- Sidebar -->
      <ul class="  navbar-nav bg-gradient-primary sidebar sidebar-dark toggled " id="accordionSidebar">
          <!-- Sidebar - Brand -->
          <a class="sidebar-brand d-flex align-items-center justify-content-center" href="http://172.16.12.40:5050/">
            <div class="sidebar-brand-icon rotate-n-15">
              <i class="fas fa-bolt"></i>
            </div>
            <div class="sidebar-brand-text mx-3">IDC SYSTEM  </div>
          </a>

          <!-- Divider -->
          <hr class="sidebar-divider my-0">

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="{{route('home')}}" >
                  <i class="fas fa-h-square"></i>
                  <span>หน้าเเรก</span>
                </a>

              </li>



              <!-- Divider -->
              <hr class="sidebar-divider">

              <!-- Heading -->
              <div class="sidebar-heading">
               Data
             </div>



         <!-- Nav Item - Utilities Collapse Menu -->
         <li class="nav-item">
         <a class="nav-link collapsed" href="{{route('searchhistory')}}" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-history"></i>
          <span>กระเเสไฟย้อนหลัง</span>
        </a>

        </li>

            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item">

             <a class="nav-link collapsed" href="#"  aria-expanded="true" aria-controls="collapseUtilities" style="display: none">
            {{-- <a class="nav-link collapsed" href="{{route('searchtecnical')}}"  aria-expanded="true" aria-controls="collapseUtilities"> --}}
                <i class="fas fa-pager"></i>
                <span>ข้อมูลเชิงลึกย้อนหลัง</span>
              </a>

            </li>


            <li class="nav-item">
              <a class="nav-link collapsed" href="{{route('searchreport')}}"  aria-expanded="true" aria-controls="collapseUtilities">
                  <i class="fas fa-fw fa-clipboard"></i>
                  <span>สรุปรายงาน</span>
                </a>
                {{-- <div id="report" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                      <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">หัวข้อย่อย:</h6>
                        <a class="collapse-item" href="#">ภาพรวม</a>
                        <a class="collapse-item" href="{{route('searchreport')}}">Report </a>
                      </div>
                    </div> --}}


              </li>



            <!-- Divider -->
            <hr class="sidebar-divider">
            <!-- Heading -->
            <div class="sidebar-heading">
              INTERFACE
            </div>



            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#current_view" aria-expanded="true" aria-controls="collapseUtilities">
                  <i class="fas fa-fw fa-bell"></i>
                  <span>เเจ้งเตือนปัจจุบัน</span>
                </a>
                <div id="current_view" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">หัวข้อย่อย:</h6>
                    <a class="collapse-item" href="{{route('currentAlamhigh')}}">กระเเสไฟสูง</a>
                    <a class="collapse-item" href="{{route('currentAlamlow')}}">กระเเสไฟต่ำ</a>
                    <a class="collapse-item" href="{{route('currentAlamprom')}}">กระเเสไฟมีปัญหา</a>
                  </div>
                </div>
              </li>

                <li class="nav-item" >
                  <a class="nav-link collapsed" href="{{route('searchhistorycurrent')}}" >
                      <i class="fas fa-file-medical-alt"></i>
                    <span>ประวัติการเเจ้งเตือน</span>
                  </a>

                </li>




         <!-- Divider -->
         <hr class="sidebar-divider">
            <!-- Heading -->
            <div class="sidebar-heading">
              HELP
            </div>





          <!-- Nav Item - Utilities Collapse Menu -->
             <li class="nav-item">
                <a class="nav-link collapsed" href="{{route('help')}}" >
                  <i class="fas fa-fw fa-question-circle"></i>
                  <span>ช่วยเหลือ</span>
                </a>
              </li>
                <!-- Nav Item - Utilities Collapse Menu -->


             @if( Auth::user()->type  == 1 )
             <li class="nav-item">
                <a class="nav-link collapsed" href="{{route('admin\dashboard')}}" >
                  <i class="fas fa-arrow-circle-left"></i>
                  <span>กลับหน้า การจัดการ </span>
                </a>
              </li>
              @elseif( Auth::user()->type  == 2)
                  <li class="nav-item">
                      <a class="nav-link collapsed" href="{{route('maintence\dashboard')}}" >
                      <i class="fas fa-arrow-circle-left"></i>
                      <span>กลับหน้า การจัดการ </span>
                      </a>
                  </li>

             @endif





        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
          <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

      </ul>
      <!-- End of Sidebar -->

      <!-- Content Wrapper -->
      <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

          <!-- Topbar -->
          <nav class="  custom-navbar navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow" >

            <!-- Sidebar Toggle (Topbar) -->
            <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
              <i class="fa fa-bars"></i>
            </button>

            <!-- Topbar Time เวลา จาก เซิฟเวอร์ -->
            <span class="mr-3 d-none d-lg-inline text-gray-600 large text-menubar ml-3"   id="time">00:00:00  </span>
            <a id="circle"  style=" margin-right: 30px"  class="mr-3 d-none d-lg-inline text-gray-600 large animate-flicker"></a>
            <span class="text-muted">Phase 3</span>



            <!-- Topbar Navbar -->
            <ul class="navbar-nav ml-auto">



              <li class="nav-item dropdown no-arrow mx-1 " >
                  <a class="nav-link dropdown-toggle " href="{{route('currentAlamlow')}}"  role="button"  aria-expanded="false">
                    <i class="fas fa-bell fa-fw  size-notification"  style="text-shadow: 0 0 0px #000;color:{{$color['color_low']}}"></i>
                          <span class="badge badge-danger badge-counter " style="font-size: 15px;" id="low"></span>
                  </a>
                </li>

                <div class="topbar-divider d-none d-sm-block"></div>

                <li class="nav-item dropdown no-arrow mx-1 " >
                <a class="nav-link dropdown-toggle "    href="{{route('currentAlamhigh')}}"  role="button"  aria-expanded="false">
                      <i class="fas fa-bell fa-fw  size-notification"  style="text-shadow: 0 0 0px #000;color:{{$color['color_high']}}"></i>
                            <span class="badge badge-danger badge-counter " style="font-size: 15px;" id="high"></span>
                    </a>
                  </li>
                  <div class="topbar-divider d-none d-sm-block"></div>

                  <li class="nav-item dropdown no-arrow mx-1 " >
                      <a class="nav-link dropdown-toggle " href="{{route('currentAlamprom')}}"  role="button"  aria-expanded="false">
                        <i class="fas fa-bell fa-fw  size-notification"  style="text-shadow: 0 0 0px #000;color:{{$color['color_critical']}}"></i>
                              <span class="badge badge-danger badge-counter " style="font-size: 15px;" id="critical"></span>
                      </a>
                    </li>
                    <div class="topbar-divider d-none d-sm-block"></div>


              <!-- Nav Item - User Information -->
              <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ Auth::user()->name }}</span>
                  <i class="fas fa-fw fa-user-circle" style="font-size: 30px"> </i>
                </a>
                <!-- Dropdown - User Information -->
                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">

                  <a class="dropdown-item" href="#" data-toggle="modal" data-target="#Modelsettinglayout">
                    <i class="fas fa-cog fa-sm fa-fw mr-2 text-gray-400"></i>
                   ตั้งค่า
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    ออกจากระบบ
                  </a>
                </div>
              </li>

            </ul>

          </nav>
          <!-- End of Topbar -->

          <!-- Begin Page Content -->
          <div class="container-fluid">

            @yield('content')

          </div>
          <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright &copy; IDC-JASTEL {{date("Y")}}</span>
            </div>
          </div>
        </footer>
        <!-- End of Footer -->

      </div>
      <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
          <div class="modal-header" style="background-color:#4e73df;">

            <span class="modal-title text-light" >  คุณต้องการออกจากระบบใช่หรือไม่ </span>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true" class="text-light">×</span>
            </button>
          </div>
          <div class="modal-body">
              <div class="container">
                  <div class="row justify-content-center">


                      <i class="fas fa-tired justify-content-center mt-3 mb-3" style="font-size: 150px;color:#dc3545;"></i>

                  </div>
              </div>

          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">ปิดหน้าต่าง</button>
             <a class="btn btn-primary" href="{{ route('logout') }}" onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">

             ออกจากระบบ
             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
            </form>
            </a>
          </div>
        </div>
      </div>
    </div>


<!-- Modal -->
<div class="modal fade" id="Modelsettinglayout" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
      <div class="modal-content">

        <form  action="{{route('updateuserlayout',Auth::user()->email)}}"   method="post"     enctype="multipart/form-data" >
            @csrf
        <div class="modal-header" style="background-color:#4e73df;">

          <span class="modal-title text-light">ตั้งค่าการเเสดงผล</span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" class="text-light">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-4">
                  <div class="list-group" id="list-tab" role="tablist">
                    <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">ตั้งค่าเสียง</a>
                    <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">ตั้งค่าสี</a>
                    <a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages">การเเสดงผล</a>

                  </div>
                </div>
                <div class="col-8">
                  <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">สถานะเสียงเเจ้งเตือน</label>
                            <select class="form-control"  name="statussound">
                                @if(Auth::user()->sound == 1)
                                        <option value="1">เปิด</option>
                                        <option value="0">ปิด</option>
                                @else
                                        <option  value="0">ปิด</option>
                                        <option  value="1">เปิด</option>
                                @endif

                            </select>
                          </div>

                    </div>
                    <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">ตรีมสีในการเเสดงผล</label>
                            <select id="mySelect" data-show-content="true" class="form-control" name="selectcolor">

                                <option data-content="<i class='fa fa-stop' style='color:#4e73df;font-size: 25px;'></i> ตรีมสีที่ 1" value="#4e73df"></option>
                                <option data-content="<i class='fa fa-stop' style='color:#008080;font-size: 25px;'></i> ตรีมสีที่ 2" value="#008080"></option>
                                <option data-content="<i class='fa fa-stop' style='color:#3CB371;font-size: 25px;'></i> ตรีมสีที่ 3" value="#3CB371"></option>


                            </select>
                          </div>

                    </div>
                    <div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">
                        <div class="row">
                            <div class="col-md-6 mt-2">
                                <div class="card" style="width: 8rem; ">
                                    <img class="card-img-top" src="img/Layout 1.png" alt="Card image cap">
                                    <div class="form-check ">
                                        <center>  <input class="form-check-input mt-3" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>  </center>

                                    </div>
                                  </div>

                            </div>
                            <div class="col-md-6 mt-2">
                                <div class="card" style="width: 8rem; ">
                                    <img class="card-img-top " src="img/Layout 2.png" alt="Card image cap">
                                    <div class="form-check ">
                                        <center>  <input class="form-check-input mt-3" type="radio" name="exampleRadios" id="exampleRadios1" value="option2" unchecked> </center>

                                    </div>
                                  </div>

                            </div>

                        </div>

                    </div>

                  </div>
                </div>
              </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>
          <button type="submit" class="btn btn-primary">บันทึก</button>
        </div>

        </form>
      </div>
    </div>
  </div>




  <!-- Modal  เอาวันขึ้นครั้งเเรกที่มีการเปิดเว็บไซต์ ถ้าผู้ใช้ กดรับทราบมันจะไม่ขึ้นอีก-->
<div class="modal fade" id="modelnotificationupdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header bg-warning">
          <h5 class="modal-title text-center text-muted">หน้าต่างเเจ้งเตือนการอัพเดท !!!!</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body  text-muted">
            <span> หัวข้อ :  {{$querylastnotification->subject}} </span> <br>
            <span> วันที่ประกาศ : {{$querylastnotification->datetime}}</span><br>
            <span> โดย :ทีมงาน Innovation  </span>
          <center>   <span> --------------------------------------------</span> </center>


            <div class="form-group text-muted">
                <label for="exampleFormControlTextarea1">รายละเอียด</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" readonly>{{$querylastnotification->textvalue}}
                </textarea>
              </div>
        </div>
        <div class="modal-footer">
                <form action="{{route('postupdatenotification',auth()->user()->email)}}"  method="post" >
                    @csrf
                    <button type="submit" class="btn-primary " >รับทราบ</button>

                </form>

        </div>
      </div>
    </div>
  </div>




  </body>


<!-- ถ้า   ค่า ststus == 0 จะเข้าโหมด รอ admin เปิด ค่า status ให้ -->
@else
<body   class="bg-color"  >
    <div class="container-fluid" >
        <div class="row justify-content-center">
                     <div class="col-md-4 mt-5">
                    <div class="card mb-5">
                        <div class="card-header bg-warning text-white">กรุณารอ Admin ทำการยืนยันสถานะ</div>
                        <div class="card-body">
                                <center>
                                        <div class="w-100 ">
                                                <img src="../img/images.png" class="img-fluid img-thumbnail"  height="400px;" alt="Sheep">
                                        </div>
                                        <div class="mt-5">
                                                <span>กรุณาติดต่อ แอดมินแผนก IDC </span>
                                        </div>
                                        <div >

                                            <span> 02x-xxx-xxx </span>

                                    </div>
                                        <div class="mt-3">
                                            <a class="btn btn-primary"  href="{{ route('logout') }}"  onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();" role="button">

                                               รับทราบเเล้ว
                                               <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                                </form>

                                            </a>

                                        </div>
                                </center>

                        </div>
                    </div>
                </div>

            </div>

 </div>


  </body>

@endif



  <!-- jquery -->
  <script type="text/javascript" src="{{ URL::asset('/js/jquery-3.4.1.min.js') }}"></script>

  <!-- bootstrap -->
  <script type="text/javascript" src="{{ URL::asset('/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script type="text/javascript" src="{{ URL::asset('/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  <!-- Custom scripts for all pages-->
  <script type="text/javascript" src="{{ URL::asset('/js/sb-admin-2.min.js') }}"></script>

 <!-- การเเจ้งเตือน -->
 <script type="text/javascript" src="{{ URL::asset('/js/toastr.min.js') }}"></script>

 <!-- เวลา จาก server -->
 <script type="text/javascript" src="{{ URL::asset('/js/time.js') }}"></script>

 <!-- เวลาในการ reload หน้า web -->
 <script type="text/javascript" src="{{ URL::asset('/js/timerequet.js') }}"></script>

 <!-- ปรับ layout 3D ให้คงที่ -->
<script type="text/javascript" src="{{ URL::asset('/js/resize2.js') }}"></script>


<!-- table -->
<script type="text/javascript" src="{{ URL::asset('/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/buttons.html5.min.js') }}"></script>






<!-- bootstrap select -->
<script type="text/javascript" src="{{ URL::asset('/js/bootstrap-select.min.js') }}"></script>


 <!-- highchart -->
 <script src="{{ URL::asset('/js/highstock.js') }}"></script>
 <script src="{{ URL::asset('/js/data.js') }}"></script>

 <!-- pickadate -->
 <script type="text/javascript" src="{{ URL::asset('/js/lib_pickadate.js') }}"></script>
 <script type="text/javascript" src="{{ URL::asset('/js/pickadate.js') }}"></script>



 <!--Javascript  ข้างนอก -->
 <!--ดึงข้อมูลลงตาราง -->
 <script type="text/javascript" src="{{ URL::asset('/js/table.js') }}"></script>
 <!-- เกี่ยวกับ pop up model -->
 <script type="text/javascript" src="{{ URL::asset('/js/model_query.js') }}"></script>
 <!-- chart 1Minutes -->
 <script type="text/javascript" src="{{ URL::asset('/js/chart_1minute.js') }}"></script>


      <!-- เอาไว้เเจ้งเตือนเมื่อเข้าสู่ระบบ เเละ  มีการแก้ไขหรืออัพเดทอะไรสักอย่าง -->
      <script>
        @if(Session::get('alert-type') == "success" )
            toastr.success("{{ Session::get('message') }}");

        @elseif(Session::get('alert-type') == "warning")
            toastr.warning("{{ Session::get('message') }}");

        @elseif(Session::has('message'))
                toastr.success("{{ Session::get('message') }}");
        @endif
    </script>


  <!--เอาไว้เลือกสีการเเจ้งเตือน -->
  <script>
    $('#mySelect').selectpicker();
    </script>

 <!--เอาไว้เเจ้งเตือนการอัพเดทอะไรใหม่ๆ -->
<script type="text/javascript">
    $(window).on('load',function(){


                //เข้ารหัส code laravel
                var email  =  {!! json_encode((array)auth()->user()->email) !!};

                fetch('getwhonotificationupdate/'+encodeURIComponent(email),{mode: 'cors'})
                .then((response) => {
                    return response.json();
                })
                .then((myJson) => {

                            //ถ้าผู้ใช้ยังไม่กด รับทราบก็จะขึ้นเเบบนั้นไปตลอดจนกว่าจะกดรับทราบ
                            if(myJson.data == "NO"){
                                    $('#modelnotificationupdate').modal('show');
                            }



                 });

    });
</script>




</html>
