<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');


Route::get('/login', function () {
    return view('auth.login');
});


Auth::routes();



//Route for normal user
Route::group(['middleware' => ['auth']], function () {


    #หน้าเเรก
    Route::get('/home', 'HomeController@index')->name('home');

    #หน้าการค้นหากระเเสไฟย้อนหลัง
    Route::get('/searchhistory', 'HomeController@searchhistory')->name('searchhistory');

    #เอาไว้รับ request จาก การค้นหา
    Route::post('/queryhistory', 'HomeController@queryhistory')->name('queryhistory');

    #หน้าการค้นหาข้อมูลเชิกลึกย้อนหลัง
    Route::get('/searchtecnical', 'HomeController@searchtecnical')->name('searchtecnical');

    #เอาไว้รับ request จาก การค้นหาข้อมูลเชิกลึกย้อนหลัง
    Route::post('/querytecnical', 'HomeController@querytecnical')->name('querytecnical');

    ##หน้าการสรุปค่ากระเเสไฟ
    Route::get('/searchreport', 'HomeController@searchreport')->name('searchreport');

    #เอาไว้รับ request จาก การสรุปค่ากระเเสไฟ
    Route::post('/report', 'HomeController@report')->name('report');

    #หน้ากระเเสไฟสูง
    Route::get('/currentAlamhigh', 'HomeController@currentAlamhigh')->name('currentAlamhigh');

    #หน้ากระเเสไฟต่ำ
    Route::get('/currentAlamlow', 'HomeController@currentAlamlow')->name('currentAlamlow');

    #หน้ากระเเสไฟมีปัญหา
    Route::get('/currentAlamprom', 'HomeController@currentAlamprom')->name('currentAlamprom');

    #ประวัติกระเเสไฟย้อนหลัง
    Route::get('/searchhistorycurrent', 'HomeController@searchhistorycurrent')->name('searchhistorycurrent');

    #เอาไว้รับ request จาก การค้นหา ประวัติกระเเสไฟย้อนหลัง
    Route::post('/historycurrent','HomeController@historycurrent')->name('historycurrent');

    #ช่วยเหลือ
    Route::get('/help', 'HomeController@help')->name('help');





    #API--------------------------------------
    #api เอาไว้คิวรีว่าในตู้เเร็คที่เลือกมีตัวเเคมป์ไฟกี่ตัว
    Route::get('/getcampinrack/{id}', 'HomeController@getcampinrack')->name('getcampinrack');

    #api ดึง rack ออกมาว่ามีจำนวน
    Route::get('/getrack', 'HomeController@getrack')->name('getrack');

    #api ดึง ว่าตู้ rack ไหนมี alaram critical บ้าง
     Route::get('/getcriticalinrack', 'HomeController@getcriticalinrack')->name('getcriticalinrack');

    #api นับจำนวน สิ่งที่มีปัญหา
     Route::get('/getcountpromble', 'HomeController@getcountpromble')->name('getcountpromble');

     #api คิวรี ความผิดปกติ currentview ยังไม่เคลียร์
     Route::get('/getcurrentviewunclear', 'HomeController@getcurrentviewunclear')->name('getcurrentviewunclear');

    #api คิวรี ความผิดปกติ currentview เคลียร์เเล้ว
    Route::get('/getcurrentviewclear', 'HomeController@getcurrentviewclear')->name('getcurrentviewclear');

    #api คิวรี อุปกรณ์วัดกระเเสไฟ pcb ที่ออนไลน์
    Route::get('/getdevicepcbonline', 'HomeController@getdevicepcbonline')->name('getdevicepcbonline');

    #api คิวรี อุปกรณ์วัดกระเเสไฟ pcb ที่ออฟไลน์
    Route::get('/getdevicepcbofline', 'HomeController@getdevicepcbofline')->name('getdevicepcbofline');

    #api คิวรี ข้อมูล 1 นาที
    Route::get('/getData1Minutes/{id}', 'HomeController@getData1Minutes')->name('getData1Minutes');

    #api จำนวน อลาม สูง  ต่ำ  ร้ายเเรง
    Route::get('/getalamall', 'HomeController@getalamall')->name('getalamall');

    #api คิวรี 1 นาที ตามที่มีการ ค้นหา
    Route::get('/queryData1Minutefromsearch/{startdate}/{enddate}/{keydevice}','HomeController@queryData1Minutefromsearch')->name('queryData1Minutefromsearch');

    #api เอาไว้ ดูข้อมูลย่อยว่า 15 นาที ผ่านมี 1 นาทีอะไรบ้างที่มีปัญหา
    Route::get('/getdetal15minutes/{datetime}/{keydevice}','HomeController@getdetal15minutes')->name('getdetal15minutes');

    #api เอาไว้ ดูข้อมูลย่อยว่า 24 ชั่วโมง ผ่านมี 1 นาทีอะไรบ้างที่มีปัญหา
    Route::get('/getdetal24h/{datetime}/{keydevice}','HomeController@getdetal24h')->name('getdetal24h');

    #api เอาไว้อัพเดทผู้ใช้ จำพวกเสียง  สี เเละรูปแบบการเเสดงผล
    Route::post('/updateuserlayout/{email}','HomeController@updateuserlayout')->name('updateuserlayout');

    #api เอาไว้เช็คว่าผู้ัใช้กดเปิดเสียงหรือไม่ถ้ากดให้ทำการเเจ้งเตือนเสียงนั้น
    Route::get('/getstatesound/{email}', 'HomeController@getstatesound')->name('getstatesound');

    #api เอาไว้ query ว่าใครกดยอมรับการเเจ้งเตือนเเล้วมั่ง
    Route::get('/getwhonotificationupdate/{email}','HomeController@getwhonotificationupdate')->name('getwhonotificationupdate');

    #api นี้เอาไว้ให้ผู้ใช้กดรับทราบการอัพเดท
    Route::post('/postupdatenotification/{email}','HomeController@postupdatenotification')->name('postupdatenotification');

    #api นี้เอาไว้คิวรีข้อมูลเพิ่มเติมของอุปกรณ์วัดในตู้เเร็ค พวกค่าสูง  ค่าต่ำ
    Route::get('/getmoredeviceonrack/{startdate}/{endate}/{keyrack}','HomeController@getmoredeviceonrack')
    ->name('getmoredeviceonrack');




    #ส่วนนอก--------------------------------------
    #เช็คว่าใครออนไลน์ในระบบบ้าง
    Route::get('/check', 'UserController@userOnlineStatus');


    #เวลาของ server
    Route::get('/gettimefromserver','UserController@gettimefromserver')->name('gettimefromserver');

});


//Route for admin
Route::group(['prefix' => 'admin'], function(){
    Route::group(['middleware' => ['admin']], function(){

         #หน้าเเรก ซึ่งหน้าเเรกของ admin  จัดการผู้ใช้
         Route::get('/dashboard', 'admin\AdminController@index')->name('admin\dashboard');

         #เช็คว่าใครออนไลน์ในระบบบ้าง
         Route::get('/check', 'UserController@userOnlineStatus');

         #อัพเดทหรือแก้ไขข้อมูลผู้ใช้
         Route::post('/updateuser/{id}','admin\AdminController@updateuser');

         #เอาไว้ลบผู้ใช้
         Route::post('/deleteuser/{id}','admin\AdminController@deleteuser')->name('admin\deleteuser');

         #จัดการตู้ Rack
         Route::get('/rack', 'admin\AdminController@rack')->name('admin\rack');

         #อัพการเปิดสถานะอุปกรณ์ที่เชื่อมต่อเข้ามาใหม่
         Route::post('/updatedeviceconnectnew/{id}','admin\AdminController@updatedeviceconnectnew');

         #อัพการจัดการอุปกรณ์ที่เปิดเเต่ยังไม่ถูกเพิ่มไปในตู้เเร็ค
         Route::post('/updeviceopenfortoreack/{id}','admin\AdminController@updeviceopenfortoreack');

         #ดึงตัวเเคมป์ใน rack ว่ามีตู้ rackที่เลือกมีอะไรบ้าง
         Route::get('/getcampinrack/{id}', 'admin\AdminController@getcampinrack')
         ->name('admin\getcampinrack');

         #เอาไว้ลบอุปกรณ์วัด
         Route::post('/deletedevice/{id}','admin\AdminController@deletedevice')
         ->name('admin\deletedevice');

         #เอาไว้ย้ายอุปกรณ์ไปตู้rackใหม่ที่ต้องการย้าย เเละ การตั้งค่าเกณฑ์กระเเสไฟด้วย
         Route::post('/movecamp/{id}', 'admin\AdminController@movecamp')->name('admin\movecamp');

         #การตั้งค่าข้อความเเจ้งเตือนผู้ใช้ถึงการอัพเดทเเละสิ่งที่จะเกิดขึ้นในเว็บ
         Route::get('/notificationnew', 'admin\AdminController@notificationnew')
         ->name('admin\notificationnew');

         #เอาไว้เเจ้งการอัพเดทแก้ผู้ใช้
         Route::post('/updatenotificationnew', 'admin\AdminController@updatenotificationnew')
         ->name('admin\updatenotificationnew');


         #จัดการสถานะอุปกรณ์วัดว่าเป็นอย่างไร
         Route::get('/devicepcb', 'admin\AdminController@devicepcb')->name('admin\devicepcb');


         #เอาไว้เพิ่มรูปภาพอุปกรณ์วัด pcb
         Route::post('/adddevicepcb', 'admin\AdminController@adddevicepcb')->name('admin\adddevicepcb');


        #เอาไว้เเสดงข้อมูลอุปกรณ์วัดPCB
        Route::get('/showdatapcb/{id}', 'admin\AdminController@showdatapcb')
        ->name('admin\showdatapcb');


        #เอาไว้แก้ไขชื่ออุปกรณ์
        Route::post('/editpcbname', 'admin\AdminController@editpcbname')
        ->name('admin\editpcbname');


         #เอาไว้ลบอุปกรณ์พีซีบี
         Route::post('/deletedevicepcb/{id}','admin\AdminController@deletedevicepcb')
         ->name('admin\deletedevicepcb');


         #เอาไว้ดูlog ว่าใครทำอะไรบ้าง
         Route::get('/log', 'admin\AdminController@log')->name('admin\log');

         #เอาไว้ตั้งค่าการเเจ้งเตือนระดับร้ายเเรง ไฟดับ
         Route::get('/critical', 'admin\AdminController@critical')->name('admin\critical');

         #เอาไว้อัพเดทสีระรับร้ายเเรงไฟดับ
         Route::post('/updatecritical', 'admin\AdminController@updatecritical')
         ->name('admin\updatecritical');

         #เอาไว้ตั้งค่าการเเจ้งเตือนกระเเสไฟสูงกว่าเกณฑ์
         Route::get('/high', 'admin\AdminController@high')->name('admin\high');

         #เอาไว้อัพเดทสีระรับร้ายเเรงไฟฟ้าสูงกว่าเกณฑ์
         Route::post('/updatehigh', 'admin\AdminController@updatehigh')->name('admin\updatehigh');

         #เอาไว้ตั้งค่าการเเจ้งเตือนกระเเสไฟต่ำกว่าเกณฑ์
         Route::get('/low', 'admin\AdminController@low')->name('admin\low');

         #เอาไว้อัพเดทสีระรับร้ายเเรงไฟฟ้าต่ำกว่าเกณฑ์
         Route::post('/updatelow', 'admin\AdminController@updatelow')->name('admin\updatelow');

         #เอาไว้ตั้งค่าการเเจ้งเตือนกระเเสไฟปกติ
         Route::get('/normal', 'admin\AdminController@normal')->name('admin\normal');

         #เอาไว้อัพเดทสีระรับร้ายเเรงไฟฟ้าปกติ
         Route::post('/updatenormal', 'admin\AdminController@updatenormal')->name('admin\updatenormal');

         #เอาไว้ให้เเนะนำ admin ตรงนี้คืออะไร
         Route::get('/help', 'admin\AdminController@help')->name('admin\help');

         #เอาไว้อัพเดทข้อมูลตู้เเร็ค
         Route::post('/updateRack/{id}','admin\AdminController@updateRack')->name('admin\updateRack');






    });
});


//Route for maintence
Route::group(['prefix' => 'maintence'], function(){
    Route::group(['middleware' => ['maintence']], function(){

        #จัดการตู้ Rack
        Route::get('/dashboard', 'maintence\MaintenceController@rack')->name('maintence\dashboard');

        #อัพการเปิดสถานะอุปกรณ์ที่เชื่อมต่อเข้ามาใหม่
        Route::post('/updatedeviceconnectnew/{id}','maintence\MaintenceController@updatedeviceconnectnew');

        #อัพการจัดการอุปกรณ์ที่เปิดเเต่ยังไม่ถูกเพิ่มไปในตู้เเร็ค
        Route::post('/updeviceopenfortoreack/{id}','maintence\MaintenceController@updeviceopenfortoreack');

        #ดึงตัวเเคมป์ใน rack ว่ามีตู้ rackที่เลือกมีอะไรบ้าง
        Route::get('/getcampinrack/{id}', 'maintence\MaintenceController@getcampinrack')
        ->name('maintence\getcampinrack');

        #เอาไว้ลบอุปกรณ์วัด
        Route::post('/deletedevice/{id}','maintence\MaintenceController@deletedevice')
        ->name('maintence\deletedevice');


        #เอาไว้ย้ายอุปกรณ์ไปตู้rackใหม่ที่ต้องการย้าย
        Route::post('/movecamp/{id}', 'maintence\MaintenceController@movecamp')
        ->name('maintence\movecamp');

        #จัดการสถานะอุปกรณ์วัดว่าเป็นอย่างไร
        Route::get('/devicepcb', 'maintence\MaintenceController@devicepcb')->name('maintence\devicepcb');

        #เอาไว้เพิ่มรูปภาพอุปกรณ์วัด pcb
        Route::post('/adddevicepcb', 'maintence\MaintenceController@adddevicepcb')
        ->name('maintence\adddevicepcb');

        #เอาไว้เเสดงข้อมูลอุปกรณ์วัดPCB
        Route::get('/showdatapcb/{id}', 'maintence\MaintenceController@showdatapcb')
        ->name('maintence\showdatapcb');


        #เอาไว้แก้ไขชื่ออุปกรณ์
        Route::post('/editpcbname', 'maintence\MaintenceController@editpcbname')
        ->name('maintence\editpcbname');


        #เอาไว้ลบอุปกรณ์พีซีบี
        Route::post('/deletedevicepcb/{id}','maintence\MaintenceController@deletedevicepcb')
        ->name('maintence\deletedevicepcb');

        #เอาไว้ตั้งค่าการเเจ้งเตือนระดับร้ายเเรง ไฟดับ
        Route::get('/critical', 'maintence\MaintenceController@critical')->name('maintence\critical');

        #เอาไว้อัพเดทสีระรับร้ายเเรงไฟดับ
        Route::post('/updatecritical', 'maintence\MaintenceController@updatecritical')
        ->name('maintence\updatecritical');


        #เอาไว้ตั้งค่าการเเจ้งเตือนกระเเสไฟสูงกว่าเกณฑ์
        Route::get('/high', 'maintence\MaintenceController@high')->name('maintence\high');

        #เอาไว้อัพเดทสีระรับร้ายเเรงไฟฟ้าสูงกว่าเกณฑ์
        Route::post('/updatehigh', 'maintence\MaintenceController@updatehigh')
        ->name('maintence\updatehigh');


        #เอาไว้ตั้งค่าการเเจ้งเตือนกระเเสไฟต่ำกว่าเกณฑ์
        Route::get('/low', 'maintence\MaintenceController@low')->name('maintence\low');

        #เอาไว้อัพเดทสีระรับร้ายเเรงไฟฟ้าต่ำกว่าเกณฑ์
        Route::post('/updatelow', 'maintence\MaintenceController@updatelow')
        ->name('maintence\updatelow');


        #เอาไว้ตั้งค่าการเเจ้งเตือนกระเเสไฟปกติ
        Route::get('/normal', 'maintence\MaintenceController@normal')->name('maintence\normal');

        #เอาไว้อัพเดทสีระรับร้ายเเรงไฟฟ้าปกติ
        Route::post('/updatenormal', 'maintence\MaintenceController@updatenormal')
        ->name('maintence\updatenormal');


        #เอาไว้ให้เเนะนำ admin ตรงนี้คืออะไร
        Route::get('/help', 'maintence\MaintenceController@help')->name('maintence\help');


        #เอาไว้อัพเดทข้อมูลตู้เเร็ค
        Route::post('/updateRack/{id}','maintence\MaintenceController@updateRack')
        ->name('maintence\updateRack');

    });
});

